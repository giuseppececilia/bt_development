package com.bluethread.websocket.client;

import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public abstract class Servant<T> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Servant.class
			.getName());
	
	protected BlockingQueue<T> queue;
	//protected Logger myLogger;
	
	Worker worker;
	
	public Servant(){

		queue = new LinkedBlockingQueue<T>();		
		worker = new Worker("Worker");
		worker.start();
		
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "Abstract Servant class instantiated", "Servant()", "\n");
		}
		
	}
	
	private void privateHandleRequest(T t){
		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "Servant", "privateHandleRequest(T)",
					"Handle received request: " + t+"\n");
		}

		
		handleRequest(t);
	}
	
	public abstract void handleRequest(T t);
	public abstract void cleanUpBeforeClosing();
	
	public void shutdown(){
		
		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "Servant", "shutdown()",
					"Call CleanUpBeforeClosing, deblocking servant main thread and shut down");
		}
		
		cleanUpBeforeClosing();

		
		worker.close();		
		
		
	}
	
	public synchronized void feedRequest(T t){
		
		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "Servant", "feedRequest(T)",
					"New request received: " + t+"\n");
		}

		
		try {
			queue.put(t);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	class Worker extends Thread{
		/**
		 * Logger for this class
		 */
		
		private boolean stop = false;
		public Worker(String name){
			super(name);
		}
		public void close(){
			stop = true;
		}
		
		public void run(){
			while(true){
				if(stop && queue.isEmpty()) return;
				try {
					T t = queue.poll(10, TimeUnit.SECONDS);
					if ( t!= null ) privateHandleRequest(t);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
			
		}
	}

}
