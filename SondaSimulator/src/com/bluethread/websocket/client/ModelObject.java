package com.bluethread.websocket.client;

public class ModelObject {
	
	private String ID;
	private long timestamp;
	private double lat;
	private double lng;
	private double param1;
	private double param2;
	private double param3;
	
	public ModelObject(String ID, 
			long timestamp, 
			double lat, 
			double lng, 
			double param1,
			double param2,
	        double param3)
	{
		this.ID = ID;
		this.timestamp = timestamp;
		this.lat = lat;
		this.lng = lng;
		this.param1 = param1;
		this.param2 = param2;
		this.param3 = param3;
	}
	
	
	public String toString(){
		return new String("[ID "+ID+", timestamp "+timestamp+", lat "+lat+
				", long "+lng+", param1 "+param1+", param2 "+param2+", param3 "+param3+"]");
	}
		

}
