package com.bluethread.websocket.client;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import java.util.logging.*;

@ClientEndpoint
public class SondaDataSender {
	
	private Session userSession;
	private MessageHandler messageHandler;
	private URI serverUri;
	private Logger myLogger;
	
	public SondaDataSender(final Properties initialProps) throws URISyntaxException{

		myLogger = Logger.getLogger("SondaDataSender");
		String uriString = initialProps.getProperty("serverURI");
		if (uriString == null) {
			myLogger.log(Level.SEVERE, "Endpoint URI not defined. Quitting now");
			System.exit(-1);
		}
		
		serverUri = new URI(uriString);
		myLogger.log(Level.INFO, "Endpoint URI: "+serverUri.toString());
		connect();
			
	}
	
	public void shutdown(){
		
		disconnect();
	}
	
	private void disconnect() {
		// TODO Auto-generated method stub
		try {
			userSession.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public SondaDataSender(final URI endpointURI){
		serverUri = endpointURI;
		connect();
	}
	
	
	private void connect(){
		
//		Connector theConnector = new Connector(serverUri);
//		theConnector.start();
		
		try{
			WebSocketContainer container = ContainerProvider.getWebSocketContainer();
			userSession = container.connectToServer(this, serverUri);
			myLogger.log(Level.INFO,"Endpoint CONNECTED");
		}catch (Exception e){
			throw new RuntimeException(e);			
		}
	}
	
	
	@OnOpen
	public void onOpen(final Session userSession){
		this.userSession = userSession;
		myLogger.log(Level.INFO,"Endpoint CONNECTED");
	}
	
	@OnClose
	public void onClose(final Session userSession, CloseReason reason){
		this.userSession = null;
		myLogger.log(Level.INFO,"Endpoint DISCONNECTED");
	}
	
	@OnMessage
	public void onMessage(final String message){
		if (messageHandler != null) messageHandler.handleMessage(message);
		myLogger.log(Level.INFO,"Message received from Endpoint");
	}

	public void addMessageHandler(final MessageHandler messageHandler){
		
		this.messageHandler = messageHandler;
		
	}
	
		
	public void sendMessage (final String message ){
		userSession.getAsyncRemote().sendText(message);
	}
	
	public static interface MessageHandler{
		public void handleMessage(String message);
	}
	
	private class Connector extends Thread{
		private URI serverUri;
		public Connector(URI serverURI){
			this.serverUri = serverURI;
		}
		public void run(){
			try{
				WebSocketContainer container = ContainerProvider.getWebSocketContainer();
				userSession = container.connectToServer(SondaDataSender.this, serverUri);
			}catch (Exception e){
				throw new RuntimeException(e);			
			}
		}
	}
}
