package com.bluethread.websocket.client;

import com.google.gson.*;
public class TestGson {
	
	public static void main(String[] args){
		
		ModelObject mo = new ModelObject("Sonda_xyz",1234523654,42.6478724,12.24678,43562.43,56446.644,4446.343);
		System.out.println(mo);
		Gson gson = new Gson();
		
		String jsonString = gson.toJson(mo);
		
		System.out.println(jsonString);
		
	}

}
