package com.bluethread.websocket.client;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;






import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;


public class SondaDataConsole {

	static class Handler implements SondaDataSender.MessageHandler{

		@Override
		public void handleMessage(String message) {
			// TODO Auto-generated method stub
			myLogger.log(Level.INFO, jsonMessageToString(message));
		}
		
	}
	
	private static Logger myLogger = Logger.getLogger("SondaDataConsole");
	
	public static void main(String[] args) throws InterruptedException, URISyntaxException, IOException{
		
		
		Properties props = new Properties();
		String propFileName = "com/bluethread/websocket/client/config.properties";
		InputStream inputProperties = Thread.currentThread().getContextClassLoader().getResourceAsStream(propFileName); 
		props.load(inputProperties);
		
		
		
		final SondaDataSender sondaDataSender = new SondaDataSender(props);
		sondaDataSender.addMessageHandler( new SondaDataConsole.Handler());

		
		
		
		for(int i =0; i<10; i++){
			
			Thread.sleep(5000);
			
			sondaDataSender.sendMessage(stringToJsonMessage(props));
			
			
		}
		
		sondaDataSender.shutdown();
	}
	
	
	private static String stringToJsonMessage( final Properties props) {
		// TODO Auto-generated method stub
		String ID = props.getProperty("sondaID");
		String param1 = props.getProperty("param1");
		String param2 = props.getProperty("param2");
		String param3 = props.getProperty("param3");
		JsonObjectBuilder job = Json.createObjectBuilder();
		job.add("ID",ID);
		job.add("time", System.currentTimeMillis());
		job.add("lat", 41.4352653);
		job.add("long", 12.35426735);
		job.add("param1",param1);
		job.add("param2",param2);
		job.add("param3",param3);
		String jsonFormattedString = job.build().toString();
		myLogger.log(Level.INFO,"sending JSON message "+ jsonFormattedString);
		return jsonFormattedString;
	}
	
	private static String jsonMessageToString(final String responseString) {

		//JsonObject root = Json.createReader(new StringReader(responseString)).readObject();
		//String message = root.getString("message");
		//String sender = root.getString("sender");
		//String received = root.getString("received");

		return responseString;
	}
}
