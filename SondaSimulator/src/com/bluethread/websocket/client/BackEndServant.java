package com.bluethread.websocket.client;

import java.util.logging.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;
import java.util.logging.Level;

import javax.json.*;
import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import com.bluethread.sonda.simulator.data.SondaData;



@ClientEndpoint
public class BackEndServant extends Servant<SondaData> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(BackEndServant.class
			.getName());

	private Properties props;
	private URI websocketUri;
	private Session userSession;
	private boolean sendStringsOnly=false;
	
	public BackEndServant() {
		super();
		Properties props = new Properties();
		String propFileName = "com/bluethread/websocket/client/config.properties";
		InputStream inputProperties = Thread.currentThread().getContextClassLoader().getResourceAsStream(propFileName); 
		try {
			props.load(inputProperties);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			logger.logp(Level.SEVERE, "BackEndServant", "BackEndServant()",
					"Endpoint URI not defined. Quitting now", e);

			System.exit(-1);
		}
		String serverURI = props.getProperty("serverURI");
		String sendOnlyStrings = props.getProperty("sendOnlyStrings");
		sendStringsOnly = Boolean.parseBoolean(sendOnlyStrings);
		if (serverURI == null)
		{
			logger.logp(Level.SEVERE, "BackEndServant", "BackEndServant()",
					"Endpoint URI not defined. Quitting now");
			System.exit(-1);
		}			
		try {
			websocketUri = new URI(serverURI );
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "BackEndServant", "BackEndServant()",
						"Endpoint URI not defined. Quitting now");
			}
			System.exit(-1);
		}
		connect();
		
	}
	
	
	private void connect() {
		// TODO Auto-generated method stub
		try{
			WebSocketContainer container = ContainerProvider.getWebSocketContainer();
			userSession = container.connectToServer(this, websocketUri);

			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "BackEndServant", "connect()",
						"Endpoint CONNECTED");
			}

		}catch (Exception e){
			
			logger.logp(Level.SEVERE, "BackEndServant", "connect()",
					"Endpoint not reachable. Quitting now", e);

			System.exit(-1);			
		}
	}


	@Override
	public void handleRequest(SondaData sd) {
		// TODO Auto-generated method stub
		String jsonFormat;
		if (sendStringsOnly)
			{
			  jsonFormat = sd.toJsonString2();
			}else 
				jsonFormat = sd.toJsonString();
		
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "BackEndServant",
					"handleRequest(SondaData)",
					">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		}

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "BackEndServant",
					"handleRequest(SondaData)",
					"sending JSON message  - jsonFormat=" + jsonFormat);
		}

		
		userSession.getAsyncRemote().sendText(jsonFormat);
		
	}
	
	
	public synchronized void feedRequest(SondaData data){
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "BackEndServant", "feedRequest(SondaData)",
					"New request received: - data=" + data);
		}

		
		try {
			queue.put(data);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@OnOpen
	public void onOpen(final Session userSession){
		this.userSession = userSession;

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "BackEndServant", "onOpen(Session)",
					"Endpoint CONNECTED");
		}

	}
	
	@OnClose
	public void onClose(final Session userSession, CloseReason reason){
		this.userSession = null;
		
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "BackEndServant",
					"onClose(Session, CloseReason)", "Endpoint DISCONNECTED");
		}

		
	}
	
	@OnMessage
	public void onMessage(final String message){
		
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "BackEndServant", "onMessage(String)",
					"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		}

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "BackEndServant", "onMessage(String)",
					"Message received from Endpoint: - message=" + message);
		}
	}


	@Override
	public void cleanUpBeforeClosing() {
		// TODO Auto-generated method stub
		try {
			userSession.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "BackEndServant",
					"cleanUpBeforeClosing()",
					"Closing user session, releasing all resources");
		}
	}
	
}
