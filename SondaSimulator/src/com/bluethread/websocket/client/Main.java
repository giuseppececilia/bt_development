package com.bluethread.websocket.client;



import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bluethread.sonda.simulator.data.SondaData; 

public class Main extends Thread{

	private boolean run = true;
	private Logger myLogger;
	private long sendingInterval;

	public Main(){
		super("MainThread");
		myLogger = Logger.getLogger("Main");
		
		Properties props = new Properties();
		String propFileName = "com/bluethread/websocket/client/config.properties";
		InputStream inputProperties = Thread.currentThread().getContextClassLoader().getResourceAsStream(propFileName); 
		try {
			props.load(inputProperties);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			myLogger.log(Level.SEVERE, "Endpoint URI not defined. Quitting now");
			System.exit(-1);
		}
		
		sendingInterval = Long.decode(props.getProperty("sendingInterval"));
		
	}
	
	public static SondaData sondaDataGenerator(){
		double rangeMin = 40.0000;
		double rangeMax = 42.0000;

		Random randomGenerator = new Random();

		String ID = "S"+ randomGenerator.nextInt(50);
		Date currentDate = new Date(System.currentTimeMillis());
		double latitude = rangeMin + (rangeMax - rangeMin) * randomGenerator.nextDouble();
		rangeMin = 12.0000;
		rangeMax = 13.0000;
		double longitude = rangeMin + (rangeMax - rangeMin) * randomGenerator.nextDouble();
		double p1 = randomGenerator.nextDouble();
		double p2 = randomGenerator.nextDouble();
		double p3 = randomGenerator.nextDouble();
		return new SondaData(ID,currentDate,latitude,longitude,p1,p2,p3); 
	}

	public void shutdown(){
		run = false;
	}


	public void run(){

		BackEndServant client;
		client = new BackEndServant();

		while (run){
			SondaData sd= Main.sondaDataGenerator();
			client.feedRequest(sd);
			try {
				Thread.sleep(sendingInterval);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		client.shutdown();
	}

	public static void main(String [] args){
		
		Main myMain = new Main();
		myMain.start();
				
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		myMain.shutdown();
		
		//		SondaClient client;
		//		client = new SondaClient();
		//				
		//		SondaData sd1= Main.sondaDataGenerator();
		//		SondaData sd2= Main.sondaDataGenerator();
		//		
		//		client.feedRequest(sd1);
		//		client.feedRequest(sd2);
		//		try {
		//			Thread.sleep(10000);
		//		} catch (InterruptedException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		//		
		//		client.shutdown();
	}
}
