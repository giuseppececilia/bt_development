package com.bluethread.sonda.server;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.bluethread.websocket.client.BackEndServant;

public class GenericServer {
	
	protected Properties myProps;
	protected Logger myLogger;
	
	protected BackEndServant clientOfTheEarthLeg;
	
	
	static {
        // must be called before any Logger method is used.
        System.setProperty("java.util.logging.manager", MyLogManager.class.getName());
    }
	
    public static class MyLogManager extends LogManager {
        static MyLogManager instance;
        public MyLogManager() { instance = this; }
        @Override public void reset() { /* don't reset yet. */ }
        private void reset0() { super.reset(); }
        public static void resetFinally() { instance.reset0(); }
    }
	
	
	
	public GenericServer (){
		
		myProps = new Properties();
		String propFileName = "com/bluethread/sonda/server/config.properties";
		InputStream inputProperties = Thread.currentThread().getContextClassLoader().getResourceAsStream(propFileName); 
		myLogger = Logger.getLogger("Server");
		
		try {
			myProps.load(inputProperties);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			myLogger.log(Level.SEVERE, "Endpoint URI not defined. Quitting now");
			System.exit(-1);
		}
		
		clientOfTheEarthLeg = new BackEndServant();
		
		
	}
	
	public void shutdown(){
		clientOfTheEarthLeg.shutdown();
	}
	
}
