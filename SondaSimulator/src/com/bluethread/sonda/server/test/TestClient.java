package com.bluethread.sonda.server.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class TestClient {
	
	private Socket serverSocket;
	
	public TestClient(int port){
		
		try {
			serverSocket = new Socket("localhost",port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	
	public void startTest(){
		try {
			PrintWriter writer = new PrintWriter(serverSocket.getOutputStream());
			writer.println("$GPGSA,A,3,03,05,07,08,10,15,18,19,21,28,,,1.4,0.9,1.1*3A");
			writer.flush();
			writer.close();
			serverSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Test Client: data sent: $GPGSA,A,3,03,05,07,08,10,15,18,19,21,28,,,1.4,0.9,1.1*3A");
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void main(String [] args){
		TestClient client = new TestClient(4321);
		client.startTest();
		
	}

}
