package com.bluethread.sonda.server;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.bluethread.sonda.processors.NMEAProcessor;
import com.bluethread.sonda.processors.SimpleProcessor;

public class TCPServer extends GenericServer{
	
	private ServerSocket socket;
	private int serverPort;
	private int poolSize;
	private SocketListener socketListener = null;
	
	
	
	public TCPServer()
	{
		super();
		
		serverPort = Integer.parseInt(myProps.getProperty("serverPort"));
		poolSize = Integer.parseInt(myProps.getProperty("poolSize"));
		
		try {
			socket= new ServerSocket(serverPort);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			myLogger.log(Level.SEVERE, "Cannot open socket on port: "+serverPort);
		}
					
		myLogger.log(Level.INFO,"**************************************************");
		myLogger.log(Level.INFO,"SERVER STARTED ON PORT: "+serverPort);
		myLogger.log(Level.INFO,"**************************************************");
			
	}
	
	@Override
	public void shutdown(){
		super.shutdown();
		myLogger.log(Level.INFO,"Server quitting");		
		socketListener.shutdown();		
	}
	
	public void runServer(){
		myLogger.log(Level.INFO, "Waiting connections on port: "+serverPort);
		if (socketListener == null){
			socketListener = new SocketListener();
			socketListener.start();
		}		
	}
		
	
	public static void main(String [] args){
		
		TCPServer server = new TCPServer();
		/*
		 * Catch the CTRL+C when running at command shell
		 */
		Runtime.getRuntime().addShutdownHook(new ShutdownInitiator(server));
		
		server.runServer();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Please press CTRL+C to stop the server.");
		System.out.println("");
		System.out.println("");
		
	/*
	 * Following lines should be uncommented if running in Eclipse IDE since CTRL+C doesn't work	
	 */
		
//		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//		System.out.println("Please press a key to stop the server.");
//		String line;
//		try {
//			line = reader.readLine();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		server.shutdown();
	}
	

	/*
	 * Server in the server. Just listen blocked on the open socket
	 * Activate an executor in the pool when a new connection is accepted
	 * on the socket 
	 */
	public class SocketListener extends Thread{
		
		private ExecutorService pool;
		private boolean stopServer = false;
		
		public SocketListener(){
			pool = Executors.newFixedThreadPool(poolSize);
		}
		
		/*
		 * shutdown set the stop flag at true and then de-block
		 * the listening thread by sending a new connect on the 
		 * socket
		 */
		public void shutdown(){
				
			stopServer = true;
			try {
				new Socket("localhost",serverPort).close();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			};
			
			myLogger.log(Level.INFO,"Socket listener shutting down");
			
		}
		
		public void run(){
			while(!stopServer){
				TCPServer.this.myLogger.log(Level.INFO,"SocketListener started, waiting for connections");
				try {										
					Socket socket = TCPServer.this.socket.accept();
					TCPServer.this.myLogger.log(Level.INFO,"New connection request");
					//pool.execute(new SimpleProcessor(socket));
					pool.execute(new NMEAProcessor(socket.getInputStream()));
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			pool.shutdown();
			try {
				TCPServer.this.socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			TCPServer.this.myLogger.log(Level.INFO,"SocketListener quitting");
		}
	}
	/*
	 * Thread executed by the runtime to shutdown the main server when CTRL+C is caught
	 */
	static class ShutdownInitiator extends Thread{
		 
		private TCPServer serverToClose;
		 
		 
		 public ShutdownInitiator(TCPServer toClose){
			 serverToClose = toClose;
			 
		 }		
		 public void run(){
			// System.out.println("Shutdown procedure started");
			 serverToClose.shutdown();
			 
			 try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
	 }
	
}
