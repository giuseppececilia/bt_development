package com.bluethread.sonda.processors;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class SimpleProcessor extends AbstractProcessor{

	private InputStream socketInStream;
	
	public SimpleProcessor(Socket socket){
		try {
			socketInStream = socket.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socketInStream));
		String line;
		try {
			while((line = bufferedReader.readLine())!=null){
				
				System.out.println(line);	
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
