package com.bluethread.sonda.processors;

import java.io.InputStream;
import java.util.logging.Level;

import net.sf.marineapi.nmea.event.AbstractSentenceListener;
import net.sf.marineapi.nmea.event.SentenceEvent;
import net.sf.marineapi.nmea.event.SentenceListener;
import net.sf.marineapi.nmea.io.SentenceReader;
import net.sf.marineapi.nmea.sentence.GSASentence;
 

public class NMEAProcessor extends AbstractProcessor implements SentenceListener{

	
	private InputStream nmeaIs;
	private SentenceReader sr;
	
	public NMEAProcessor(InputStream is){
		if(is !=null)
			nmeaIs = is;
		else System.exit(-1);
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		sr = new SentenceReader(nmeaIs);
		sr.addSentenceListener(this);
		sr.addSentenceListener(new GSASentenceReader());
		sr.start();
		myLogger.log(Level.INFO,"NMEAProcessor in execution");
	}

	@Override
	public void readingPaused() {
		// TODO Auto-generated method stub
		
		myLogger.log(Level.INFO, "READING PAUSED");
	}

	@Override
	public void readingStarted() {
		// TODO Auto-generated method stub
		myLogger.log(Level.INFO, "READING STARTED");
	}

	@Override
	public void readingStopped() {
		// TODO Auto-generated method stub
		myLogger.log(Level.INFO, "READING STOPPED");
	}

	@Override
	public void sentenceRead(SentenceEvent arg0) {
		// TODO Auto-generated method stub
		myLogger.log(Level.INFO, "READING SENTENCE");
		myLogger.log(Level.INFO, "SENTENCE: "+ arg0.getSentence().toString());
	
	}
	
	public class GSASentenceReader extends AbstractSentenceListener<GSASentence>{

		@Override
		public void sentenceRead(GSASentence sentence) {
			// TODO Auto-generated method stub
			NMEAProcessor.this.myLogger.log(Level.INFO,"DOP position: "+sentence.getPositionDOP());
		}
		
	}

}
