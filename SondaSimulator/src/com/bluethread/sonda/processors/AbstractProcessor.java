package com.bluethread.sonda.processors;

import java.util.logging.Logger;

public abstract class AbstractProcessor implements Runnable {

	protected Logger myLogger;
	
	public AbstractProcessor(){
		myLogger = Logger.getLogger("Processor");
	}
	
	@Override
	public abstract  void run();
}
