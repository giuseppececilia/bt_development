package com.bluethread.sonda.simulator.data;

import java.util.Date;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.swing.text.DateFormatter;

public class SondaData {
	
	private String ID;
	private Date samplingDate;
	private double latitude;
	private double longitude;
	private double parameter_1;
	private double parameter_2;
	private double parameter_3;
	private String jsonString = null;
	
	public SondaData(String ID, 
			Date date,
			double latitude,
			double longitude, 
			double p1, 
			double p2, 
			double p3 ){
		
		if(ID!=null) this.ID = ID;
		else this.ID = "EMPTY";
		
		if(date!=null) this.samplingDate= date;
		else date = new Date(System.currentTimeMillis());
		
		this.latitude =  latitude;
		this.longitude = longitude;
		this.parameter_1 = p1;
		this.parameter_2 = p2;
		this.parameter_3 = p3;
	}
	
	public String toString(){
		return "{ID="+ID+
				", sampling date="+ samplingDate+
				", latitude="+latitude+
				", longitude"+longitude+
				", parameter 1="+parameter_1+
				", parameter 2="+parameter_2+
				", parameter 3="+parameter_3+"}";
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public Date getSamplingDate() {
		return samplingDate;
	}

	public void setSamplingDate(Date samplingDate) {
		this.samplingDate = samplingDate;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getParameter_1() {
		return parameter_1;
	}

	public void setParameter_1(double parameter_1) {
		this.parameter_1 = parameter_1;
	}

	public double getParameter_2() {
		return parameter_2;
	}

	public void setParameter_2(double parameter_2) {
		this.parameter_2 = parameter_2;
	}

	public double getParameter_3() {
		return parameter_3;
	}

	public void setParameter_3(double parameter_3) {
		this.parameter_3 = parameter_3;
	}
	
	public String toJsonString(){
		JsonObjectBuilder job = Json.createObjectBuilder();
		job.add("ID", getID());
		job.add("sampling date",getSamplingDate().toString());
		job.add("lat",getLatitude());
		job.add("lng", getLongitude());
		job.add("p1", getParameter_1());
		job.add("p2", getParameter_2());
		job.add("p3", getParameter_3());
		return job.build().toString();
	}

	public String toJsonString2(){
		JsonObjectBuilder job = Json.createObjectBuilder();
		job.add("ID", getID());
		job.add("sampling date",getSamplingDate().toString());
		job.add("lat",Double.toString(getLatitude()));
		job.add("lng", Double.toString(getLongitude()));
		job.add("p1", Double.toString(getParameter_1()));
		job.add("p2", Double.toString(getParameter_2()));
		job.add("p3", Double.toHexString(getParameter_3()));
		return job.build().toString();
	}
	
}
