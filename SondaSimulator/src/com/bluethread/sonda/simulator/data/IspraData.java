package com.bluethread.sonda.simulator.data;

import java.util.Date;
import java.util.HashMap;

public class IspraData {
	
	private HashMap <Integer, Double>innerDataContainer;
	private String ID;
	private Date samplingDate;
	private double latitude;
	private double longitude;
	String jsonString;
	
	public IspraData(String ID, 
			Date date,
			double latitude,
			double longitude)
	{
		
		this.ID = ID;
		this.samplingDate = date;
		this.longitude = longitude;
		this.latitude = latitude;
		this.innerDataContainer = new HashMap<Integer, Double>();
		
	}
	
	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public Date getSamplingDate() {
		return samplingDate;
	}

	public void setSamplingDate(Date samplingDate) {
		this.samplingDate = samplingDate;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public boolean addParameter (Integer code, Double value){
		
		if(!innerDataContainer.containsKey(code)){
			innerDataContainer.put(code, value);
			return true;
		}
		else return false;
	}
	
	
	public String toString(){
		return "{ID="+ID+
				", sampling date="+ samplingDate+
				", latitude="+latitude+
				", longitude"+longitude+
				", parameters}" + innerDataContainer.values().toString();
	}
	

}
