package com.bluethread.sonda.simulator.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.GridLayout;
import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.PrintStream;

import javax.swing.UIManager;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

import com.bluethread.websocket.client.Main;
import java.awt.FlowLayout;
import javax.swing.border.CompoundBorder;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;
import java.awt.Font;

public class SondaSimulatorGUI_v2 {

	private JFrame frmWebsocketClient;
	JButton startButton;
	JButton stopButton;
	JPanel statusPanel;
	JTextArea logArea;
	JScrollPane scrollPane;
	private Main myMain;
	JPanel panel;
	JLabel statusLabel;
	//private Main myMain;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SondaSimulatorGUI_v2 window = new SondaSimulatorGUI_v2();
					window.frmWebsocketClient.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SondaSimulatorGUI_v2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmWebsocketClient = new JFrame();
		frmWebsocketClient.setTitle("Blue Thread - Simulatore e generatore dati di sonda");
		frmWebsocketClient.setBounds(100, 100, 450, 300);
		frmWebsocketClient.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmWebsocketClient.getContentPane().setLayout(new BorderLayout(0, 0));
		
		statusPanel = new JPanel();
		statusPanel.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null), new BevelBorder(BevelBorder.LOWERED, null, null, null, null)));
		statusPanel.setBackground(new Color(240, 240, 240));
		frmWebsocketClient.getContentPane().add(statusPanel, BorderLayout.NORTH);
		statusPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		statusLabel = new JLabel("            ");
		statusLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		statusLabel.setBackground(Color.RED);
		statusPanel.add(statusLabel);
		statusLabel.setOpaque(true);
		
		
		logArea = new JTextArea();
		scrollPane = new JScrollPane(logArea);
		frmWebsocketClient.getContentPane().add(scrollPane);
		//frame.getContentPane().add(logArea);
		PrintStream printStream = new PrintStream(new CustomOutputStream(logArea));
		
		panel = new JPanel();
		panel.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null), new BevelBorder(BevelBorder.LOWERED, null, null, null, null)));
		frmWebsocketClient.getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		startButton = new JButton("START");
		startButton.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(startButton);
		
		stopButton = new JButton("STOP");
		stopButton.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(stopButton);
		stopButton.addActionListener(new StopButtonActionListener());
		startButton.addActionListener(new StartButtonActionListener());
		startButton.setEnabled(true);
		stopButton.setEnabled(false);
		System.setOut(printStream);
		System.setErr(printStream);
		
		
		
	}

	
	private class StartButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			myMain = new Main();
			myMain.start();
			statusLabel.setBackground(Color.GREEN);
			startButton.setEnabled(false);
			stopButton.setEnabled(true);
		}
	}
	private class StopButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			myMain.shutdown();
			myMain = null;
			statusLabel.setBackground(Color.RED);
			startButton.setEnabled(true);
			stopButton.setEnabled(false);
		}
	}
	
	
	
	
	
}
