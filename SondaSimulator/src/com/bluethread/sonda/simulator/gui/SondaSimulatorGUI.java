package com.bluethread.sonda.simulator.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.GridLayout;
import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.PrintStream;

import javax.swing.UIManager;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

import com.bluethread.websocket.client.Main;

public class SondaSimulatorGUI {

	private JFrame frmWebsocketClient;
	JButton startButton;
	JButton stopButton;
	JPanel statusPanel;
	JTextArea logArea;
	JScrollPane scrollPane;
	private Main myMain;
	//private Main myMain;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SondaSimulatorGUI window = new SondaSimulatorGUI();
					window.frmWebsocketClient.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SondaSimulatorGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmWebsocketClient = new JFrame();
		frmWebsocketClient.setTitle("Websocket Client");
		frmWebsocketClient.setBounds(100, 100, 450, 300);
		frmWebsocketClient.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmWebsocketClient.getContentPane().setLayout(new GridLayout(4, 1, 0, 0));
		
		startButton = new JButton("START");
		startButton.addActionListener(new StartButtonActionListener());
		
		frmWebsocketClient.getContentPane().add(startButton);
		
		stopButton = new JButton("STOP");
		stopButton.addActionListener(new StopButtonActionListener());
		frmWebsocketClient.getContentPane().add(stopButton);
		
		statusPanel = new JPanel();
		statusPanel.setBackground(Color.RED);
		frmWebsocketClient.getContentPane().add(statusPanel);
		
		
		
		logArea = new JTextArea();
		scrollPane = new JScrollPane(logArea);
		frmWebsocketClient.getContentPane().add(scrollPane);
		//frame.getContentPane().add(logArea);
		PrintStream printStream = new PrintStream(new CustomOutputStream(logArea));
		System.setOut(printStream);
		System.setErr(printStream);
		
		
		
	}

	
	private class StartButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			myMain = new Main();
			myMain.start();
			statusPanel.setBackground(Color.GREEN);
		}
	}
	private class StopButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			myMain.shutdown();
			myMain = null;
			statusPanel.setBackground(Color.RED);
		}
	}
	
	
	
	
	
}
