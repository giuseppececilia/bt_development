package com.bluethread.websocket.server;

import javax.websocket.CloseReason;  
import javax.websocket.OnClose;  
import javax.websocket.OnMessage;  
import javax.websocket.Session;  
import javax.websocket.server.ServerEndpoint;  

import java.io.IOException;  
import java.util.LinkedList;  
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@ServerEndpoint(value = "/echo")
public class EchoWebSocketEndpoint {

    private static List<Session> connectedSessions = new LinkedList<>();
    private Logger myLogger;

    public EchoWebSocketEndpoint() {
		// TODO Auto-generated constructor stub
    	myLogger = Logger.getLogger("EchoWebsocketEndpoint");
	}
    
    @OnMessage
    public void getMessage(final String message, final Session session) {
        if (!connectedSessions.contains(session)) {
            connectedSessions.add(session);
            broadcastToAll("user" + session.getId() + " joined");
            broadcastToAll("user" + session.getId() + " says > " + message);
        } else {
            broadcastToAll("user" + session.getId() + " says > " + message);
        }
        
        myLogger.log(Level.INFO, message);
        myLogger.log(Level.INFO, " ");

    }

    private void broadcastToAll(final String s) {
        for (Session session : connectedSessions) {
            try {
                session.getBasicRemote().sendText(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @OnClose
    public void closeConnectionHandler(Session session, CloseReason closeReason) {
        connectedSessions.remove(session);
        broadcastToAll("user" + session.getId() + " quit");
    }

}