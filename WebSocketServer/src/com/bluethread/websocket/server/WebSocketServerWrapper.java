package com.bluethread.websocket.server;

import org.glassfish.tyrus.server.Server;

import javax.websocket.DeploymentException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;

public class WebSocketServerWrapper {

	public static void main(String args[]) {
		runServer();
	}

	private static void runServer() {
		
		Server server = new Server("localhost",8025,"/websocket", null, EchoWebSocketEndpoint.class);
		
		
		try {
			server.start();
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Please press a key to stop the server.");
			
			reader.readLine();
		} catch (DeploymentException  e) {
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
		catch(Exception e){
			
		}
		finally {
			server.stop();
		}

	}
}