grammar SmartBoxCommandsGrammar;

options {
  language=Java;
}
/*
 * @members {
   String s;}
 * 
 */


@header{
	package com.bluethread.rimorchiatori.smartbox.northifc.parser.antlrgenerated;
}

root			: loginsentence //{s = $loginsentence.text; System.out.println("found "+s);}
				| crewsentence  //{s = $crewsentence.text; System.out.println("found "+s);}
				| turnsentence  //{s = $turnsentence.text; System.out.println("found "+s);}
				| missionsentence //{s = $missionsentence.text; System.out.println("found "+s);}
				| missionstateacksentence //{s = $missionstateacksentence.text; System.out.println("found "+s);}
				| mmsiaddsentence
				| mmsiremovesentence;

loginsentence	: logkey sep mmsi sep loginparameters ;

loginparameters	: ok | nok;




crewsentence	: crwky sep mmsi sep crewparameters;

crewparameters	: nummembers crewmembers;

nummembers		:INT;

crewmembers		: (member)+  ;

member			: sep name sep surname sep employeeid sep role  ;

name			: STRING;

surname			: STRING;

employeeid		: INT;

role			: COMANDANTE | MARINAIO |NOSTROMO|CAPO_MACCHINE;

COMANDANTE		:'COMANDANTE';

MARINAIO		:'MARINAIO';

NOSTROMO		:'NOSTROMO';

CAPO_MACCHINE	:'CAPO MACCHINE';




turnsentence	: trnky sep mmsi sep turnparams;

trnky	:TURNKEY;

TURNKEY			: 'TURN_UPDATE';

turnparams		: numofturns turns;

numofturns		: INT;

turns			:(turn)+;

turn			: sep turntk ;

turntk			: MATTINA|POMERIGGIO|NOTTURNO|FESTIVO|NOTTURNO_E_FESTIVO;

MATTINA			:'MATTINA';

POMERIGGIO		:'POMERIGGIO';

NOTTURNO		:'NOTTURNO';

FESTIVO			:'FESTIVO';

NOTTURNO_E_FESTIVO:'NOTTURNO E FESTIVO';



missionsentence	: misky missionparams;

misky			:MISSIONKEY;

MISSIONKEY		:'MISSION_SEND';

missionparams	:sep missionid sep missiondate sep missiontime sep shipname sep pilotname sep type sep destination sep mmsi;

missionid		:INT;

missiondate		: date ;

missiontime		:time;

shipname		:STRING;

pilotname		: pilotfirstname pilotsecondname
				| STRING;

pilotfirstname		: STRING ;

pilotsecondname		: STRING ;

type			: STRING;

destination		: STRING;

mmsi			: INT;


knownmmsi		:INT;

missionstateacksentence	:misstackky sep mmsi sep missionid;


misstackky			:MISSIONSTATEACKKEY;

MISSIONSTATEACKKEY	:'MISSION_STATE_ACK';




mmsiaddsentence	:	mmsiaddkey (sep knownmmsi)+;

mmsiaddkey			: MMSIADDKEY;

MMSIADDKEY			:'MMSI_ADD';

mmsiremovesentence : mmsiremovekey (sep knownmmsi)+;


mmsiremovekey		: MMSIREMOVEKEY;

MMSIREMOVEKEY		:'MMSI_REMOVE';

date		: year slash month slash day;

year		:INT;

month		:INT;

day			:INT;
 

time: hour ':' minutes;

hour		:INT;
minutes		:INT;

slash:       '/'|'-';


ok				:OKTOKEN;

OKTOKEN			:'OK'|'ok';

nok				:NOKTOKEN;

NOKTOKEN		:'NOK'|'nok';


crwky			:CREWKEY;

CREWKEY			:'CREW_UPDATE';

logkey			:LOGINKEY;

LOGINKEY		: 'LOGIN_RECEIVE';

sep			: SEPARATOR;

SEPARATOR	: '|';


//ROLETOKEN		:  'COMANDANTE'
				//|  'MARINAIO'
				//|  'NOSTROMO'
				//;


INT				: [0-9] + ;

//STRING			: ('a'..'z'| 'A'..'Z')+ ;
STRING			: [a-zA-Z]+ ;

WS: (' ' |'\n' |'\r' )+ {skip();} ;

