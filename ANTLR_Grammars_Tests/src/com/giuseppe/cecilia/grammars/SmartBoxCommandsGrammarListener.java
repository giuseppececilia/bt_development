// Generated from SmartBoxCommandsGrammar.g4 by ANTLR 4.6

	package com.giuseppe.cecilia.grammars;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SmartBoxCommandsGrammarParser}.
 */
public interface SmartBoxCommandsGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#root}.
	 * @param ctx the parse tree
	 */
	void enterRoot(SmartBoxCommandsGrammarParser.RootContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#root}.
	 * @param ctx the parse tree
	 */
	void exitRoot(SmartBoxCommandsGrammarParser.RootContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#loginsentence}.
	 * @param ctx the parse tree
	 */
	void enterLoginsentence(SmartBoxCommandsGrammarParser.LoginsentenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#loginsentence}.
	 * @param ctx the parse tree
	 */
	void exitLoginsentence(SmartBoxCommandsGrammarParser.LoginsentenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#loginparameters}.
	 * @param ctx the parse tree
	 */
	void enterLoginparameters(SmartBoxCommandsGrammarParser.LoginparametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#loginparameters}.
	 * @param ctx the parse tree
	 */
	void exitLoginparameters(SmartBoxCommandsGrammarParser.LoginparametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#crewsentence}.
	 * @param ctx the parse tree
	 */
	void enterCrewsentence(SmartBoxCommandsGrammarParser.CrewsentenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#crewsentence}.
	 * @param ctx the parse tree
	 */
	void exitCrewsentence(SmartBoxCommandsGrammarParser.CrewsentenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#crewparameters}.
	 * @param ctx the parse tree
	 */
	void enterCrewparameters(SmartBoxCommandsGrammarParser.CrewparametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#crewparameters}.
	 * @param ctx the parse tree
	 */
	void exitCrewparameters(SmartBoxCommandsGrammarParser.CrewparametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#nummembers}.
	 * @param ctx the parse tree
	 */
	void enterNummembers(SmartBoxCommandsGrammarParser.NummembersContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#nummembers}.
	 * @param ctx the parse tree
	 */
	void exitNummembers(SmartBoxCommandsGrammarParser.NummembersContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#crewmembers}.
	 * @param ctx the parse tree
	 */
	void enterCrewmembers(SmartBoxCommandsGrammarParser.CrewmembersContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#crewmembers}.
	 * @param ctx the parse tree
	 */
	void exitCrewmembers(SmartBoxCommandsGrammarParser.CrewmembersContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#member}.
	 * @param ctx the parse tree
	 */
	void enterMember(SmartBoxCommandsGrammarParser.MemberContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#member}.
	 * @param ctx the parse tree
	 */
	void exitMember(SmartBoxCommandsGrammarParser.MemberContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(SmartBoxCommandsGrammarParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(SmartBoxCommandsGrammarParser.NameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#surname}.
	 * @param ctx the parse tree
	 */
	void enterSurname(SmartBoxCommandsGrammarParser.SurnameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#surname}.
	 * @param ctx the parse tree
	 */
	void exitSurname(SmartBoxCommandsGrammarParser.SurnameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#employeeid}.
	 * @param ctx the parse tree
	 */
	void enterEmployeeid(SmartBoxCommandsGrammarParser.EmployeeidContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#employeeid}.
	 * @param ctx the parse tree
	 */
	void exitEmployeeid(SmartBoxCommandsGrammarParser.EmployeeidContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#role}.
	 * @param ctx the parse tree
	 */
	void enterRole(SmartBoxCommandsGrammarParser.RoleContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#role}.
	 * @param ctx the parse tree
	 */
	void exitRole(SmartBoxCommandsGrammarParser.RoleContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#turnsentence}.
	 * @param ctx the parse tree
	 */
	void enterTurnsentence(SmartBoxCommandsGrammarParser.TurnsentenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#turnsentence}.
	 * @param ctx the parse tree
	 */
	void exitTurnsentence(SmartBoxCommandsGrammarParser.TurnsentenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#turnparams}.
	 * @param ctx the parse tree
	 */
	void enterTurnparams(SmartBoxCommandsGrammarParser.TurnparamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#turnparams}.
	 * @param ctx the parse tree
	 */
	void exitTurnparams(SmartBoxCommandsGrammarParser.TurnparamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#numofturns}.
	 * @param ctx the parse tree
	 */
	void enterNumofturns(SmartBoxCommandsGrammarParser.NumofturnsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#numofturns}.
	 * @param ctx the parse tree
	 */
	void exitNumofturns(SmartBoxCommandsGrammarParser.NumofturnsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#turns}.
	 * @param ctx the parse tree
	 */
	void enterTurns(SmartBoxCommandsGrammarParser.TurnsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#turns}.
	 * @param ctx the parse tree
	 */
	void exitTurns(SmartBoxCommandsGrammarParser.TurnsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#turn}.
	 * @param ctx the parse tree
	 */
	void enterTurn(SmartBoxCommandsGrammarParser.TurnContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#turn}.
	 * @param ctx the parse tree
	 */
	void exitTurn(SmartBoxCommandsGrammarParser.TurnContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#missionsentence}.
	 * @param ctx the parse tree
	 */
	void enterMissionsentence(SmartBoxCommandsGrammarParser.MissionsentenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#missionsentence}.
	 * @param ctx the parse tree
	 */
	void exitMissionsentence(SmartBoxCommandsGrammarParser.MissionsentenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#missionparams}.
	 * @param ctx the parse tree
	 */
	void enterMissionparams(SmartBoxCommandsGrammarParser.MissionparamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#missionparams}.
	 * @param ctx the parse tree
	 */
	void exitMissionparams(SmartBoxCommandsGrammarParser.MissionparamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#missionid}.
	 * @param ctx the parse tree
	 */
	void enterMissionid(SmartBoxCommandsGrammarParser.MissionidContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#missionid}.
	 * @param ctx the parse tree
	 */
	void exitMissionid(SmartBoxCommandsGrammarParser.MissionidContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#missiondate}.
	 * @param ctx the parse tree
	 */
	void enterMissiondate(SmartBoxCommandsGrammarParser.MissiondateContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#missiondate}.
	 * @param ctx the parse tree
	 */
	void exitMissiondate(SmartBoxCommandsGrammarParser.MissiondateContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#missiontime}.
	 * @param ctx the parse tree
	 */
	void enterMissiontime(SmartBoxCommandsGrammarParser.MissiontimeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#missiontime}.
	 * @param ctx the parse tree
	 */
	void exitMissiontime(SmartBoxCommandsGrammarParser.MissiontimeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#shipname}.
	 * @param ctx the parse tree
	 */
	void enterShipname(SmartBoxCommandsGrammarParser.ShipnameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#shipname}.
	 * @param ctx the parse tree
	 */
	void exitShipname(SmartBoxCommandsGrammarParser.ShipnameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#pilotname}.
	 * @param ctx the parse tree
	 */
	void enterPilotname(SmartBoxCommandsGrammarParser.PilotnameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#pilotname}.
	 * @param ctx the parse tree
	 */
	void exitPilotname(SmartBoxCommandsGrammarParser.PilotnameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#firstname}.
	 * @param ctx the parse tree
	 */
	void enterFirstname(SmartBoxCommandsGrammarParser.FirstnameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#firstname}.
	 * @param ctx the parse tree
	 */
	void exitFirstname(SmartBoxCommandsGrammarParser.FirstnameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#secondname}.
	 * @param ctx the parse tree
	 */
	void enterSecondname(SmartBoxCommandsGrammarParser.SecondnameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#secondname}.
	 * @param ctx the parse tree
	 */
	void exitSecondname(SmartBoxCommandsGrammarParser.SecondnameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(SmartBoxCommandsGrammarParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(SmartBoxCommandsGrammarParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#destination}.
	 * @param ctx the parse tree
	 */
	void enterDestination(SmartBoxCommandsGrammarParser.DestinationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#destination}.
	 * @param ctx the parse tree
	 */
	void exitDestination(SmartBoxCommandsGrammarParser.DestinationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#mmsi}.
	 * @param ctx the parse tree
	 */
	void enterMmsi(SmartBoxCommandsGrammarParser.MmsiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#mmsi}.
	 * @param ctx the parse tree
	 */
	void exitMmsi(SmartBoxCommandsGrammarParser.MmsiContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#missionstateacksentence}.
	 * @param ctx the parse tree
	 */
	void enterMissionstateacksentence(SmartBoxCommandsGrammarParser.MissionstateacksentenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#missionstateacksentence}.
	 * @param ctx the parse tree
	 */
	void exitMissionstateacksentence(SmartBoxCommandsGrammarParser.MissionstateacksentenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#date}.
	 * @param ctx the parse tree
	 */
	void enterDate(SmartBoxCommandsGrammarParser.DateContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#date}.
	 * @param ctx the parse tree
	 */
	void exitDate(SmartBoxCommandsGrammarParser.DateContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#year}.
	 * @param ctx the parse tree
	 */
	void enterYear(SmartBoxCommandsGrammarParser.YearContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#year}.
	 * @param ctx the parse tree
	 */
	void exitYear(SmartBoxCommandsGrammarParser.YearContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#month}.
	 * @param ctx the parse tree
	 */
	void enterMonth(SmartBoxCommandsGrammarParser.MonthContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#month}.
	 * @param ctx the parse tree
	 */
	void exitMonth(SmartBoxCommandsGrammarParser.MonthContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#day}.
	 * @param ctx the parse tree
	 */
	void enterDay(SmartBoxCommandsGrammarParser.DayContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#day}.
	 * @param ctx the parse tree
	 */
	void exitDay(SmartBoxCommandsGrammarParser.DayContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#time}.
	 * @param ctx the parse tree
	 */
	void enterTime(SmartBoxCommandsGrammarParser.TimeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#time}.
	 * @param ctx the parse tree
	 */
	void exitTime(SmartBoxCommandsGrammarParser.TimeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#hour}.
	 * @param ctx the parse tree
	 */
	void enterHour(SmartBoxCommandsGrammarParser.HourContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#hour}.
	 * @param ctx the parse tree
	 */
	void exitHour(SmartBoxCommandsGrammarParser.HourContext ctx);
	/**
	 * Enter a parse tree produced by {@link SmartBoxCommandsGrammarParser#minutes}.
	 * @param ctx the parse tree
	 */
	void enterMinutes(SmartBoxCommandsGrammarParser.MinutesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmartBoxCommandsGrammarParser#minutes}.
	 * @param ctx the parse tree
	 */
	void exitMinutes(SmartBoxCommandsGrammarParser.MinutesContext ctx);
}