// Generated from SmartBoxCommandsGrammar.g4 by ANTLR 4.6

	package com.giuseppe.cecilia.grammars;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SmartBoxCommandsGrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SmartBoxCommandsGrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#root}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoot(SmartBoxCommandsGrammarParser.RootContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#loginsentence}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoginsentence(SmartBoxCommandsGrammarParser.LoginsentenceContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#loginparameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoginparameters(SmartBoxCommandsGrammarParser.LoginparametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#crewsentence}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCrewsentence(SmartBoxCommandsGrammarParser.CrewsentenceContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#crewparameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCrewparameters(SmartBoxCommandsGrammarParser.CrewparametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#nummembers}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNummembers(SmartBoxCommandsGrammarParser.NummembersContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#crewmembers}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCrewmembers(SmartBoxCommandsGrammarParser.CrewmembersContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#member}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMember(SmartBoxCommandsGrammarParser.MemberContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitName(SmartBoxCommandsGrammarParser.NameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#surname}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSurname(SmartBoxCommandsGrammarParser.SurnameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#employeeid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmployeeid(SmartBoxCommandsGrammarParser.EmployeeidContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#role}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRole(SmartBoxCommandsGrammarParser.RoleContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#turnsentence}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTurnsentence(SmartBoxCommandsGrammarParser.TurnsentenceContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#turnparams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTurnparams(SmartBoxCommandsGrammarParser.TurnparamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#numofturns}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumofturns(SmartBoxCommandsGrammarParser.NumofturnsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#turns}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTurns(SmartBoxCommandsGrammarParser.TurnsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#turn}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTurn(SmartBoxCommandsGrammarParser.TurnContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#missionsentence}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMissionsentence(SmartBoxCommandsGrammarParser.MissionsentenceContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#missionparams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMissionparams(SmartBoxCommandsGrammarParser.MissionparamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#missionid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMissionid(SmartBoxCommandsGrammarParser.MissionidContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#missiondate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMissiondate(SmartBoxCommandsGrammarParser.MissiondateContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#missiontime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMissiontime(SmartBoxCommandsGrammarParser.MissiontimeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#shipname}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShipname(SmartBoxCommandsGrammarParser.ShipnameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#pilotname}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPilotname(SmartBoxCommandsGrammarParser.PilotnameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#firstname}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFirstname(SmartBoxCommandsGrammarParser.FirstnameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#secondname}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSecondname(SmartBoxCommandsGrammarParser.SecondnameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(SmartBoxCommandsGrammarParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#destination}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDestination(SmartBoxCommandsGrammarParser.DestinationContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#mmsi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMmsi(SmartBoxCommandsGrammarParser.MmsiContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#missionstateacksentence}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMissionstateacksentence(SmartBoxCommandsGrammarParser.MissionstateacksentenceContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#date}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDate(SmartBoxCommandsGrammarParser.DateContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#year}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitYear(SmartBoxCommandsGrammarParser.YearContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#month}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMonth(SmartBoxCommandsGrammarParser.MonthContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#day}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDay(SmartBoxCommandsGrammarParser.DayContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#time}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTime(SmartBoxCommandsGrammarParser.TimeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#hour}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHour(SmartBoxCommandsGrammarParser.HourContext ctx);
	/**
	 * Visit a parse tree produced by {@link SmartBoxCommandsGrammarParser#minutes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinutes(SmartBoxCommandsGrammarParser.MinutesContext ctx);
}