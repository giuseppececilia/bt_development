// Generated from SmartBoxCommandsGrammar.g4 by ANTLR 4.6

	package com.giuseppe.cecilia.grammars;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SmartBoxCommandsGrammarLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, LOGINKEY=2, SEPARATOR=3, OKTOKEN=4, NOKTOKEN=5, CREWKEY=6, ROLETOKEN=7, 
		TURNKEY=8, TURNTK=9, MISSIONKEY=10, MISSIONSTATEACKKEY=11, SLASH=12, INT=13, 
		STRING=14, WS=15;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "LOGINKEY", "SEPARATOR", "OKTOKEN", "NOKTOKEN", "CREWKEY", "ROLETOKEN", 
		"TURNKEY", "TURNTK", "MISSIONKEY", "MISSIONSTATEACKKEY", "SLASH", "INT", 
		"STRING", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "':'", "'LOGIN_RECEIVE'", "'|'", "'OK'", "'NOK'", "'CREW_UPDATE'", 
		null, "'TURN_UPDATE'", null, "'MISSION_SEND'", "'MISSION_STATE_ACK'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, "LOGINKEY", "SEPARATOR", "OKTOKEN", "NOKTOKEN", "CREWKEY", 
		"ROLETOKEN", "TURNKEY", "TURNTK", "MISSIONKEY", "MISSIONSTATEACKKEY", 
		"SLASH", "INT", "STRING", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public SmartBoxCommandsGrammarLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "SmartBoxCommandsGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 14:
			WS_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void WS_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			skip();
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\21\u00d4\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\3\2\3\2\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3"+
		"\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\5\ba\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\5\n\u00a1\n\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3"+
		"\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\r\3\r\3\16\6\16\u00c5\n\16\r\16\16\16\u00c6\3\17\6"+
		"\17\u00ca\n\17\r\17\16\17\u00cb\3\20\6\20\u00cf\n\20\r\20\16\20\u00d0"+
		"\3\20\3\20\2\2\21\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31"+
		"\16\33\17\35\20\37\21\3\2\6\4\2//\61\61\3\2\62;\4\2C\\c|\5\2\f\f\17\17"+
		"\"\"\u00dc\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2"+
		"\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27"+
		"\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\3!\3\2\2"+
		"\2\5#\3\2\2\2\7\61\3\2\2\2\t\63\3\2\2\2\13\66\3\2\2\2\r:\3\2\2\2\17`\3"+
		"\2\2\2\21b\3\2\2\2\23\u00a0\3\2\2\2\25\u00a2\3\2\2\2\27\u00af\3\2\2\2"+
		"\31\u00c1\3\2\2\2\33\u00c4\3\2\2\2\35\u00c9\3\2\2\2\37\u00ce\3\2\2\2!"+
		"\"\7<\2\2\"\4\3\2\2\2#$\7N\2\2$%\7Q\2\2%&\7I\2\2&\'\7K\2\2\'(\7P\2\2("+
		")\7a\2\2)*\7T\2\2*+\7G\2\2+,\7E\2\2,-\7G\2\2-.\7K\2\2./\7X\2\2/\60\7G"+
		"\2\2\60\6\3\2\2\2\61\62\7~\2\2\62\b\3\2\2\2\63\64\7Q\2\2\64\65\7M\2\2"+
		"\65\n\3\2\2\2\66\67\7P\2\2\678\7Q\2\289\7M\2\29\f\3\2\2\2:;\7E\2\2;<\7"+
		"T\2\2<=\7G\2\2=>\7Y\2\2>?\7a\2\2?@\7W\2\2@A\7R\2\2AB\7F\2\2BC\7C\2\2C"+
		"D\7V\2\2DE\7G\2\2E\16\3\2\2\2FG\7E\2\2GH\7Q\2\2HI\7O\2\2IJ\7C\2\2JK\7"+
		"P\2\2KL\7F\2\2LM\7C\2\2MN\7P\2\2NO\7V\2\2Oa\7G\2\2PQ\7O\2\2QR\7C\2\2R"+
		"S\7T\2\2ST\7K\2\2TU\7P\2\2UV\7C\2\2VW\7K\2\2Wa\7Q\2\2XY\7P\2\2YZ\7Q\2"+
		"\2Z[\7U\2\2[\\\7V\2\2\\]\7T\2\2]^\7Q\2\2^_\7O\2\2_a\7Q\2\2`F\3\2\2\2`"+
		"P\3\2\2\2`X\3\2\2\2a\20\3\2\2\2bc\7V\2\2cd\7W\2\2de\7T\2\2ef\7P\2\2fg"+
		"\7a\2\2gh\7W\2\2hi\7R\2\2ij\7F\2\2jk\7C\2\2kl\7V\2\2lm\7G\2\2m\22\3\2"+
		"\2\2no\7O\2\2op\7C\2\2pq\7V\2\2qr\7V\2\2rs\7K\2\2st\7P\2\2t\u00a1\7C\2"+
		"\2uv\7R\2\2vw\7Q\2\2wx\7O\2\2xy\7G\2\2yz\7T\2\2z{\7K\2\2{|\7I\2\2|}\7"+
		"I\2\2}~\7K\2\2~\u00a1\7Q\2\2\177\u0080\7P\2\2\u0080\u0081\7Q\2\2\u0081"+
		"\u0082\7V\2\2\u0082\u0083\7V\2\2\u0083\u0084\7W\2\2\u0084\u0085\7T\2\2"+
		"\u0085\u0086\7P\2\2\u0086\u00a1\7Q\2\2\u0087\u0088\7H\2\2\u0088\u0089"+
		"\7G\2\2\u0089\u008a\7U\2\2\u008a\u008b\7V\2\2\u008b\u008c\7K\2\2\u008c"+
		"\u008d\7X\2\2\u008d\u00a1\7Q\2\2\u008e\u008f\7P\2\2\u008f\u0090\7Q\2\2"+
		"\u0090\u0091\7V\2\2\u0091\u0092\7V\2\2\u0092\u0093\7W\2\2\u0093\u0094"+
		"\7T\2\2\u0094\u0095\7P\2\2\u0095\u0096\7Q\2\2\u0096\u0097\7\"\2\2\u0097"+
		"\u0098\7G\2\2\u0098\u0099\7\"\2\2\u0099\u009a\7H\2\2\u009a\u009b\7G\2"+
		"\2\u009b\u009c\7U\2\2\u009c\u009d\7V\2\2\u009d\u009e\7K\2\2\u009e\u009f"+
		"\7X\2\2\u009f\u00a1\7Q\2\2\u00a0n\3\2\2\2\u00a0u\3\2\2\2\u00a0\177\3\2"+
		"\2\2\u00a0\u0087\3\2\2\2\u00a0\u008e\3\2\2\2\u00a1\24\3\2\2\2\u00a2\u00a3"+
		"\7O\2\2\u00a3\u00a4\7K\2\2\u00a4\u00a5\7U\2\2\u00a5\u00a6\7U\2\2\u00a6"+
		"\u00a7\7K\2\2\u00a7\u00a8\7Q\2\2\u00a8\u00a9\7P\2\2\u00a9\u00aa\7a\2\2"+
		"\u00aa\u00ab\7U\2\2\u00ab\u00ac\7G\2\2\u00ac\u00ad\7P\2\2\u00ad\u00ae"+
		"\7F\2\2\u00ae\26\3\2\2\2\u00af\u00b0\7O\2\2\u00b0\u00b1\7K\2\2\u00b1\u00b2"+
		"\7U\2\2\u00b2\u00b3\7U\2\2\u00b3\u00b4\7K\2\2\u00b4\u00b5\7Q\2\2\u00b5"+
		"\u00b6\7P\2\2\u00b6\u00b7\7a\2\2\u00b7\u00b8\7U\2\2\u00b8\u00b9\7V\2\2"+
		"\u00b9\u00ba\7C\2\2\u00ba\u00bb\7V\2\2\u00bb\u00bc\7G\2\2\u00bc\u00bd"+
		"\7a\2\2\u00bd\u00be\7C\2\2\u00be\u00bf\7E\2\2\u00bf\u00c0\7M\2\2\u00c0"+
		"\30\3\2\2\2\u00c1\u00c2\t\2\2\2\u00c2\32\3\2\2\2\u00c3\u00c5\t\3\2\2\u00c4"+
		"\u00c3\3\2\2\2\u00c5\u00c6\3\2\2\2\u00c6\u00c4\3\2\2\2\u00c6\u00c7\3\2"+
		"\2\2\u00c7\34\3\2\2\2\u00c8\u00ca\t\4\2\2\u00c9\u00c8\3\2\2\2\u00ca\u00cb"+
		"\3\2\2\2\u00cb\u00c9\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\36\3\2\2\2\u00cd"+
		"\u00cf\t\5\2\2\u00ce\u00cd\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0\u00ce\3\2"+
		"\2\2\u00d0\u00d1\3\2\2\2\u00d1\u00d2\3\2\2\2\u00d2\u00d3\b\20\2\2\u00d3"+
		" \3\2\2\2\b\2`\u00a0\u00c6\u00cb\u00d0\3\3\20\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}