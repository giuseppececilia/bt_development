package com.giuseppe.cecilia.grammars;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

public class TestCase {

	@Test
	public void testLoginOK() {

		ANTLRInputStream input;

		input = new ANTLRInputStream("LOGIN_RECEIVE|OK");
		SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);
		// parser.root();
		ParseTree tree = parser.root();

		System.out.println(tree.toStringTree(parser));

	}

	@Test
	public void testLoginNOK() {

		ANTLRInputStream input;

		input = new ANTLRInputStream("LOGIN_RECEIVE|NOK");
		SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);
		// parser.root();
		ParseTree tree = parser.root();

		System.out.println(tree.toStringTree(parser));

	}

	@Test
	public void testMission() {

		ANTLRInputStream input;

		input = new ANTLRInputStream(
				"MISSION_SEND|123|2017/11/21|1:05|BELLAVITA|PEPPINO NAVIGANTE|ENTRATA|BANCHINA|1234567");
		SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);

		ParseTree tree = parser.root();

		System.out.println(tree.toStringTree(parser));

	}

	@Test
	public void testCrew() {

		ANTLRInputStream input;
		input = new ANTLRInputStream("CREW_UPDATE|15|pippo|PEPPE|123453|COMANDANTE|mario|esposito|12345|NOSTROMO");

		SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);
		// parser.root();
		ParseTree tree = parser.root();

		System.out.println(tree.toStringTree(parser));

	}

	@Test
	public void testTurn() {

		ANTLRInputStream input;
		input = new ANTLRInputStream("TURN_UPDATE|15|MATTINA|NOTTURNO|NOTTURNO E FESTIVO");

		SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);
		// parser.root();
		ParseTree tree = parser.root();

		System.out.println(tree.toStringTree(parser));

	}

	@Test
	public void testMissionStateAck() {

		ANTLRInputStream input;
		input = new ANTLRInputStream("MISSION_STATE_ACK|12345");

		SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);
		// parser.root();
		ParseTree tree = parser.root();

		System.out.println(tree.toStringTree(parser));

	}

}
