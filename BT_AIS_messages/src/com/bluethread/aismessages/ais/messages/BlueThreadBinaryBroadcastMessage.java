/*
 * AISMessages
 * - a java-based library for decoding of AIS messages from digital VHF radio traffic related
 * to maritime navigation and safety in compliance with ITU 1371.
 * 
 * (C) Copyright 2011-2013 by S-Consult ApS, DK31327490, http://s-consult.dk, Denmark.
 * 
 * Released under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * For details of this license see the nearby LICENCE-full file, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 * or send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA.
 * 
 * NOT FOR COMMERCIAL USE!
 * Contact sales@s-consult.dk to obtain a commercially licensed version of this software.
 * 
 */

package com.bluethread.aismessages.ais.messages;




import static com.bluethread.aismessages.ais.Decoders.FLOAT_DECODER;
import static com.bluethread.aismessages.ais.Decoders.UNSIGNED_INTEGER_DECODER;

import com.bluethread.aismessages.nmea.messages.NMEAMessage;

/**
 * broadcast message with unspecified binary payload. The St. Lawrence Seaway
 * AIS system and the USG PAWSS system use this payload for local extension
 * messages. It is variable in length up to a maximum of 1008 bits (up to 5
 * AIVDM sentence payloads).
 * 
 * @author blue thread - navigation technologies
 * 
 */
@SuppressWarnings("serial")
public class BlueThreadBinaryBroadcastMessage extends BinaryBroadcastMessage {

    public BlueThreadBinaryBroadcastMessage(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    protected BlueThreadBinaryBroadcastMessage(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    protected void checkAISMessage() {
    }

    @SuppressWarnings("unused")
	public Integer getSequenceId() {
        //return getDecodedValue(() -> functionalId, value -> functionalId = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.apply(getBits(52, 56)));
    	return getDecodedValue(() -> sequenceId, value -> sequenceId = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.apply(getBits(66, 68)));
	}
    
    @SuppressWarnings("unused")
   	public Integer getTransactionId() {
           //return getDecodedValue(() -> functionalId, value -> functionalId = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.apply(getBits(52, 56)));
       	return getDecodedValue(() -> transactionId, value -> transactionId = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.apply(getBits(68, 76)));
   	}
    
    @SuppressWarnings("unused")
   	public Integer getP1Code() {
           //return getDecodedValue(() -> functionalId, value -> functionalId = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.apply(getBits(52, 56)));
       	return getDecodedValue(() -> p1Code, value -> p1Code = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.apply(getBits(76, 84)));
   	}
    
    @SuppressWarnings("unused")
   	public Float getP1Value() {
           //return getDecodedValue(() -> functionalId, value -> functionalId = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.apply(getBits(52, 56)));
       	return getDecodedValue(() -> p1Value, value -> p1Value = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(84, 104)));
   	}
 
    @SuppressWarnings("unused")
   	public Integer getP2Code() {
           //return getDecodedValue(() -> functionalId, value -> functionalId = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.apply(getBits(52, 56)));
       	return getDecodedValue(() -> p2Code, value -> p2Code = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.apply(getBits(104, 112)));
   	}
    
    @SuppressWarnings("unused")
   	public Float getP2Value() {
           //return getDecodedValue(() -> functionalId, value -> functionalId = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.apply(getBits(52, 56)));
       	return getDecodedValue(() -> p2Value, value -> p2Value = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(112, 132)));
   	}
    @Override
    public String toString() {
        return "BlueThreadBinaryBroadcastMessage{" +
                "messageType=" + getMessageType() +
                ", designatedAreaCode=" + getDesignatedAreaCode() +
                ", functionalId=" + getFunctionalId() +
                ", messageLinkageId=" + getMessageLinkageId() +
                ", sequenceId=" + getSequenceId() +
                ", transactionId=" + getTransactionId() +
                ", p1Code=" + getP1Code() +
                ", p1Value=" + getP1Value() +
                ", p2Code=" + getP2Code() +
                ", p2Value=" + getP2Value() +
                ", binaryData='" + getBinaryData() + '\'' +
                "} " + super.toString();
    }

    private transient Integer sequenceId;
    private transient Integer transactionId;
    private transient Integer p1Code;
    private transient Float p1Value;
    private transient Integer p2Code;
    private transient Float p2Value;
    
}
