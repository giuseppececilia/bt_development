package com.bluethread.ais.messages.test;

import org.junit.Test;

import com.bluethread.ais.messages.BtAisAsmLoginSend;
import com.bluethread.ais.messages.BtAisAsm;

import dk.dma.ais.binary.SixbitEncoder;
import dk.dma.ais.binary.SixbitException;
import dk.dma.ais.message.AisMessage;
import dk.dma.ais.message.AisMessage6;
import dk.dma.ais.message.AisMessage8;
import dk.dma.ais.message.AisMessageException;
import dk.dma.ais.sentence.Abm;
import dk.dma.ais.sentence.Bbm;
import dk.dma.ais.sentence.SentenceException;
import dk.dma.ais.sentence.Vdm;
import org.junit.Assert;

public class BtAisAsmLoginSendTC {

	@Test
	public void testAisMsg6() {

		BtAisAsmLoginSend btAsm = new BtAisAsmLoginSend();

		btAsm.setPIN(5856);

		SixbitEncoder encoder = btAsm.getEncoded();

		try {
			System.out.println("1.ASM login Encoded:" + encoder.encode());
		} catch (SixbitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		AisMessage6 aisMessage = new AisMessage6();
		aisMessage.setAppMessage(btAsm);
		aisMessage.setDestination(2470100);
		aisMessage.setUserId(244884121);
		aisMessage.setRetransmit(0);
		aisMessage.setRepeat(0);
		try {
			System.out.println("2.LoginMessage Encoded:" + aisMessage.getEncoded().encode());
		} catch (SixbitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Abm abm = new Abm();

		try {
			abm.setBinaryData(aisMessage);
			abm.setTalker("AI");
			abm.setTotal(1);
			abm.setChannel(Character.toUpperCase('a'));
			abm.setSequence(0);
			abm.setNum(1);

		} catch (SixbitException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		System.out.println("3.ABM: " + abm.getEncoded());

		Vdm vdm = new Vdm();
		try {
			vdm.setTalker("AI");
			vdm.setMessageData(aisMessage);
			vdm.setNum(1);
			vdm.setSequence(0);
			vdm.setTotal(1);
			vdm.setChannel(Character.toUpperCase('a'));

			System.out.println("4.VDM:" + vdm.getEncoded());

		} catch (SixbitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Vdm vdm2 = new Vdm();
		try {
			vdm2.parse(vdm.getEncoded());

			AisMessage6 msg = (AisMessage6) AisMessage.getInstance(vdm2);

			BtAisAsm asm = (BtAisAsm) msg.getApplicationMessage();

			System.out.println("5.ASM content has code:" + asm.getMessageCode());
			System.out.println("6.PIN:" + ((BtAisAsmLoginSend) asm).getPIN());

			Assert.assertEquals(99999, ((BtAisAsmLoginSend) asm).getPIN());

		} catch (SentenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AisMessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SixbitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void testAisMsg8() {

		BtAisAsmLoginSend btAsm = new BtAisAsmLoginSend();

		btAsm.setPIN(9999);

		SixbitEncoder encoder = btAsm.getEncoded();

		try {
			System.out.println("ASM login Encoded:" + encoder.encode());
		} catch (SixbitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		AisMessage8 aisMessage = new AisMessage8();
		aisMessage.setAppMessage(btAsm);
		aisMessage.setUserId(219015063);
		aisMessage.setRepeat(0);
		try {
			System.out.println("LoginMessage Encoded:" + aisMessage.getEncoded().encode());
		} catch (SixbitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Abm abm = new Abm();

		try {
			abm.setBinaryData(aisMessage);
			abm.setTalker("AI");
			abm.setTotal(1);
			abm.setChannel(null);
			abm.setSequence(0);
			abm.setNum(1);
			abm.setChannel(Character.toUpperCase('b'));

		} catch (SixbitException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		System.out.println("ABM: " + abm.getEncoded());

		Vdm vdm = new Vdm();
		try {
			vdm.setTalker("AI");
			vdm.setMessageData(aisMessage);
			vdm.setNum(1);
			vdm.setSequence(0);
			vdm.setTotal(1);
			vdm.setChannel(Character.toUpperCase('b'));

			System.out.println("VDM:" + vdm.getEncoded());

		} catch (SixbitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Vdm vdm2 = new Vdm();
		try {
			vdm2.parse(vdm.getEncoded());

			AisMessage8 msg = (AisMessage8) AisMessage.getInstance(vdm2);

			BtAisAsm asm = (BtAisAsm) msg.getApplicationMessage();

			System.out.println("ASM content has code:" + asm.getMessageCode());
			System.out.println("PIN:" + ((BtAisAsmLoginSend) asm).getPIN());

			Assert.assertEquals(9999, ((BtAisAsmLoginSend) asm).getPIN());

		} catch (SentenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AisMessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SixbitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	
	@Test
	public void testBBM(){


		BtAisAsmLoginSend btAsm = new BtAisAsmLoginSend();

		btAsm.setPIN(11111);

		SixbitEncoder encoder = btAsm.getEncoded();

		try {
			System.out.println("ASM login Encoded:" + encoder.encode());
		} catch (SixbitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		AisMessage8 aisMessage = new AisMessage8();
		aisMessage.setAppMessage(btAsm);
		aisMessage.setUserId(2470100);
		aisMessage.setRepeat(0);
		try {
			System.out.println("LoginMessage Encoded:" + aisMessage.getEncoded().encode());
		} catch (SixbitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Bbm bbm = new Bbm();

		try {
			bbm.setBinaryData(aisMessage);
			bbm.setTalker("AI");
			bbm.setTotal(1);
			bbm.setChannel(null);
			bbm.setSequence(0);
			bbm.setNum(1);
			bbm.setChannel(null);

		} catch (SixbitException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		System.out.println("BBM: " + bbm.getEncoded());

		Vdm vdm = new Vdm();
		try {
			vdm.setTalker("AI");
			vdm.setMessageData(aisMessage);
			vdm.setNum(1);
			vdm.setSequence(0);
			vdm.setTotal(1);
			vdm.setChannel(Character.toUpperCase('b'));

			System.out.println("VDM:" + vdm.getEncoded());

		} catch (SixbitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Vdm vdm2 = new Vdm();
		try {
			vdm2.parse(vdm.getEncoded());

			AisMessage8 msg = (AisMessage8) AisMessage.getInstance(vdm2);

			BtAisAsm asm = (BtAisAsm) msg.getApplicationMessage();

			System.out.println("ASM content has code:" + asm.getMessageCode());
			System.out.println("PIN:" + ((BtAisAsmLoginSend) asm).getPIN());

			Assert.assertEquals(9999, ((BtAisAsmLoginSend) asm).getPIN());

		} catch (SentenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AisMessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SixbitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	
	}

}
