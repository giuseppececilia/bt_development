package com.bluethread.ais.messages.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.bluethread.ais.messages.BtAisAsm;
import com.bluethread.ais.messages.BtAisAsm.Turn;
import com.bluethread.ais.messages.BtAisAsmTurnUpdate;
import com.bluethread.ais.messages.MaxNumberOfItemsException;

import dk.dma.ais.binary.SixbitEncoder;
import dk.dma.ais.binary.SixbitException;
import dk.dma.ais.message.AisMessage;
import dk.dma.ais.message.AisMessage6;
import dk.dma.ais.message.AisMessageException;
import dk.dma.ais.sentence.SentenceException;
import dk.dma.ais.sentence.Vdm;

public class BtAisAsmTurnUpdateTC {

	@Test
	public void test() {
		
		BtAisAsmTurnUpdate btAsm = new BtAisAsmTurnUpdate();
		List<Turn> turns = new ArrayList<Turn>();
		turns.add(BtAisAsm.Turn.MATTINA);
		turns.add(BtAisAsm.Turn.FESTIVO);
		turns.add(BtAisAsm.Turn.POMERIGGIO);
		turns.add(BtAisAsm.Turn.NOTTURNO);
		turns.add(BtAisAsm.Turn.NOTTURNO_FESTIVO);
		
		try {
			btAsm.setTurns(turns);
		} catch (MaxNumberOfItemsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		SixbitEncoder encoder = btAsm.getEncoded();
		try {
			System.out.println("ASM Encoded:"+encoder.encode());
		} catch (SixbitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		AisMessage6 aisMsg = new AisMessage6();
		aisMsg.setAppMessage(btAsm);
		
		aisMsg.setUserId(213213213);
		aisMsg.setDestination(123123123);
		aisMsg.setRepeat(0);
		aisMsg.setRetransmit(0);
		aisMsg.setSeqNum(1);
		
		try {
			System.out.println("AIS message encoded: "+aisMsg.getEncoded().encode());
		} catch (SixbitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Vdm vdm = new Vdm();
		
		try {
			vdm.setMessageData(aisMsg);
			vdm.setChannel(Character.toUpperCase('a'));
			vdm.setTalker("AR");
			vdm.setTotal(1);
			vdm.setSequence(0);
			vdm.setNum(1);
			
			System.out.println("VDM encoded:"+vdm.getEncoded());
			
			
			Vdm vdm2 = new Vdm();
			vdm2.parse(vdm.getEncoded());
			AisMessage6 msg = (AisMessage6)AisMessage.getInstance(vdm2);
			BtAisAsmTurnUpdate asm = (BtAisAsmTurnUpdate)msg.getApplicationMessage();
			List<Turn> listOfturns = asm.getTurns();
			
			Turn[] arrayOfTurns = new Turn[listOfturns.size()];
			
			listOfturns.toArray(arrayOfTurns);
			
			for(int j=0; j<arrayOfTurns.length;j++){
				System.out.println(arrayOfTurns[j].getOrdinal());
				
			}
			
			
			System.out.println("Turns: "+asm.getNumberOfTurns());
		} catch (SixbitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SentenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AisMessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	}

}
