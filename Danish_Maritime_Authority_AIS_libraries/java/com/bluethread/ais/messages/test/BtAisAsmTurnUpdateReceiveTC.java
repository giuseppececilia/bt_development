package com.bluethread.ais.messages.test;

import org.junit.Test;

import com.bluethread.ais.messages.BtAisAsmLoginReceive;
import com.bluethread.ais.messages.BtAisAsmLoginSend;
import com.bluethread.ais.messages.BtAisAsmTurnUpdateReceive;

import dk.dma.ais.binary.SixbitEncoder;
import dk.dma.ais.binary.SixbitException;
import dk.dma.ais.message.AisMessage6;
import dk.dma.ais.sentence.Abm;

public class BtAisAsmTurnUpdateReceiveTC {

		
	
	@Test
	public void testTurn() {

		BtAisAsmTurnUpdateReceive btAsm = new BtAisAsmTurnUpdateReceive();

		btAsm.setAnswer(true);
		
		AisMessage6 aisMessage = new AisMessage6();
		aisMessage.setAppMessage(btAsm);
		aisMessage.setDestination(219015063);
		aisMessage.setUserId(123456789);
		aisMessage.setRetransmit(0);
		aisMessage.setRepeat(0);

		try {
			System.out.println("TurnUpdateReceive Encoded:" + aisMessage.getEncoded().encode());
		} catch (SixbitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Abm abm = new Abm();

		try {
			abm.setBinaryData(aisMessage);
			abm.setTalker("AI");
			abm.setTotal(1);
			abm.setChannel(null);
			abm.setSequence(0);
			abm.setNum(1);

		} catch (SixbitException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		System.out.println("ABM: " + abm.getEncoded());
		
	}

}
