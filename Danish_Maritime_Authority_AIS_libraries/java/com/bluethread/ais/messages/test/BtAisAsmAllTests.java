package com.bluethread.ais.messages.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BtAisAsmCrewUpdateTC.class })
public class BtAisAsmAllTests {

}
