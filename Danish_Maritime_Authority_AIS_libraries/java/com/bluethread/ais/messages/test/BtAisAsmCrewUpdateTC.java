package com.bluethread.ais.messages.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.bluethread.ais.messages.BtAisAsm;
import com.bluethread.ais.messages.BtAisAsm.Turn;
import com.bluethread.ais.messages.BtAisAsmLoginSend;
import com.bluethread.ais.messages.MaxNumberOfItemsException;
import com.bluethread.ais.messages.BtAisAsmCrewUpdate;
import com.bluethread.ais.messages.BtAisAsmCrewUpdate.Addict;

import dk.dma.ais.binary.SixbitEncoder;
import dk.dma.ais.binary.SixbitException;
import dk.dma.ais.message.AisMessage;
import dk.dma.ais.message.AisMessage6;
import dk.dma.ais.message.AisMessageException;
import dk.dma.ais.sentence.SentenceException;
import dk.dma.ais.sentence.Vdm;

public class BtAisAsmCrewUpdateTC {

	@Test
	public void test() {

		List<Addict> addicts = new ArrayList<Addict>();
	

		BtAisAsmCrewUpdate.Addict addict1 = new BtAisAsmCrewUpdate.Addict();
		addict1.setRole(BtAisAsm.CrewRole.COMANDANTE);
		addict1.setID(12345);
		addict1.setName("PIPPO");
		addict1.setSurname("ESPOSITO");
		addicts.add(addict1);

		BtAisAsmCrewUpdate.Addict addict2 = new BtAisAsmCrewUpdate.Addict();
		addict2.setRole(BtAisAsm.CrewRole.MARINAIO);
		addict2.setID(43552442);
		addict2.setName("RAFFAELE");
		addict2.setSurname("CIRILLO");
		addicts.add(addict2);

		BtAisAsmCrewUpdate.Addict addict3 = new BtAisAsmCrewUpdate.Addict();
		addict3.setRole(BtAisAsm.CrewRole.MARINAIO);
		addict3.setID(23315553);
		addict3.setName("CIRO");
		addict3.setSurname("RUSSO");
		addicts.add(addict3);
	
		BtAisAsmCrewUpdate.Addict addict4 = new BtAisAsmCrewUpdate.Addict();
		addict4.setRole(BtAisAsm.CrewRole.MARINAIO);
		addict4.setID(8483774);
		addict4.setName("VINCENZO");
		addict4.setSurname("SCOGNAMIGLIO");
		addicts.add(addict4);

		BtAisAsmCrewUpdate.Addict addict5 = new BtAisAsmCrewUpdate.Addict();
		addict5.setRole(BtAisAsm.CrewRole.MARINAIO);
		addict5.setID(67108863);
		addict5.setName("MARIO");
		addict5.setSurname("GARGIULO");
		addicts.add(addict5);
		
		System.out.println(addicts.toString());
		BtAisAsmCrewUpdate btAsm = new BtAisAsmCrewUpdate();

		
		
		try {
			btAsm.setAddicts(addicts);
		} catch (MaxNumberOfItemsException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		AisMessage6 aisMsg = new AisMessage6();
		aisMsg.setAppMessage(btAsm);
		aisMsg.setDestination(244660879);
		aisMsg.setUserId(219015063);
		aisMsg.setRetransmit(0);
		aisMsg.setRepeat(0);

		SixbitEncoder encoder = aisMsg.getEncoded();

		try {
			String encodedCalculated = encoder.encode();

			//Assert.assertEquals("2@JoVRa001tr1", encodedCalculated);

			System.out.println("Encoded:" + encodedCalculated);
		} catch (SixbitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Vdm vdm = new Vdm();
		try {
			vdm.setTalker("AR");
			vdm.setMessageData(aisMsg);
			vdm.setNum(1);
			vdm.setSequence(0);
			vdm.setTotal(1);
			vdm.setChannel(Character.toUpperCase('a'));
			System.out.println("VDM encoded:"+vdm.getEncoded());
			
		} catch (SixbitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Vdm vdm2 = new Vdm();
		
			try {
				vdm2.parse(vdm.getEncoded());
				AisMessage6 msg = (AisMessage6) AisMessage.getInstance(vdm2);

				BtAisAsmCrewUpdate asm = (BtAisAsmCrewUpdate) msg.getApplicationMessage();
				
				List<Addict> crew = asm.getAddicts();
				
				for(Addict member:crew){
					System.out.println("***************************************");
					System.out.println(member.getID());
					System.out.println(member.getName());
					System.out.println(member.getSurname());
				}

				System.out.println("ASM content has code:" + asm.getMessageCode());
				
				
			} catch (SentenceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (AisMessageException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SixbitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
		
		
			
		
		
	}

}
