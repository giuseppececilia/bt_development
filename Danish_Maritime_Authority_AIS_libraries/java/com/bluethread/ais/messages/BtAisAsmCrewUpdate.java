/**
 *
 */
package com.bluethread.ais.messages;

import java.util.ArrayList;
import java.util.List;

import dk.dma.ais.binary.BinArray;
import dk.dma.ais.binary.SixbitEncoder;
import dk.dma.ais.binary.SixbitException;

/**
 * @author Giuseppe Cecilia
 * Maximum number of addicts 5
 * maximum length of name and surname 12 characters
 *
 */
public class BtAisAsmCrewUpdate extends BtAisAsm {

	
	private List<Addict> listOfAddicts;
	

	public BtAisAsmCrewUpdate() {
		super();
		setMsg(BtAisAsm.BtAisMessageEnum.CREW_UPDATE);
		listOfAddicts = new ArrayList<Addict>();
		
	}

	public BtAisAsmCrewUpdate(BinArray array) throws SixbitException {
		//super(array);
		this();
		parse(array);
		
	}

	/*
	 * Crew ASM Enums
	 */


	public int getNumberOfAddicts() {
		return listOfAddicts.size();
	}

	

	public List<Addict> getAddicts() {
		return listOfAddicts;
	}

	public void setAddicts(List<Addict> addicts)throws MaxNumberOfItemsException {
		if (addicts.size()>5) throw new MaxNumberOfItemsException();
		else
			this.listOfAddicts = addicts;
	}

	public static class Addict {


		public BtAisAsm.CrewRole getRole() {
			return role;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getSurname() {
			return surname;
		}

		public void setSurname(String surname) {
			this.surname = surname;
		}

		public void setRole(BtAisAsm.CrewRole role) {
			this.role = role;
		}

		public long getID() {
			return ID;
		}

		public void setID(long iD) {
			ID = iD;
		}

		private BtAisAsm.CrewRole role;
		private long ID;
		
		private String name;
		private String surname;
	}
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * dk.dma.ais.message.binary.AisApplicationMessage#parse(dk.dma.ais.binary.
	 * BinArray)
	 */

	@Override
	public void parse(BinArray binArray) throws SixbitException {
		// TODO Auto-generated method stub
		// Superclass parses field messageCode
		super.parse(binArray);
		int numberOfAddicts = (int) binArray.getVal(3);
		for (int j = 0; j < numberOfAddicts; j++) {
			Addict newAddict = new Addict();
			newAddict.setRole(BtAisAsm.CrewRole.getRole((int) binArray.getVal(4)));
			newAddict.setID(binArray.getVal(26));
			newAddict.setName(binArray.getString(12));
			newAddict.setSurname(binArray.getString(12));
			listOfAddicts.add(newAddict);
		}
	}

	@Override
	public SixbitEncoder getEncoded() {
		// TODO Auto-generated method stub
		Addict[] addicts = new Addict[listOfAddicts.size()];
		listOfAddicts.toArray(addicts);
		SixbitEncoder encoder = super.getEncoded();
		encoder.addVal(listOfAddicts.size(), 3);
		for(int i=0; i<listOfAddicts.size();i++){
			encoder.addVal(addicts[i].getRole().getOrdinal(), 4);
			encoder.addVal(addicts[i].getID(), 26);
			encoder.addString(addicts[i].getName(),12);
			encoder.addString(addicts[i].getSurname(),12);
		}
		return encoder;
	}

}
