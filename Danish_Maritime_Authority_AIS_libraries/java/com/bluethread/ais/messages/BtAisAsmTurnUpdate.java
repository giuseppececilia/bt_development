/**
 *
 */
package com.bluethread.ais.messages;

import java.util.ArrayList;
import java.util.List;

import dk.dma.ais.binary.BinArray;
import dk.dma.ais.binary.SixbitEncoder;
import dk.dma.ais.binary.SixbitException;

/**
 * @author Giuseppe Cecilia
 * Maximum number of addicts 5
 * maximum length of name and surname 12 characters
 *
 */
public class BtAisAsmTurnUpdate extends BtAisAsm {

	
	private List<Turn> listOfTurns;
	

	public BtAisAsmTurnUpdate() {
		super();
		setMsg(BtAisAsm.BtAisMessageEnum.TURN_UPDATE);
		listOfTurns = new ArrayList<Turn>();
		
	}

	public BtAisAsmTurnUpdate(BinArray array) throws SixbitException {
		//super(array);
		this();
		parse(array);
		
	}

	/*
	 * Crew ASM Enums
	 */


	public int getNumberOfTurns() {
		return listOfTurns.size();
	}

	

	public List<Turn> getTurns() {
		return listOfTurns;
	}

	public void setTurns(List<Turn> turns)throws MaxNumberOfItemsException {
		if (turns.size()>5) throw new MaxNumberOfItemsException();
		else
			this.listOfTurns = turns;
	}

	
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * dk.dma.ais.message.binary.AisApplicationMessage#parse(dk.dma.ais.binary.
	 * BinArray)
	 */

	@Override
	public void parse(BinArray binArray) throws SixbitException {
		// TODO Auto-generated method stub
		// Superclass parses field messageCode
		super.parse(binArray);
		int numberOfTurns = (int) binArray.getVal(3);
		for (int j = 0; j < numberOfTurns; j++) {
			Turn t = Turn.getTurn((int)binArray.getVal(3));
			listOfTurns.add(t);
		}
	}

	@Override
	public SixbitEncoder getEncoded() {
		// TODO Auto-generated method stub
		Turn[] turns = new Turn[listOfTurns.size()];
		listOfTurns.toArray(turns);
		SixbitEncoder encoder = super.getEncoded();
		encoder.addVal(listOfTurns.size(), 3);
		for(int i=0; i<listOfTurns.size();i++){
			encoder.addVal(turns[i].getOrdinal(), 3);
		}
		return encoder;
	}

}
