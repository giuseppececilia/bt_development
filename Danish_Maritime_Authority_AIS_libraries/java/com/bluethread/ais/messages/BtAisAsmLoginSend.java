package com.bluethread.ais.messages;

import dk.dma.ais.binary.BinArray;
import dk.dma.ais.binary.SixbitEncoder;
import dk.dma.ais.binary.SixbitException;

public class BtAisAsmLoginSend extends BtAisAsm {

	private long PIN;

	public BtAisAsmLoginSend() {
		super();
		setMsg(BtAisAsm.BtAisMessageEnum.LOGIN_SEND);
	}

	public BtAisAsmLoginSend(BinArray array) throws SixbitException {
		this();
		parse(array);
	}

	@Override
	public SixbitEncoder getEncoded() {
		// TODO Auto-generated method stub
		SixbitEncoder encoder = super.getEncoded();
		encoder.addVal(PIN, 17);
		return encoder;

	}

	@Override
	public void parse(BinArray binArray) throws SixbitException {
		// TODO Auto-generated method stub
		super.parse(binArray);
		PIN = binArray.getVal(17);
	}

	public long getPIN() {
		return PIN;
	}

	public void setPIN(long pIN) {
		PIN = pIN;
	}

}
