package com.bluethread.ais.messages;

import dk.dma.ais.binary.BinArray;
import dk.dma.ais.binary.SixbitEncoder;
import dk.dma.ais.binary.SixbitException;

public class BtAisAsmServiceRes extends BtAisAsm {

	/*
	 * TODO add missing fields: tonnsl, tonnsn, flag, typeofship, larg, length,
	 * drafttons, displacement, deadweight,
	 */
	private int tugreportId;
	private BtAisAsm.TugRole tugRole;

	public BtAisAsmServiceRes() {
		super();
		setMsg(BtAisAsm.BtAisMessageEnum.SERVICE_RES);

	}

	public BtAisAsmServiceRes(BinArray array) throws SixbitException {
		this();
		parse(array);

	}

	@Override
	public SixbitEncoder getEncoded() {
		// TODO Auto-generated method stub
		SixbitEncoder encoder = super.getEncoded();

		return encoder;
	}

	@Override
	public void parse(BinArray binArray) throws SixbitException {
		// TODO Auto-generated method stub
		super.parse(binArray);
	}

}
