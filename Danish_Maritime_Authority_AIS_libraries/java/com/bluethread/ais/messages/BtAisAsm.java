/**
 *
 */
package com.bluethread.ais.messages;

import dk.dma.ais.binary.BinArray;
import dk.dma.ais.binary.SixbitEncoder;
import dk.dma.ais.binary.SixbitException;
import dk.dma.ais.message.binary.AisApplicationMessage;

/**
 * @author Giuseppe Cecilia
 *
 */
public class BtAisAsm extends AisApplicationMessage {

	public static final int CREW_UPDATE = 0;
	public static final int CREW_UPDATE_RECEIVE = 1;
	public static final int SERVICE_REQ = 2;
	public static final int SERVICE_RES = 3;
	public static final int LOGIN_SEND = 4;
	public static final int LOGIN_RECEIVE = 5;
	public static final int TURN_UPDATE = 6;
	public static final int TURN_UPDATE_RECEIVE = 7;
	
	
	public static final int UNSPECIFIED = -1;

	public static enum BtAisMessageEnum {
		CREW_UPDATE(0), CREW_UPDATE_RECEIVE(1),
		SERVICE_REQ(2), SERVICE_RES(3), 
		LOGIN_SEND(4), LOGIN_RECEIVE(5), 
		TURN_UPDATE(6),TURN_UPDATE_RECEIVE(7), 
		UNSPECIFIED(-1);

		private BtAisMessageEnum(int val) {
			messageCode = val;
		}

		private int messageCode;

		public int getOrdinal() {
			return messageCode;
		}

	}

	public static enum CrewRole {
		COMANDANTE(0), NOSTROMO(1), MARINAIO(2), CAPO_MACCHINE(3), UNDEFINED(-1);
		private int role;

		private CrewRole(int r) {
			role = r;
		}

		public int getOrdinal() {
			return role;
		}

		public static CrewRole getRole(int code) {
			switch (code) {
			case 0:
				return CrewRole.COMANDANTE;
			case 1:
				return CrewRole.NOSTROMO;
			case 2:
				return CrewRole.MARINAIO;
			case 3:
				return CrewRole.CAPO_MACCHINE;
			default:
				return CrewRole.UNDEFINED;

			}
		}
	}

	public static enum Turn {
		MATTINA(0), POMERIGGIO(1), NOTTURNO_FESTIVO(2), NOTTURNO(3), FESTIVO(4), UNDEFINED(-1);
		private int code;

		private Turn(int c) {
			code = c;
		}

		public int getOrdinal() {
			return code;
		}

		public static Turn getTurn(int code) {
			switch (code) {
			case 0:
				return Turn.MATTINA;
			case 1:
				return Turn.POMERIGGIO;
			case 2:
				return Turn.NOTTURNO_FESTIVO;
			case 3:
				return Turn.NOTTURNO;
			case 4:
				return Turn.FESTIVO;
			default:
				return Turn.UNDEFINED;
			}
		}
	}

	public static enum TugRole {
		PRINCIPALE(0), AUSILIARIO(1);

		private int role;

		private TugRole(int val) {
			role = val;
		}

		public int getOrdinal() {
			return role;
		}

	}

	public static enum MissionState {
		ASSEGNATA(0), EVASA(1);

		private int state;

		private MissionState(int val) {
			state = val;
		}

		public int getOrdinal() {
			return state;
		}

	}

	public static enum MissionType {
		ENTRATA(0), USCITA(1);

		private int state;

		private MissionType(int val) {
			state = val;
		}

		public int getOrdinal() {
			return state;
		}

	}

	/**
	 * @param dac
	 * @param fi
	 */

	private static final int BT_DAC = 1;
	private static final int BT_FI = 30;

	private int messageCode;

	public BtAisAsm() {
		super(BT_DAC, BT_FI);
		// TODO Auto-generated constructor stub
	}

	public BtAisAsm(BinArray array) throws SixbitException {
		super(BT_DAC, BT_FI, array);
		// TODO Auto-generated constructor stub
	}

	@Override
	public SixbitEncoder getEncoded() {
		SixbitEncoder encoder = new SixbitEncoder();
		encoder.addVal(messageCode, 4);
		return encoder;
	}

	/*
	 *
	 */
	// public void setMsgCode(int mc) {
	// messageCode = mc;
	// }

	public void setMsg(BtAisMessageEnum msge) {
		if (msge != null)
			messageCode = msge.getOrdinal();
		else
			messageCode = BtAisMessageEnum.UNSPECIFIED.getOrdinal();

	}

	@Override
	public void parse(BinArray binArray) throws SixbitException {
		// TODO Auto-generated method stub
		messageCode = (int) binArray.getVal(4);
	}

	public int getMessageCode() {
		return messageCode;
	}

}
