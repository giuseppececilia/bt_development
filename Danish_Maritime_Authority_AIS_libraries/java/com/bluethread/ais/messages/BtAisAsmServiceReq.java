package com.bluethread.ais.messages;

import dk.dma.ais.binary.BinArray;
import dk.dma.ais.binary.SixbitEncoder;
import dk.dma.ais.binary.SixbitException;

public class BtAisAsmServiceReq extends BtAisAsm {

	private int missionId;
	private BtAisAsm.MissionState missionState;
	private int day;
	private int month;
	private int hour;
	private int minute;
	private String shipName;
	private int shipId;
	private long shipMMSI;
	private String pilotName;
	private BtAisAsm.MissionType missionType;
	private String missionStart;
	private String missionDestination;

	/*
	 * TODO add missing fields: tonnsl, tonnsn, flag, typeofship, larg, length,
	 * drafttons, displacement, deadweight,
	 */
	private int tugreportId;
	private BtAisAsm.TugRole tugRole;

	public BtAisAsmServiceReq() {
		super();
		setMsg(BtAisAsm.BtAisMessageEnum.SERVICE_REQ);

	}

	public BtAisAsmServiceReq(BinArray array) throws SixbitException {
		this();
		parse(array);

	}

	@Override
	public SixbitEncoder getEncoded() {
		// TODO Auto-generated method stub
		SixbitEncoder encoder = super.getEncoded();

		encoder.addVal(missionId, 14);
		encoder.addVal(missionState.getOrdinal(), 3);
		encoder.addVal(month, 4);
		encoder.addVal(day, 5);
		encoder.addVal(hour, 5);
		encoder.addVal(minute, 5);
		encoder.addString(shipName, 10);
		encoder.addVal(shipId, 10);
		encoder.addVal(shipMMSI, 30);
		encoder.addString(pilotName, 10);
		encoder.addVal(missionType.getOrdinal(), 5);
		encoder.addString(missionStart, 10);
		encoder.addString(missionDestination, 10);
		/*
		 * TODO add missing fields: tonnsl, tonnsn, flag, typeofship, larg,
		 * length, drafttons, displacement, deadweight,
		 */

		encoder.addVal(tugreportId, 14);
		encoder.addVal(tugRole.getOrdinal(), 2);

		return encoder;
	}

	@Override
	public void parse(BinArray binArray) throws SixbitException {
		// TODO Auto-generated method stub
		super.parse(binArray);
	}

	public int getMissionId() {
		return missionId;
	}

	public void setMissionId(int missionId) {
		this.missionId = missionId;
	}

	public BtAisAsm.MissionState getMissionState() {
		return missionState;
	}

	public void setMissionState(BtAisAsm.MissionState missionState) {
		this.missionState = missionState;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public int getShipId() {
		return shipId;
	}

	public void setShipId(int shipId) {
		this.shipId = shipId;
	}

	public long getShipMMSI() {
		return shipMMSI;
	}

	public void setShipMMSI(long shipMMSI) {
		this.shipMMSI = shipMMSI;
	}

	public String getPilotName() {
		return pilotName;
	}

	public void setPilotName(String pilotName) {
		this.pilotName = pilotName;
	}

	public BtAisAsm.MissionType getMissionType() {
		return missionType;
	}

	public void setMissionType(BtAisAsm.MissionType missionType) {
		this.missionType = missionType;
	}

	public String getMissionStart() {
		return missionStart;
	}

	public void setMissionStart(String missionStart) {
		this.missionStart = missionStart;
	}

	public String getMissionDestination() {
		return missionDestination;
	}

	public void setMissionDestination(String missionDestination) {
		this.missionDestination = missionDestination;
	}

}
