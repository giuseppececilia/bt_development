package com.bluethread.ais.messages;

import dk.dma.ais.binary.BinArray;
import dk.dma.ais.binary.SixbitEncoder;
import dk.dma.ais.binary.SixbitException;

public class BtAisAsmCrewUpdateReceive extends BtAisAsm {

	private boolean answer;

	public BtAisAsmCrewUpdateReceive() {
		super();
		setMsg(BtAisAsm.BtAisMessageEnum.CREW_UPDATE_RECEIVE);
	}

	public BtAisAsmCrewUpdateReceive(BinArray array) throws SixbitException {
		
		this();
		parse(array);
	}

	@Override
	public SixbitEncoder getEncoded() {
		// TODO Auto-generated method stub
		SixbitEncoder encoder = super.getEncoded();
		if (answer)
			encoder.addVal(1, 1);
		else
			encoder.addVal(0, 1);
		return encoder;

	}

	@Override
	public void parse(BinArray binArray) throws SixbitException {
		// TODO Auto-generated method stub
		super.parse(binArray);
		int value = (int) binArray.getVal(1);
		switch (value) {
		case 0:
			answer = false;
		default:
			answer = true;
		}

	}

	public boolean isAnswer() {
		return answer;
	}

	public void setAnswer(boolean answer) {
		this.answer = answer;
	}

}
