package com.bluethread.mediation.main.controlifc;

import com.bluethread.mediation.types.Event;
import com.bluethread.mediation.types.EventType;

public class Command extends Event {

	private CommandType myType;
	private String payload="";
	
	public Command(CommandType ct) {
		super(EventType.Command);
		// TODO Auto-generated constructor stub
		myType = ct;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}
	
	public String toString(){
		return myType.name()+","+payload;
				
				
	}
	
	public CommandType getCommand(){
		return myType;
	}
	

}
