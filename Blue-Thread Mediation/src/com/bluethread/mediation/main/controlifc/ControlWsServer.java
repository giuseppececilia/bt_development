package com.bluethread.mediation.main.controlifc;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

import javax.websocket.DeploymentException;


import org.glassfish.tyrus.server.Server;

import com.bluethread.mediation.main.MediationMain;



public class ControlWsServer {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ControlWsServer.class
			.getName());
	
	
	private Server controlServer=null;
	private WaitingThread waitingThread=null;
	private int port;
	private Class<?> websocketHandler;
	
	
	
	public ControlWsServer(Properties props){
		
		String controlServerPort = props.getProperty("controlServerPort");
		if(controlServerPort != null)
			port = Integer.parseInt(controlServerPort);
		else port = 8026;
		websocketHandler=MediationMain.class;
		
	}
	public ControlWsServer(Properties props, Class <?> handler){
		this(props);
		websocketHandler = handler;
	}
	
	public ControlWsServer(Class <?> handler){
		port = 8026;
		websocketHandler = handler;
	}
	
	public void startInterface(){
		
		if (waitingThread== null) 
			waitingThread = new WaitingThread();
		waitingThread.start();

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "ControlWsServer", "startInterface()",
					"Started control interface on port:"+port);
		}
	}
	
	public void stopInterface(){
		waitingThread.shutdown();
	}
	
	private class WaitingThread extends Thread{
		/**
		 * Logger for this class
		 */
		private final Logger logger = Logger.getLogger(WaitingThread.class
				.getName());

		private boolean stop = false;
		
		public synchronized  void shutdown(){
			stop = true;
			notifyAll();

			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "WaitingThread", "shutdown()",
						"Control interface shutdown, closing websocket on port:"+port);
			}
		}
		
		public synchronized void run(){
			while (!stop){
				if (controlServer == null){
					controlServer = new Server("localhost",port,"/btmediation",null,websocketHandler);
				}
				try {
					controlServer.start();
				} catch (DeploymentException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					System.exit(-1);
				}
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
			}

			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WaitingThread", "run()",
						"Quitting control interface thread ");
			}
		}
		
	}
	
	public static void main(String [] args){
		ControlWsServer underTest = new ControlWsServer(MediationMain.class);
		underTest.startInterface();
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		underTest.stopInterface();
	}

}
