package com.bluethread.mediation.main.controlifc;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

public class MessageEncoder implements Encoder.Text<Command>{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MessageEncoder.class
			.getName());

	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String encode(Command cmd) throws EncodeException {
		// TODO Auto-generated method stub
		 JAXBContext jaxbContext = null;
	        StringWriter st = null;
	        try {
	            jaxbContext = JAXBContext.newInstance(Command.class);
	 
	            Marshaller marshaller = jaxbContext.createMarshaller();
	            
	     
	            JAXBElement<Command> je = new JAXBElement<Command>(new QName(""), Command.class, cmd);
	            st = new StringWriter();
	            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	            marshaller.marshal(je, st);
	            
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "MessageEncoder", "encode(Command)",
						"Outgoing XML - st=" + st.toString());
			}
	            System.out.println(st.toString());
	            
	 
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	        return st.toString();
	}

}
