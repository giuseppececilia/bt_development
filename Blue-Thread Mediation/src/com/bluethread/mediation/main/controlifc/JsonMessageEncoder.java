package com.bluethread.mediation.main.controlifc;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

public class JsonMessageEncoder implements Encoder.Text<Command>{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JsonMessageEncoder.class
			.getName());

	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String encode(Command cmd) throws EncodeException {
		// TODO Auto-generated method stub
		JsonObjectBuilder job = Json.createObjectBuilder();
		
		job.add("type", cmd.getCommand().ordinal());
		job.add("payload", cmd.getPayload());
		
	        return job.build().toString();
	}

}
