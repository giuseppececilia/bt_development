package com.bluethread.mediation.main.controlifc.test;

import java.util.logging.Logger;
import java.util.logging.Level;

import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.bluethread.mediation.main.controlifc.Command;
import com.bluethread.mediation.main.controlifc.ControlWsServer;
import com.bluethread.mediation.main.controlifc.JsonMessageDecoder;
import com.bluethread.mediation.main.controlifc.JsonMessageEncoder;

@ServerEndpoint(value = "/control",
decoders = {
    JsonMessageDecoder.class,},
encoders = {
    JsonMessageEncoder.class
})

public class ControlIfcTesterServer {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ControlIfcTesterServer.class.getName());
	
	@OnOpen
	public void onOpen(final Session session){
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "ControlIfcTesterServer",
					"onOpen(Session)", "Session opened - session=" + session.toString());
		}
		
	}
	
	@OnMessage
	public void getMessage(final Command cmd, final Session session){
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "MediationMain",
					"getMessage(Command, Session)", "Command received - cmd="
							+ cmd);
		}
	}
	
	
	public static void main (String [] args){
		ControlWsServer server = new ControlWsServer(ControlIfcTesterServer.class);
		server.startInterface();
		try {
			Thread.sleep(180000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
