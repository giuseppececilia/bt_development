package com.bluethread.mediation.main.controlifc.test;


import org.glassfish.tyrus.client.ClientManager;


import javax.websocket.CloseReason;
import javax.websocket.DeploymentException;
import javax.websocket.Session;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Scanner;



public class ControlIfcTesterClient{
	
	public ControlIfcTesterClient(){
		try {
			runClient();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void runClient() throws IOException {

		ClientManager client = ClientManager.createClient();
		Session session = null;
		
		try {
			session = client.connectToServer(ControlIfcTestClient.class, new URI("ws://localhost:8026/btmediation/control"));

			Scanner scanner = new Scanner(System.in);
			System.out.println("Enter text to send or type exit");
			while (scanner.hasNext()) {
				String input = scanner.nextLine();
				if ("exit".equals(input)) {
					break;
				}
			}

		} catch (DeploymentException | URISyntaxException e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen())
				session.close(new CloseReason(CloseReason.CloseCodes.GOING_AWAY, "Bye"));
		}
	}
	
	
	public static void main(String [] args){
		
		ControlIfcTesterClient testClient = new ControlIfcTesterClient();
		
	}


	

}
