package com.bluethread.mediation.main.controlifc.test;

import javax.websocket.EndpointConfig;
import javax.websocket.Session;
import com.bluethread.mediation.main.controlifc.Command;
import com.bluethread.mediation.main.controlifc.CommandType;
import com.bluethread.mediation.main.controlifc.JsonMessageEncoder;
import com.bluethread.mediation.main.controlifc.JsonMessageDecoder;
import javax.websocket.ClientEndpoint;
import javax.websocket.OnOpen;






@ClientEndpoint(
		encoders ={JsonMessageEncoder.class},
		decoders={JsonMessageDecoder.class})
public class ControlIfcTestClient {
	
	@OnOpen
	public void onOpen(Session session, EndpointConfig arg1) {
		
		System.out.println("Joined the server");
		Command cmd = new Command(CommandType.Stop);
		cmd.setPayload("Payload, payload, payload, payload");
		session.getAsyncRemote().sendObject(cmd);
	}

}
