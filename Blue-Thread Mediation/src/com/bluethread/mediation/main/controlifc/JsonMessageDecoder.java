package com.bluethread.mediation.main.controlifc;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

import java.io.StringReader;

import javax.xml.bind.*;

public class JsonMessageDecoder implements Decoder.Text<Command>{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(JsonMessageDecoder.class
			.getName());


	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Command decode(String s) throws DecodeException {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "MessageDecoder", "decode(String)",
					"Incoming XML - s=" + s);
		}

		Command cmd = null;
		JsonObject job = Json.createReader(new StringReader(s)).readObject();
		int cmdType = job.getInt("type");
		cmd = new Command(CommandType.values()[cmdType]);
		String payload = job.getString("payload");
		cmd.setPayload(payload);
		return cmd;
	}

	@Override
	public boolean willDecode(String s) {
		// TODO Auto-generated method stub
		return (s!=null);
	}

}
