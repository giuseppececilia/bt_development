package com.bluethread.mediation.main;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.websocket.CloseReason;

import javax.websocket.OnClose;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.OnMessage;





import com.bluethread.mediation.ais.AisProtocolHandler;
import com.bluethread.mediation.serial.SerialController;
import com.bluethread.mediation.types.Event;
import com.bluethread.mediation.types.ifc.EventsListener;
import com.bluethread.mediation.websocket.client.BackEndServant;
import com.bluethread.mediation.main.controlifc.Command;
import com.bluethread.mediation.main.controlifc.ControlWsServer;
import com.bluethread.mediation.main.controlifc.MessageDecoder;
import com.bluethread.mediation.main.controlifc.MessageEncoder;


@ServerEndpoint(value = "/mediationMain",
    decoders = {
        MessageDecoder.class,},
    encoders = {
        MessageEncoder.class
    })

public class MediationMain implements EventsListener{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MediationMain.class
			.getName());
	
	private Properties configProps;
	private ControlWsServer ctrlIfc;
	private SerialController frontEnd;
	private AisProtocolHandler mediation;
	private BackEndServant backEnd;
	private  static String PROP_FILENAME= "com/bluethread/mediation/main/config.properties";
	
	public MediationMain(){
		initialize();
	}
	public MediationMain(String confFile){
		PROP_FILENAME = confFile;
		initialize();
	}
	
	public void startService(){
		
	}
	
	private void initialize() {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "MediationMain", "initialize()",
					"Blue Thread Mediation - initializing\n");
		}

		configProps = new Properties();
		InputStream inputProperties = Thread.currentThread().getContextClassLoader().getResourceAsStream(PROP_FILENAME); 
		try {
			configProps.load(inputProperties);
			
			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "MediationMain", "initialize()",
						"***************************************************************************************************************************\n");
				logger.logp(Level.INFO, "MediationMain", "initialize()",
						"Blue Thread Mediation - loaded configuration "+configProps.toString()+"\n");
				logger.logp(Level.INFO, "MediationMain", "initialize()",
						"***************************************************************************************************************************\n");
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block

			logger.logp(Level.SEVERE, "MediationMain", "initialize()",
					"Failed to load properties file - quitting", e);

			//e.printStackTrace();
			System.exit(-1);
		}
		
		ctrlIfc = new ControlWsServer(configProps);
		ctrlIfc.startInterface();
		
		backEnd = new BackEndServant(configProps);
		mediation = new AisProtocolHandler(backEnd, configProps);
		frontEnd = new SerialController(mediation,configProps);
	}

	private void startBackEndProcessor() {
		// TODO Auto-generated method stub
		
	}

	private void startAISMessagesProcessor() {
		// TODO Auto-generated method stub
		
	}

	private void startSerialReceiver() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyEvent(Event evt) {
		// TODO Auto-generated method stub
		
	}
	
	@OnMessage
	public void getMessage(final Command cmd, final Session session){
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "MediationMain",
					"getMessage(Command, Session)", "Command received - cmd="
							+ cmd);
		}
	}
	
	@OnClose
    public void closeConnectionHandler(Session session, CloseReason closeReason){
		
	}
	
	public static void main(String[] args){
		
		if (args.length>0){
			String configFileName = args[0];
			
			if (configFileName != null ){
				File configFile = new File(configFileName);
				if(configFile.exists()){
					MediationMain main = new MediationMain(configFileName);
				}
				else {
					MediationMain main = new MediationMain();
				}
					
			}else{
				MediationMain main = new MediationMain();
			}	
		}
		else {
			MediationMain main = new MediationMain();
		}
		
		
		
	}

}
