package com.bluethread.mediation.websocket.client;

import java.util.logging.Logger;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;
import java.util.logging.Level;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import com.bluethread.mediation.types.founding.Servant;
import com.bluethread.mediation.types.ifc.JsonDataProducer;
import com.bluethread.mediation.types.ifc.RequestHandler;
import com.bluethread.mediation.websocket.client.dashboardifc.DashboardIfc;
import com.bluethread.mediation.websocket.client.dashboardifc.IfcFactory;



@ClientEndpoint
public class BackEndServant extends Servant<JsonDataProducer> implements RequestHandler <JsonDataProducer>{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(BackEndServant.class
			.getName());

	private Properties props;
	private URI websocketUriESRI;
	private Session userSession;
	private DashboardIfc dashIfc;
	
	
	public BackEndServant(Properties prp) {
		super();
		
		if(prp == null){
			
			logger.logp(Level.SEVERE, "BackEndServant", "BackEndServant()",
					"Configuration properties undefined. Quitting now");
			System.exit(-1);			
		} 
		else props = prp;
		
		String serverURI_ESRI = props.getProperty("serverURI_ESRI");
		if (serverURI_ESRI == null)
		{
			logger.logp(Level.SEVERE, "BackEndServant", "BackEndServant()",
					"ESRI Endpoint URI not defined. Quitting now");
			System.exit(-1);
		}			
		try {
			websocketUriESRI = new URI(serverURI_ESRI );
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			if (logger.isLoggable(Level.SEVERE)) {
				logger.logp(Level.SEVERE, "BackEndServant", "BackEndServant()",
						"ESRI Endpoint URI not defined. Quitting now");
			}
			System.exit(-1);
		}
		connectESRI();
		dashIfc = (new IfcFactory(prp)).getIfc();
		
	}
	
	



	private void connectESRI() {
		// TODO Auto-generated method stub
		try{
			WebSocketContainer container = ContainerProvider.getWebSocketContainer();
			userSession = container.connectToServer(this, websocketUriESRI);

			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "BackEndServant", "connect()",
						"Endpoint CONNECTED");
			}

		}catch (Exception e){
			
			logger.logp(Level.INFO, "BackEndServant", "connect()",
					"ESRI Endpoint not reachable. Quitting now");

			System.exit(-1);			
		}
	}


	@Override
	public void handleRequest(JsonDataProducer sd) {
		// TODO Auto-generated method stub
		
		
	
		String jsonFormat;
		
		jsonFormat = sd.toJsonFormat();
	
		
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "BackEndServant",
					"handleRequest(JsonDataProducer)",
					">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		}

		
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "BackEndServant",
					"handleRequest(JsonDataProducer)",
					"data received" + sd.toString());
		}
		
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "BackEndServant",
					"handleRequest(JsonDataProducer)",
					"sending JSON message  - jsonFormat=" + jsonFormat);
		}

		userSession.getAsyncRemote().sendText(jsonFormat);
		dashIfc.sendJsonFormat(jsonFormat);
		
	}
	
	
	public synchronized void feedRequest(JsonDataProducer data){
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "BackEndServant", "feedRequest(JsonDataProducer)",
					"New request received: - data=" + data.toString());
		}

		
		try {
			queue.put(data);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@OnOpen
	public void onOpen(final Session userSession){
		this.userSession = userSession;

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "BackEndServant", "onOpen(Session)",
					"ESRI Endpoint CONNECTED");
		}

	}
	
	@OnClose
	public void onClose(final Session userSession, CloseReason reason){
		this.userSession = null;
		
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "BackEndServant",
					"onClose(Session, CloseReason)", "ESRI Endpoint DISCONNECTED");
		}

		
	}
	
	@OnMessage
	public void onMessage(final String message){
		
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "BackEndServant", "onMessage(String)",
					"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		}

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "BackEndServant", "onMessage(String)",
					"Message received from Endpoint: - message=" + message);
		}
	}


	@Override
	public void cleanUpBeforeClosing() {
		// TODO Auto-generated method stub
		try {
			userSession.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(dashIfc != null)
			dashIfc.close();

		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "BackEndServant",
					"cleanUpBeforeClosing()",
					"Closing user session, releasing all resources");
		}
	}
	
}
