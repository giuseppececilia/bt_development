package com.bluethread.mediation.websocket.client.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


import com.bluethread.mediation.types.founding.Monitor;
import com.bluethread.mediation.websocket.client.BackEndServantUnderMonitor;

public class Test{



	public static void main (String [] args){
		
		String PROP_FILENAME= "com/bluethread/mediation/main/config.properties";
		
		Properties configProps = new Properties();
		InputStream inputProperties = Thread.currentThread().getContextClassLoader().getResourceAsStream(PROP_FILENAME); 
		try {
			configProps.load(inputProperties);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
		
		BackEndServantUnderMonitor esriClient = new BackEndServantUnderMonitor(configProps);
		Monitor monitor = new Monitor(esriClient);
		monitor.startAndMonitor();

	}
}
