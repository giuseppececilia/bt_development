package com.bluethread.mediation.websocket.client.dashboardifc;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;





public class ActualDashboardIfc implements DashboardIfc{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ActualDashboardIfc.class.getName());

	/**
	 * Logger for this class
	 */
	

	
	private URL btURL;
	private RingBuffer<String> buffer;
	private boolean connected;
	private Timer reconnectTimer;
	private Worker workingThread;
	
	
	public ActualDashboardIfc(URL dashboardURL) {
		// TODO Auto-generated constructor stub
		
		btURL = dashboardURL;
		buffer = new RingBuffer<String>(100);
		connected = false;		
		connectBtDashboard();
		
	}
	
	@Override
	public void sendJsonFormat(String jsonStr) {
		// TODO Auto-generated method stub
		
			try {
				buffer.add(jsonStr);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				
			logger.logp(Level.SEVERE, "ActualDashboardIfc", "sendJsonFormat(String)", "Internal buffer error"); 

			}
		
	}
	
	private void connectBtDashboard() {
		// TODO Auto-generated method stub


		HttpURLConnection connection;

		try {
			connection = (HttpURLConnection)btURL.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			connection.setRequestMethod("POST");
			OutputStreamWriter outStream;
			outStream = new OutputStreamWriter(connection.getOutputStream());
			connected = true;
			connection.disconnect();
			workingThread = new Worker("WorkerThread");
			workingThread.start();
			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "ActualDashboardIfc", "connectBtDashboard()", "CONNECTED to BT dashboard");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			dashBoardDisconnected();
		} 
	}
	
	private void dashBoardDisconnected(){
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "ActualDashboardIfc", "dashBoardDisconnected()", "BT dashboard DISCONNECTED, trying to reconnect in 5 seconds"); 
		}

		connected = false;
		reconnectTimer = new Timer();
		reconnectTimer.schedule(new TimeoutTask(), 5000);	
		
	}
	
	private class Worker extends Thread{
		private boolean close = false;
		public Worker(String name){
			super(name);
		}
		
		public void close(){
			close = true;
			try {
				buffer.add("\n");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public void run(){
			while(connected && !close){
				HttpURLConnection myConnection;
				try{
					myConnection = (HttpURLConnection) ActualDashboardIfc.this.btURL.openConnection();
					myConnection.setDoOutput(true);
					//myConnection.setRequestProperty("Content-Type", "text/html; charset=UTF-8");
					myConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
					myConnection.setRequestProperty("Accept", "application/json");
					//myConnection.setConnectTimeout(5000);
					//myConnection.setReadTimeout(5000);
					myConnection.setRequestMethod("POST");
					
					//OutputStreamWriter myOutStream;
					//myOutStream = new OutputStreamWriter(myConnection.getOutputStream());
					OutputStream myOutStream;
					myOutStream = myConnection.getOutputStream();
					String jsonMessage = buffer.remove();
					if(jsonMessage.length()>1) {
						myOutStream.write(jsonMessage.getBytes("UTF-8"));
						System.out.println(myConnection.getResponseCode());
						if (logger.isLoggable(Level.INFO)) {
							logger.logp(Level.INFO, "Worker", "run()", "Sending json message to dashboard - jsonMessage=" + jsonMessage);
						}
					}
					myOutStream.close();
					myConnection.disconnect();

					

				}catch (IOException e){
					connected = false;
					dashBoardDisconnected();
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					logger.logp(Level.SEVERE, "Worker", "run()", "Internal buffer error");
				}
			}
			
		}
	}
	
	
	private class TimeoutTask extends TimerTask{
		/**
		 * Logger for this class
		 */
		private final Logger logger = Logger.getLogger(TimeoutTask.class.getName());
		

		@Override
		public void run() {
			// TODO Auto-generated method stub

			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "TimeoutTask", "run()", "Trying to reconnect to dashboard");
			}

			connectBtDashboard();
		}
		
	}
	
	public void close(){
		if (connected) workingThread.close();
		
	}
	
	
	public static void main(String [] args){
		try {
			ActualDashboardIfc ifc = new ActualDashboardIfc(new URL("http://vps284023.ovh.net/BTVesselTracking/server/insertParameters.php"));
			
				System.out.println("Sending pippo");
				ifc.sendJsonFormat("pippo");
				
				System.out.println("Sending pluto");
				ifc.sendJsonFormat("pluto");
				
				System.out.println("Sending paperino");
				ifc.sendJsonFormat("paperino");	
				
				Thread.sleep(10000);
			ifc.close();
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
