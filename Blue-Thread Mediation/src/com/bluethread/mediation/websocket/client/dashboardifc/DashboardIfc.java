package com.bluethread.mediation.websocket.client.dashboardifc;

public interface DashboardIfc {

	
	public void sendJsonFormat(String jsonStr);
	public void close();
}
