package com.bluethread.mediation.websocket.client.dashboardifc;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;





public class ActualDashboardIfcApacheBased implements DashboardIfc{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ActualDashboardIfcApacheBased.class.getName());

	/**
	 * Logger for this class
	 */
	

	
	private URL btURL;
	private RingBuffer<String> buffer;
	private boolean connected;
	private Timer reconnectTimer;
	private Worker workingThread;
	
	
	public ActualDashboardIfcApacheBased(URL dashboardURL) {
		// TODO Auto-generated constructor stub
		
		btURL = dashboardURL;
		buffer = new RingBuffer<String>(100);
		connected = false;		
		connectBtDashboard();
		
	}
	
	@Override
	public void sendJsonFormat(String jsonStr) {
		// TODO Auto-generated method stub
		
			try {
				buffer.add(jsonStr);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				
			logger.logp(Level.SEVERE, "ActualDashboardIfc", "sendJsonFormat(String)", "Internal buffer error"); 

			}
		
	}
	
	private void connectBtDashboard() {
		// TODO Auto-generated method stub


		HttpURLConnection connection;

		try {
			connection = (HttpURLConnection)btURL.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			connection.setRequestMethod("POST");
			OutputStreamWriter outStream;
			outStream = new OutputStreamWriter(connection.getOutputStream());
			connected = true;
			connection.disconnect();
			workingThread = new Worker("WorkerThread");
			workingThread.start();
			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "ActualDashboardIfc", "connectBtDashboard()", "CONNECTED to BT dashboard");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			dashBoardDisconnected();
		} 
	}
	
	private void dashBoardDisconnected(){
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "ActualDashboardIfc", "dashBoardDisconnected()", "BT dashboard DISCONNECTED, trying to reconnect in 5 seconds"); 
		}

		connected = false;
		reconnectTimer = new Timer();
		reconnectTimer.schedule(new TimeoutTask(), 5000);	
		
	}
	
	private class Worker extends Thread{
		private boolean close = false;
		public Worker(String name){
			super(name);
		}
		
		public void close(){
			close = true;
			try {
				buffer.add("\n");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public void run(){
			while(connected && !close){

				CloseableHttpClient client = HttpClients.createDefault();

				HttpPost post = new HttpPost(ActualDashboardIfcApacheBased.this.btURL.toString());	
				List <NameValuePair> nvps = new ArrayList <NameValuePair>();



				//HttpURLConnection myConnection;
				try{

					String jsonMessage = buffer.remove();

					if(jsonMessage.length()>1) {
						nvps.add(new BasicNameValuePair("parametri", jsonMessage));
						post.setEntity(new UrlEncodedFormEntity(nvps));
						CloseableHttpResponse response = client.execute(post);
						if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK){
							if (logger.isLoggable(Level.INFO)) {
								logger.logp(Level.INFO, "Worker", "run()", "Sent json message to dashboard - jsonMessage=" + jsonMessage);
							}
						}
					}
				}
				catch(HttpHostConnectException e){
					connected = false;
					dashBoardDisconnected();
				}
				catch (InterruptedException e) {
					// TODO Auto-generated catch block
					logger.logp(Level.SEVERE, "Worker", "run()", "Internal buffer error");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}
	
	
	private class TimeoutTask extends TimerTask{
		/**
		 * Logger for this class
		 */
		private final Logger logger = Logger.getLogger(TimeoutTask.class.getName());
		

		@Override
		public void run() {
			// TODO Auto-generated method stub

			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "TimeoutTask", "run()", "Trying to reconnect to dashboard");
			}

			connectBtDashboard();
		}
		
	}
	
	public void close(){
		if (connected) workingThread.close();
		else reconnectTimer.cancel();
		
	}
	
	
	public static void main(String [] args){
		try {
			ActualDashboardIfcApacheBased ifc = new ActualDashboardIfcApacheBased(new URL("http://vps284023.ovh.net/BTVesselTracking/server/insertParameters.php"));
			
				System.out.println("Sending pippo");
				ifc.sendJsonFormat("pippo");
				
				System.out.println("Sending pluto");
				ifc.sendJsonFormat("pluto");
				
				System.out.println("Sending paperino");
				ifc.sendJsonFormat("paperino");	
				
				Thread.sleep(10000);
			ifc.close();
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
