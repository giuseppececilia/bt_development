package com.bluethread.mediation.websocket.client.dashboardifc;

import java.util.logging.Level;
import java.util.logging.Logger;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;


public class IfcFactory {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IfcFactory.class.getName());
	
	DashboardIfc dashboardIfc;
	
	public IfcFactory(Properties props){
		
		if (props==null){
			dashboardIfc = new TestDashboardIfc();
		}
		else{
			String dashboardURL = props.getProperty("bt_dashboard_URL");
			try {
				URL dashUrl = new URL(dashboardURL);
				dashboardIfc = new ActualDashboardIfcApacheBased(dashUrl);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block

				logger.logp(Level.SEVERE, "IfcFactory", "IfcFactory(Properties)", "Failed to create interface to dashboard, creating test interface", e);
				dashboardIfc = new TestDashboardIfc();
			}
			
		}
		
		
	}
	
	public DashboardIfc getIfc(){
		return dashboardIfc;
	}

}
