package com.bluethread.mediation.websocket.client;

import java.util.logging.Logger;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;
import java.util.logging.Level;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.RemoteEndpoint.Async;
import javax.websocket.RemoteEndpoint.Basic;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import com.bluethread.mediation.types.founding.MonitorIfc;
import com.bluethread.mediation.types.founding.MonitoringObjectIfc;
import com.bluethread.mediation.types.founding.Servant;
import com.bluethread.mediation.types.founding.ServiceNotStartedException;
import com.bluethread.mediation.types.ifc.JsonDataProducer;
import com.bluethread.mediation.types.ifc.RequestHandler;
import com.bluethread.mediation.websocket.client.dashboardifc.DashboardIfc;
import com.bluethread.mediation.websocket.client.dashboardifc.IfcFactory;



/**
 * @author Giuseppe Cecilia
 *
 */

@ClientEndpoint
public class BackEndServantUnderMonitor extends Servant<JsonDataProducer> implements RequestHandler <JsonDataProducer>, MonitoringObjectIfc{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(BackEndServantUnderMonitor.class
			.getName());

	private Properties props;
	private URI websocketUriESRI;
	private Session userSession;
	private DashboardIfc dashIfc;
	private MonitorIfc myMonitor;
	
	
	public BackEndServantUnderMonitor(Properties prp) {
		super();
		
		if(prp == null){
			
			logger.logp(Level.SEVERE, "BackEndServant", "BackEndServant()",
					"Configuration properties undefined. Quitting now");
			System.exit(-1);			
		} 
		else props = prp;
		
		String serverURI_ESRI = props.getProperty("serverURI_ESRI");
		if (serverURI_ESRI == null)
		{
			logger.logp(Level.SEVERE, "BackEndServant", "BackEndServant()",
					"ESRI Endpoint URI not defined. Quitting now");
			System.exit(-1);
		}			
		try {
			websocketUriESRI = new URI(serverURI_ESRI );
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			if (logger.isLoggable(Level.SEVERE)) {
				logger.logp(Level.SEVERE, "BackEndServant", "BackEndServant()",
						"ESRI Endpoint URI not defined. Quitting now");
			}
			System.exit(-1);
		}
		
		
	}
	
	



	private void connectESRI() throws DeploymentException, IOException {
		// TODO Auto-generated method stub
		
			WebSocketContainer container = ContainerProvider.getWebSocketContainer();
			userSession = container.connectToServer(this, websocketUriESRI);

			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "BackEndServant", "connect()",
						"Endpoint CONNECTED");
			}			
		
	}


	@Override
	public void handleRequest(JsonDataProducer sd) {
		// TODO Auto-generated method stub
		
		
	
		String jsonFormat;
		
		jsonFormat = sd.toJsonFormat();
	
		
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "BackEndServant",
					"handleRequest(JsonDataProducer)",
					">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		}

		
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "BackEndServant",
					"handleRequest(JsonDataProducer)",
					"data received" + sd.toString());
		}
		
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "BackEndServant",
					"handleRequest(JsonDataProducer)",
					"sending JSON message  - jsonFormat=" + jsonFormat);
		}

		
		
		//userSession.getAsyncRemote().sendText(jsonFormat);
		try {
			userSession.getBasicRemote().sendText(jsonFormat);
		} catch (IOException e) {
			// TODO Auto-generated catch block

			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "BackEndServantUnderMonitor", "handleRequest(JsonDataProducer)", "Failed to send data to ESRI service"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			myMonitor.notifyFailure();
		}
		dashIfc.sendJsonFormat(jsonFormat);
		
	}
	
	
	public synchronized void feedRequest(JsonDataProducer data){
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "BackEndServant", "feedRequest(JsonDataProducer)",
					"New request received: - data=" + data.toString());
		}

		
		try {
			queue.put(data);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@OnOpen
	public void onOpen(final Session userSession){
		this.userSession = userSession;

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "BackEndServant", "onOpen(Session)",
					"Endpoint CONNECTED");
		}

	}
	
	@OnClose
	public void onClose(final Session userSession, CloseReason reason){
		this.userSession = null;
		
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "BackEndServant",
					"onClose(Session, CloseReason)", "Endpoint DISCONNECTED");
		}

		
	}
	
	@OnMessage
	public void onMessage(final String message){
		
		/*
		 * no messages are sent beck from ESRI service
		 */
	
	}


	@Override
	public void cleanUpBeforeClosing() {
		// TODO Auto-generated method stub
		try {
			userSession.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		dashIfc.close();

		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "BackEndServant",
					"cleanUpBeforeClosing()",
					"Closing user session, releasing all resources");
		}
	}





	@Override
	public void startService(MonitorIfc myMonitor) throws ServiceNotStartedException {
		// TODO Auto-generated method stub
		try {
			connectESRI();
		} catch (DeploymentException | IOException e) {
			// TODO Auto-generated catch block

			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "BackEndServantUnderMonitor", "startService(MonitorIfc)", "Failed to connect ESRI service"); 
			}
			throw new ServiceNotStartedException();
		}
		/*
		 * In case dashboard fails, a test interface is returned, so no handling logic for startup failures for now
		 */
		dashIfc = (new IfcFactory(props)).getIfc();
	}





	@Override
	public void close() {
		// TODO Auto-generated method stub
		
		if(dashIfc != null)
			dashIfc.close();
		
		if (userSession!=null)
		try {
			
			userSession.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
