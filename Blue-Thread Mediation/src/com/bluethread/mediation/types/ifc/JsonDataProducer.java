package com.bluethread.mediation.types.ifc;

public interface JsonDataProducer {
	
	public String toJsonFormat();

}
