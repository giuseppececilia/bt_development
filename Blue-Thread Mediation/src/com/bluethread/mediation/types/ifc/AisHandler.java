package com.bluethread.mediation.types.ifc;

import com.bluethread.aismessages.ais.messages.AISMessage;

import com.bluethread.aismessages.ais.messages.BlueThreadBinaryBroadcastMessage;
import com.bluethread.aismessages.ais.messages.StandardClassBCSPositionReport;

public interface AisHandler {
	
	public void storeTimeAndPosition(StandardClassBCSPositionReport msg);
	public boolean isMMSIValid(AISMessage msg);
	public boolean isFirst(BlueThreadBinaryBroadcastMessage msg);
	public void cacheParameters(BlueThreadBinaryBroadcastMessage msg);
	public void emptyCache();
	public void dataCollectionCompleted();
	public boolean isSecond(BlueThreadBinaryBroadcastMessage msg);
	public boolean isThird(BlueThreadBinaryBroadcastMessage msg);
	public void handleAISMessage(AISMessage msg);
	public void startPositionValidationTimer();
	public void stopPositionValidationTimer();

}
