package com.bluethread.mediation.types.ifc;

public interface RequestHandler <T> {
	
	public void feedRequest (T t);

}
