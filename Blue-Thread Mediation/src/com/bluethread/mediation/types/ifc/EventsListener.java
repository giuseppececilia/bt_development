package com.bluethread.mediation.types.ifc;

import com.bluethread.mediation.types.Event;


public interface EventsListener {
	
	public void notifyEvent(Event evt);

}
