package com.bluethread.mediation.types;

import com.bluethread.aismessages.ais.messages.AISMessage;

public class Message extends Event {

	
	private AISMessage myMsg;
	
	public Message (AISMessage msg){
		super(EventType.AisMessage);
		
		myMsg = msg;
	}
	
	public AISMessage getMsg(){
		return myMsg;
	}
}
