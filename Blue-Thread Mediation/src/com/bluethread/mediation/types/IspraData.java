package com.bluethread.mediation.types;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import com.bluethread.mediation.types.ifc.JsonDataProducer;
import com.bluethread.mediation.types.Constants;

/*
 * author: Giuseppe Cecilia
 * Class used as data class for accumulating info while FSM proceeds. 
 * Passed along the processing chain
 */

public class IspraData implements JsonDataProducer{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IspraData.class
			.getName());
	
	private HashMap <Integer, Float>innerDataContainer;
	private String ID;
	private Date samplingDate;
	private Position myPosition;
	String jsonString;
	
	public IspraData(String ID, 
			Date date,
			Position pos)
	{
		
		this.ID = ID;
		
		// set default values in case of nulls
		if (pos!=null) myPosition = pos;
		else myPosition = new Position(new Float(0),new Float(0));
		
		if(date!=null) this.samplingDate = date;
		else this.samplingDate = new Date(System.currentTimeMillis());
		
		this.innerDataContainer = new HashMap<Integer, Float>();
		
	}
	
	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public Date getSamplingDate() {
		return samplingDate;
	}

	public void setSamplingDate(Date samplingDate) {
		this.samplingDate = samplingDate;
	}

	public float getLatitude() {
		return myPosition.getLat().floatValue();
	}

	public void setLatitude(double latitude) {
		myPosition.setLat(new Float(latitude));
	}

	public float getLongitude() {
		return myPosition.getLng().floatValue();
	}

	public void setLongitude(double longitude) {
		myPosition.setLng(new Float(longitude));
	}
	
	public boolean addParameter (Integer code, Float value){
		
		/*
		 *  received data MUST be normalized;
		 *  TEMP => divided by 1000
		 *  the others divided by 100
		 */
		if(!innerDataContainer.containsKey(code)){
			switch (code.intValue())
			{
			case Constants.TEMPERATURA_CODE:
				innerDataContainer.put(code, value/1000);
				break;
			case Constants.CLOROFILLA_CODE:
			case Constants.FDOM_CODE:
			case Constants.SALINITA_CODE:
			case Constants.TORBIDITA_CODE:
				innerDataContainer.put(code, value/100);
				break;
			default:
				innerDataContainer.put(code, value);
			}
			return true;
		}
		else return false;
	}
	
	/*
	 *  method used to clean up stored data in case of data out of date
	 */
	public void cleanData(){
		innerDataContainer = null;
		innerDataContainer = new HashMap<Integer, Float>(); 
	}
	
	
	/*
	 * clone method needed to duplicate the object when passing along the processing chain
	 * in order to avoid side effects in a multi-threaded environment while accessing internal data
	 */
	@SuppressWarnings("unchecked")
	public IspraData clone(){
		IspraData toReturn = new IspraData(this.ID,this.samplingDate,this.myPosition);
		toReturn.innerDataContainer = (HashMap<Integer, Float>) this.innerDataContainer.clone();
		return toReturn;						
	}
	
	public String toString(){
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("{ID=");
		strBuff.append(this.ID);
		strBuff.append(",date=");
		strBuff.append(this.samplingDate);
		strBuff.append(",lat=");
		strBuff.append(this.getLatitude());
	strBuff.append(",lng=");
		strBuff.append(this.getLongitude());
		strBuff.append(",parameters =");		
		
        Iterator<Map.Entry<Integer, Float>> iterator = innerDataContainer.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Integer, Float> mapEntry = iterator.next();
            strBuff.append("[");
            strBuff.append(mapEntry.getKey().toString());
            strBuff.append(",");
            strBuff.append(mapEntry.getValue().toString()); 
            strBuff.append("]");
        
        }
        strBuff.append("}");
		return strBuff.toString();
	}

	@Override
	public String toJsonFormat() {
		// TODO Auto-generated method stub
		DecimalFormat df = new DecimalFormat("#0.000");
		DecimalFormat coordFormat = new DecimalFormat("#0.00000");
		JsonObjectBuilder job = Json.createObjectBuilder();
		job.add("ID", getID());
		/*
		 * time must be normalized since getTime() return number of MILLISECONDS, instead we need number of SECONDS, 
		 * so we must send only the first 10 digits of the long returned by getTime() using substring
		 */
		job.add("time",Long.toString(getSamplingDate().getTime()).substring(0, 10));
		job.add("lat",coordFormat.format(getLatitude()));
	    job.add("long", coordFormat.format(getLongitude()));
		//job.add("lat",Double.toString(getLatitude()));
		//job.add("long", Double.toString(getLongitude()));
	
        Iterator<Map.Entry<Integer, Float>> iterator = innerDataContainer.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Integer, Float> mapEntry = iterator.next();
            job.add(ParamCodeConverter.getParameterName(mapEntry.getKey()), df.format(mapEntry.getValue()));
            //job.add(ParamCodeConverter.getParameterName(mapEntry.getKey()), Double.toString(mapEntry.getValue()));
        }
		String jsonString = job.build().toString();

		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "IspraData", "toJsonFormat()",
					"JSON format of sample data: "+ jsonString);
		}

		return jsonString;
	}
	

}
