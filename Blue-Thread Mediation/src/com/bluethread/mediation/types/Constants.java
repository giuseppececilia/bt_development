package com.bluethread.mediation.types;



public class Constants {
	
	public static final int TEMPERATURA_CODE = 1;
	public static final int SALINITA_CODE = 12;
	public static final int CLOROFILLA_CODE = 193;
	public static final int TORBIDITA_CODE = 223;
	public static final int FDOM_CODE = 228;
	public static final int MAX_NUMBER_OF_RESTARTS = 10;

}
