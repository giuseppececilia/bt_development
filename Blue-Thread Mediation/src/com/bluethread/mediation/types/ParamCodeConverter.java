package com.bluethread.mediation.types;


import java.util.HashMap;

public class ParamCodeConverter {
	private static HashMap<Integer,String> conversionMap;
	
	public static String getParameterName(Integer code){
		if(conversionMap==null){
			conversionMap = new HashMap<Integer,String>();
			initialize();
		}
		return conversionMap.get(code);
	}

	private static void initialize() {
		// TODO Auto-generated method stub
		conversionMap.put(new Integer(1), "temperatura");
		conversionMap.put(new Integer(12), "salinita");
		conversionMap.put(new Integer(193), "clorofilla");
		conversionMap.put(new Integer(223), "torbidita");
		conversionMap.put(new Integer(228), "fdom");
	}
	
	public static void main(String [] args){
		System.out.println(getParameterName(new Integer(1)));
		System.out.println(getParameterName(new Integer(12)));
		System.out.println(getParameterName(new Integer(193)));
		System.out.println(getParameterName(new Integer(223)));
		System.out.println(getParameterName(new Integer(228)));
	}

}
