package com.bluethread.mediation.types.founding;

public interface MonitorIfc{
	public void notifyFailure();
}