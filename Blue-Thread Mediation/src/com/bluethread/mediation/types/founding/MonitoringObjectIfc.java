package com.bluethread.mediation.types.founding;

import com.bluethread.mediation.types.founding.ServiceNotStartedException;

public interface MonitoringObjectIfc {
	
	
	public void startService(MonitorIfc myMonitor) throws ServiceNotStartedException;
	
	public void close();
	

}
