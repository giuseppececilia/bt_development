package com.bluethread.mediation.types.founding.Test;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.bluethread.mediation.types.founding.Monitor;
import com.bluethread.mediation.types.founding.MonitorIfc;
import com.bluethread.mediation.types.founding.MonitoringObjectIfc;
import com.bluethread.mediation.types.founding.ServiceNotStartedException;

public class TestMonitoring implements MonitoringObjectIfc{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TestMonitoring.class.getName());
	
	
	private static int counter = 0;
	
	private MonitorIfc monitor;

	@Override
	public void startService(MonitorIfc myMonitor) throws ServiceNotStartedException {
		// TODO Auto-generated method stub
		
		monitor = myMonitor;
		System.out.println("start counter "+counter);
		if ((counter % 2) == 0){
			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "TestMonitoring", "startService(MonitorIfc)", "Simulating starting failure\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}

			counter++;
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw new ServiceNotStartedException();
		}
		else{
			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "TestMonitoring", "startService(MonitorIfc)", "Successfully started object\n"); 
				counter++;
			}
			
			new SimThread().start();
			return;
			
		}
		
		
		
		
	}
	private class SimThread extends Thread{
		public void run(){
			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "SimThread", "run()", "Started failure at runtime simulator thread\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}

			try {
				Thread.sleep(20000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "SimThread", "run()", "Sending failure notification to monitor\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}

			monitor.notifyFailure();
			
		}
	}
	
	public static void main (String [] args){
		Monitor monitor = new Monitor(new TestMonitoring());
		monitor.startAndMonitor();
		try {
			Thread.sleep(120000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

}
