package com.bluethread.mediation.types.founding;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.bluethread.mediation.types.Constants;

/**
 * @author Giuseppe Cecilia per Blue Thread
 *
 */
public class Monitor implements MonitorIfc{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(Monitor.class.getName());
	
	
	private MonitoringObjectIfc objectUnderMonitor;
	
	private boolean working;
	
	private static int restartsCounter = 0;
	
	public Monitor (MonitoringObjectIfc objectUnderMonitoring){
		
		objectUnderMonitor = objectUnderMonitoring;
		working = false;
		
	}
	
	public void startAndMonitor(){
		
		MonitoringThread monitor = new MonitoringThread();
		monitor.start();
		
	}
	
	private class MonitoringThread extends Thread{
		
		public void run(){
			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "MonitoringThread", "run()", "Start monitoring object " + objectUnderMonitor.getClass().getName()+"\n"); 
			}

			while (!working && (restartsCounter<= Constants.MAX_NUMBER_OF_RESTARTS)){
				try {
					restartsCounter++;
					objectUnderMonitor.startService(Monitor.this);
					working = true;
				} catch (ServiceNotStartedException e) {
					// TODO Auto-generated catch block
					if (logger.isLoggable(Level.INFO)) {
						logger.logp(Level.INFO, "MonitoringThread", "run()", "Failed to start monitored object, retrying in 5 seconds attempt n. "+restartsCounter+"\n");
						objectUnderMonitor.close();
					}
					working = false;
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
					}
					
				}
				
			}

			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "MonitoringThread", "run()", "Successfully started object "+ objectUnderMonitor.getClass().getName()+"\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
		}
	}



	@Override
	public void notifyFailure() {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "Monitor", "notifyFailure()", "Monitored object failure, restarting "+objectUnderMonitor.getClass().getName()+"\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}

		working = false;
		startAndMonitor();
	}

}
