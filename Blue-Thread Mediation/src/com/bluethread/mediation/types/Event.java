package com.bluethread.mediation.types;

public class Event {
	
	private EventType myType;
	
	public Event(EventType et){
		myType = et;
	}
	
	public EventType getType(){
		return myType;
	}

}
