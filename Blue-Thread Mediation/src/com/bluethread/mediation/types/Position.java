package com.bluethread.mediation.types;

public class Position {
	
	private Float lg;
	private Float lt;
	
	public Position(Float lat, Float lng){
		lt=lat;
		lg=lng;
	}

	public Float getLng() {
		return lg;
	}

	public void setLng(Float lg) {
		this.lg = lg;
	}

	public Float getLat() {
		return lt;
	}

	public void setLat(Float lt) {
		this.lt = lt;
	}

	@Override
	public String toString() {
		return "Position [lng=" + lg + ", lat=" + lt + "]";
	}
	
	

}
