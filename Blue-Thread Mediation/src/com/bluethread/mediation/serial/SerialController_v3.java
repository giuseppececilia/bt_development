package com.bluethread.mediation.serial;

import java.util.logging.Level;
import java.util.logging.Logger;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.function.Consumer;





import com.bluethread.mediation.ais.AisProtocolHandler;
import com.bluethread.mediation.types.Message;

import com.bluethread.aismessages.AISInputStreamReader;
import com.bluethread.aismessages.ais.messages.AISMessage;

public class SerialController_v3 {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(SerialController_v3.class.getName());
	
	private Properties myProps;
	private SerialPort myPort;
	private int rate;
	private InputStream myInputStream;
	private AisProtocolHandler next;
	private final static int TIME_OUT = 2000;
	private final static int DEFAULT_BAUD = 9600;
	private static final String DEFAULT_PORT = "COM1";
	

	
	/**
	 * Opens the default property file
	 * @throws NoSuchPortException
	 * @throws PortInUseException
	 * @throws IOException 
	 */
	public SerialController_v3(AisProtocolHandler nextInTheChain) throws NoSuchPortException, PortInUseException, IOException{
		next = nextInTheChain;
		myProps = new Properties();
		String propFileName = "com/bluethread/mediation/serial/config.properties";
		InputStream inputProperties = Thread.currentThread().getContextClassLoader().getResourceAsStream(propFileName); 
		try {
			myProps.load(inputProperties);
		} catch (IOException e) {
			logger.logp(Level.SEVERE, "SerialController_v3",
					"SerialController_v3(AisProtocolHandler)",
					"Failed to load configuration properties", e);
			System.exit(-1);
		}
		init();
		
		
		
	}
	
	
	
	/**
	 * reads port and rate form the properties received 
	 * @param props
	 * @throws NoSuchPortException
	 * @throws PortInUseException
	 * @throws IOException 
	 */
	public SerialController_v3(Properties props) throws NoSuchPortException, PortInUseException, IOException{
		if(props != null){
			myProps = props;
			
		} else{
			logger.logp(Level.SEVERE, "SerialController_v3",
					"SerialController_v3(Properties)",
					"Configuration properties are null");

			System.exit(-1);
		}
		init();
	}
	
	public SerialController_v3() throws NoSuchPortException, PortInUseException, IOException{
		this((AisProtocolHandler)null);
	}
	
	/**
	 * initialize serial port. Uses COM1 and 9600 baud as default
	 * @throws IOException 
	 */
	private void init() throws NoSuchPortException, PortInUseException, IOException{

		String portName = myProps.getProperty("portName");
		if(portName== null || portName.equals("")) portName=DEFAULT_PORT;
		String rateStr = myProps.getProperty("baud");

		if(rateStr != null){
			rate = Integer.valueOf(rateStr).intValue();
		}
		else 
			rate = DEFAULT_BAUD;

		CommPortIdentifier portId;

		portId = CommPortIdentifier.getPortIdentifier(portName);
		myPort = (SerialPort)portId.open(this.getClass().getName(), TIME_OUT);
		try {
            myPort.setSerialPortParams(rate,
                SerialPort.DATABITS_8,
                SerialPort.STOPBITS_1,
                SerialPort.PARITY_NONE);
            
            // next 2 lines are vital for blocking IO on serial port
            // otherwise IOException is raised
            
            myPort.disableReceiveTimeout();
            myPort.enableReceiveThreshold(1);
        } catch (UnsupportedCommOperationException e) {System.out.println(e);}
		myInputStream=myPort.getInputStream();
		ReaderThread rt= new ReaderThread(myPort);
		//new Deblocking(rt).start();
		rt.start();

	}
	
	
	/**
	 * Used for testing purposes
	 * @author teigcec
	 *
	 */
	class Deblocking extends Thread{
		/**
		 * Logger for this class
		 */
		private final Logger logger = Logger.getLogger(Deblocking.class
				.getName());
		
		private ReaderThread observedThread;
		public Deblocking(ReaderThread toDeblock){
			observedThread = toDeblock;
		}
		public void run(){
			System.out.println("Deblocking Thread: Started deblocking Thread");
			try {
				Thread.sleep(20000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Deblocking Thread: Requesting to deblock observer Thread");
			observedThread.quit();
		}
	}
	
	class ReaderThread extends Thread implements  Consumer<AISMessage>{
		/**
		 * Logger for this class
		 */
		private final Logger logger = Logger.getLogger(ReaderThread.class
				.getName());

		private SerialPort inPort;
		private boolean stop = false;
		private boolean stopRequested = false;
		private AISInputStreamReader aisISR;

		
		public ReaderThread(SerialPort port) throws IOException{
			inPort = port;
			
			InputStream aisIS;
			aisIS = inPort.getInputStream();
			aisISR = new AISInputStreamReader(aisIS,this);
		}
		
		public void quit(){
			aisISR.requestStop();
			inPort.close();
		}
		
		public void run(){			
			System.out.println("ReaderThread: started and waiting for data on port COM1");

			try {
				aisISR.run();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("ReaderThread: STOPPING ");
			inPort.close();
		}


		@Override
		public void accept(AISMessage t) {
			
			//send to next stage AisProtocolHandler;
			System.out.println(t.toString());
			if (next != null)
				next.feedRequest(new Message(t));
			
		}
	}
	
	public static void main(String [] args){
		try {
			new SerialController_v3();
		} catch (NoSuchPortException | PortInUseException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
