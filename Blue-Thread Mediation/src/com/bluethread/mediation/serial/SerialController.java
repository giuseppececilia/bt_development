package com.bluethread.mediation.serial;

import java.util.logging.Level;
import java.util.logging.Logger;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.function.Consumer;

import com.bluethread.mediation.ais.AisProtocolHandler;
import com.bluethread.mediation.test.TestAisProtocolHandler;
import com.bluethread.mediation.types.ifc.AisHandler;

import com.bluethread.aismessages.AISInputStreamReader;
import com.bluethread.aismessages.ais.messages.AISMessage;

public class SerialController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(SerialController.class.getName());
	
	private Properties myProps;
	private SerialPort myPort;
	private int rate;
	private AisHandler next;
	private final static int TIME_OUT = 2000;
	private final static int DEFAULT_BAUD = 9600;
	private static final String DEFAULT_PORT = "COM1";
	

	
	/**
	 * Opens the default property file
	 * @throws NoSuchPortException
	 * @throws PortInUseException
	 * @throws IOException 
	 */
	public SerialController(AisHandler nextInTheChain) throws NoSuchPortException, PortInUseException, IOException{
		next = nextInTheChain;
		myProps = new Properties();
		String propFileName = "com/bluethread/mediation/serial/config.properties";
		InputStream inputProperties = Thread.currentThread().getContextClassLoader().getResourceAsStream(propFileName); 
		try {
			myProps.load(inputProperties);
		} catch (IOException e) {
			logger.logp(Level.SEVERE, "SerialController",
					"SerialController(AisProtocolHandler)",
					"Failed to load configuration properties");
			System.exit(-1);
		}
		init();
		
		
		
	}
	
	
	
	/**
	 * reads port and rate form the properties received 
	 * @param props
	 * @throws NoSuchPortException
	 * @throws PortInUseException
	 * @throws IOException 
	 */
	public SerialController(AisProtocolHandler nextInTheChain, Properties props){
		if(props != null){
			myProps = props;
			
		} else{
			logger.logp(Level.SEVERE, "SerialController",
					"SerialController(AisProtocolHandler, Properties)",
					"Configuration properties are null");

			System.exit(-1);
		}
		next = nextInTheChain;
		init();
	}
	
	
	
	/**
	 * initialize serial port. Uses COM1 and 9600 baud as default
	 * @throws IOException 
	 */
	private void init(){
		

		String portName = myProps.getProperty("portName");
		if(portName== null || portName.equals("")) portName=DEFAULT_PORT;
		String rateStr = myProps.getProperty("baud");

		if(rateStr != null){
			rate = Integer.valueOf(rateStr).intValue();
		}
		else 
			rate = DEFAULT_BAUD;

		CommPortIdentifier portId;
		try {
			portId = CommPortIdentifier.getPortIdentifier(portName);
			myPort = (SerialPort)portId.open(this.getClass().getName(), TIME_OUT);
		} catch (PortInUseException e1) {
			// TODO Auto-generated catch block

			logger.logp(Level.INFO, "SerialController", "init()",
					"Serial port already in use - portName =" + portName);

			e1.printStackTrace();
			System.exit(-1);
		}
		catch (NoSuchPortException e1) {
			// TODO Auto-generated catch block

			logger.logp(Level.INFO, "SerialController", "init()",
					"Serial port not found - portName ="+portName);
			System.exit(-1);
		}
		
		
		try {
            myPort.setSerialPortParams(rate,
                SerialPort.DATABITS_8,
                SerialPort.STOPBITS_1,
                SerialPort.PARITY_NONE);
            
            // next 2 lines are vital for blocking IO on serial port
            // otherwise IOException is raised
            
            myPort.disableReceiveTimeout();
            myPort.enableReceiveThreshold(1);
        } catch (UnsupportedCommOperationException e) {
			logger.logp(Level.INFO, "SerialController", "init()",
					"Failed to open serial port  - portName =" + portName
							+ ", rate =" + rateStr);
			
			System.exit(-1);
			}

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "SerialController", "init()",
					"Opened serial port - portName =" + portName + ", rate ="
							+ rateStr);
		}
		
		ReaderThread rt;
		try {
			rt = new ReaderThread(myPort);
			rt.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block

			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "SerialController", "init()",
						"Failed to read data from port - portName =" + portName);
			}
		}
		
		
		
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "SerialController", "init()",
					"Started the serial port reader - portName =" + portName
							+ ", rate =" + rateStr);
		}

	}
	
	
	class ReaderThread extends Thread implements  Consumer<AISMessage>{
		/**
		 * Logger for this class
		 */
		private final Logger logger = Logger.getLogger(ReaderThread.class
				.getName());
		

		private SerialPort inPort;
		//private boolean stop = false;
		//private boolean stopRequested = false;
		private AISInputStreamReader aisISR;

		
		public ReaderThread(SerialPort port) throws IOException{
			inPort = port;
			
			InputStream aisIS;
			aisIS = inPort.getInputStream();
			aisISR = new AISInputStreamReader(aisIS,this);

			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "ReaderThread",
						"ReaderThread(SerialPort)",
						"Created serial port reader thread - port=" + port.getName());
			}
		}
		
		public void quit(){
			
			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "ReaderThread", "quit()",
						"Quit requested");
			}
			aisISR.requestStop();
			//inPort.close();

		}
		
		public void run(){
			
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "ReaderThread", "run()",
						"Reader thread started on port"+inPort.getName());
			}

			try {
				aisISR.run();

			} catch (IOException e) {
				logger.logp(Level.SEVERE, "ReaderThread", "run()",
						"Error reading from serial port");
			}

			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "ReaderThread", "run()",
						"Stopping reader thread and closing serial port");
			}

			inPort.close();

			
		}


		@Override
		public void accept(AISMessage t) {
			
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "ReaderThread", "accept(AISMessage)",
						"Received AIS message from serial port. Sending to AIS Protocol Handler - t="
								+ t+"\n");
			}
			next.handleAISMessage(t);
					
			
		}
	}
	
	public static void main(String [] args){
		

		try {
			new SerialController(new TestAisProtocolHandler());
		} catch (NoSuchPortException | PortInUseException | IOException e) {
			logger.logp(Level.SEVERE, "SerialController", "main(String[])",
					"Failed to start the Serial Controller");
		}

	}
}
