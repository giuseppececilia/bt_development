package com.bluethread.mediation.serial;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.TooManyListenersException;
import java.util.function.Consumer;





import com.bluethread.mediation.ais.AisProtocolHandler;


import com.bluethread.mediation.types.Message;

import com.bluethread.aismessages.ais.messages.AISMessage;
import com.bluethread.aismessages.nmea.NMEAMessageHandler;
import com.bluethread.aismessages.nmea.NMEAMessageInputStreamReader;

public class SerialController_v2 {
	
	private Properties myProps;
	private SerialPort myPort;
	private int rate;
	private InputStream myInputStream;
	private AisProtocolHandler next;
	
	
	/**
	 * Opens the default property file
	 * @throws NoSuchPortException
	 * @throws PortInUseException
	 * @throws IOException 
	 */
	public SerialController_v2(AisProtocolHandler nextInTheChain) throws NoSuchPortException, PortInUseException, IOException{
		next = nextInTheChain;
		myProps = new Properties();
		String propFileName = "com/bluethread/mediation/serial/config.properties";
		InputStream inputProperties = Thread.currentThread().getContextClassLoader().getResourceAsStream(propFileName); 
		try {
			myProps.load(inputProperties);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
		init();
		
	}
	
	
	
	/**
	 * reads port and rate form the properties received 
	 * @param props
	 * @throws NoSuchPortException
	 * @throws PortInUseException
	 * @throws IOException 
	 */
	public SerialController_v2(Properties props) throws NoSuchPortException, PortInUseException, IOException{
		if(props != null){
			myProps = props;
			
		} else{
			System.exit(-1);
		}
		init();
	}
	
	public SerialController_v2() throws NoSuchPortException, PortInUseException, IOException{
		this((AisProtocolHandler)null);
	}
	
	/**
	 * initialize serial port. Uses COM1 and 9600 baud as default
	 * @throws IOException 
	 */
	private void init() throws NoSuchPortException, PortInUseException, IOException{

		String portName = myProps.getProperty("portName");
		if(portName== null || portName.equals("")) portName="COM1";
		String rateStr = myProps.getProperty("baud");

		if(rateStr != null){
			rate = Integer.valueOf(rateStr).intValue();
		}
		else 
			rate = 9600;

		CommPortIdentifier portId;

		portId = CommPortIdentifier.getPortIdentifier(portName);
		myPort = (SerialPort)portId.open("AisMessagesPort", rate);
		myInputStream=myPort.getInputStream();
		ReaderThread rt= new ReaderThread(myPort);
		new Deblocking(rt).start();
		rt.start();

	}
	
	
	/**
	 * Used for testing purposes
	 * @author teigcec
	 *
	 */
	class Deblocking extends Thread{
		
		private ReaderThread observedThread;
		public Deblocking(ReaderThread toDeblock){
			observedThread = toDeblock;
		}
		public void run(){
			System.out.println("Deblocking Thread: Started deblocking Thread");
			try {
				Thread.sleep(20000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Deblocking Thread: Requesting to deblock observer Thread");
			observedThread.quit();
		}
	}
	
	class ReaderThread extends Thread implements SerialPortEventListener, Consumer<AISMessage>{
		private SerialPort inPort;
		private boolean stop = false;
		private boolean stopRequested = false;
	    private NMEAMessageHandler nmeaMessageHandler;
		private NMEAMessageInputStreamReader nmeaMessageInputStreamReader;
		
		public ReaderThread(SerialPort port) throws IOException{
			inPort = port;
			try {
				inPort.addEventListener(this);
			} catch (TooManyListenersException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			InputStream aisIS;

			aisIS = inPort.getInputStream();
			nmeaMessageHandler = new NMEAMessageHandler("TestAIS", this);
			nmeaMessageInputStreamReader = new NMEAMessageInputStreamReader(aisIS,nmeaMessageHandler);
		}
		
		public void quit(){
			nmeaMessageInputStreamReader.requestStop();
			inPort.close();
		}
		
		public void run(){			
			System.out.println("ReaderThread: started and waiting for data on port COM1");

			try {
				nmeaMessageInputStreamReader.run();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("ReaderThread: STOPPING ");
			inPort.close();
		}

		@Override
		public void serialEvent(SerialPortEvent event) {
			// TODO Auto-generated method stub
			
			System.out.println(event.toString());

			switch(event.getEventType()) {
				case SerialPortEvent.BI:
				case SerialPortEvent.OE:
				case SerialPortEvent.FE:
				case SerialPortEvent.PE:
				case SerialPortEvent.CD:
				case SerialPortEvent.CTS:
				case SerialPortEvent.DSR:
				case SerialPortEvent.RI:
				case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
					break;
				case SerialPortEvent.DATA_AVAILABLE:
					byte[] readBuffer = new byte[20];

					try {
						while (myInputStream.available() > 0) {
							int numBytes = myInputStream.read(readBuffer);
						}
						System.out.print(new String(readBuffer));
					} catch (IOException e) {System.out.println(e);}
					break;
			}
		
			
			
			
		}

		@Override
		public void accept(AISMessage t) {
			
			//send to next stage AisProtocolHandler;
			System.out.println(t.toString());
			if (next != null){
				
				next.feedRequest(new Message(t));
			}
				
			
		}
	}
	
	public static void main(String [] args){
		try {
			new SerialController_v2();
		} catch (NoSuchPortException | PortInUseException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
