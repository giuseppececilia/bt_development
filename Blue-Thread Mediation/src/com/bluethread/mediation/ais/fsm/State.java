package com.bluethread.mediation.ais.fsm;

import java.util.logging.Level;

import java.util.logging.Logger;


public abstract class State <T> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(State.class.getName());
	
	
	
	private String myName;
	private int myCode;
	

	public State(String name, int ordinal){
		myName = name;
		myCode = ordinal;
		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "State", "State(String)",
					"State created  - name=" + name+"\n");
		}

		
	}
	
	
	public int ordinal(){
		return myCode;
	}
	
	public String toString(){
		return myName;
	}
	
	public abstract State<T> nextState(T evt);
	
	
	public State<T> handleEvent(T evt){
		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "State", "handleEvent(T)",
					"Handling event  - evt=" + evt+"\n");
		}

//		action(evt);
//		outAction(evt);
		State<T> next = nextState(evt);

		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "State", "handleEvent(T)",
					"Next state on evt=" + evt + ", next=" + next+"\n");
		}
		if(this !=next){
			//do out & in actions only if we're moving to a different state
			this.outAction(evt, next);
			next.inAction(evt, this);
			next.action(evt);
			return next;
		}else{
			//we stay in the same state, performing only state actions
			this.action(evt);
			return this;
		}
//		next.inAction(evt);
//		return next;
	}
	public abstract void outAction(T evt, State<T> next);
	public abstract void inAction(T evt, State <T> prev);
	public abstract void action(T evt);

}
