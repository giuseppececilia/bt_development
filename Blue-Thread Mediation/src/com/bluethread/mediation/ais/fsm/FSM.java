package com.bluethread.mediation.ais.fsm;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.bluethread.mediation.ais.fsm.State;


import com.bluethread.mediation.types.Event;


public  class FSM {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(FSM.class.getName());
	
	private State<Event> currentState;
	
	public FSM(State<Event> initial){
		currentState = initial;

		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "FSM", "FSM(State<AISMessage>)",
					"FSM initialized in state - initial=" + initial+"\n");
		}
	}
	
	
	public void handleEvent (Event evt){
		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "FSM", "handleMsg(AISMessage)",
					"Handling event - evt=" + evt +" in state "+currentState+"\n");
		}
		
		currentState = currentState.handleEvent(evt);

		
	}
	
	public State<Event> getCurrentState(){
		return currentState;
	}
	
	public void setCurrentState(State<Event> newState){
		currentState = newState;
	}
	
	public static void main(String [] args){}
}
