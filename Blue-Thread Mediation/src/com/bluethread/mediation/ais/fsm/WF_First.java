
package com.bluethread.mediation.ais.fsm;


import java.util.logging.Logger;
import java.util.logging.Level;

import com.bluethread.mediation.ais.fsm.State;
import com.bluethread.mediation.types.Event;
import com.bluethread.mediation.types.EventType;
import com.bluethread.mediation.types.Message;
import com.bluethread.mediation.types.ifc.AisHandler;

import com.bluethread.aismessages.ais.messages.AISMessage;
import com.bluethread.aismessages.ais.messages.BinaryBroadcastMessage;
import com.bluethread.aismessages.ais.messages.BlueThreadBinaryBroadcastMessage;
import com.bluethread.aismessages.ais.messages.BlueThreadConstants;
import com.bluethread.aismessages.ais.messages.StandardClassBCSPositionReport;

public class WF_First extends State<Event>{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(WF_First.class
			.getName());

	private AisHandler myHandler;

	public WF_First(AisHandler hdlr){
		super ("WF_First", 1);
		myHandler = hdlr;
		
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "WF_First", "WF_First(AisHandler)",
					"Wait for first AIS binary broadcast message\n");
		}
		
	}
	
	@Override
	public State<Event> nextState(Event evt) {
		// TODO Auto-generated method stub
		
		if(evt.getType() == EventType.Timeout){
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WF_First", "nextState(Event)",
						"Timeout received in WF_First, going to WF_ValidPosition\n");
			}
			myHandler.startPositionValidationTimer();
			return new WF_ValidPosition(myHandler);
		}
			
		
		AISMessage msg = ((Message)evt).getMsg();
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "WF_First", "nextState(AISMessage)",
					"Message received - msg=" + msg+"\n");
		}

		switch(msg.getMessageType()){
		case BinaryBroadcastMessage: // BinaryBroadcastMessage arrived!
			if (!myHandler.isMMSIValid(msg)) return this;
			// check if it's our message DAC shall be 0, FID shall be 29
			if(!(((BinaryBroadcastMessage)msg).getFunctionalId() == BlueThreadConstants.FID && ((BinaryBroadcastMessage)msg).getDesignatedAreaCode()==BlueThreadConstants.DAC)) return this;
			
			if (myHandler.isFirst((BlueThreadBinaryBroadcastMessage)msg)){
				
				if (logger.isLoggable(Level.FINE)) {
					logger.logp(Level.FINE, "WF_First",
							"nextState(AISMessage)",
							"Binary broadcast message received from the right source MMSI\n");
				}
				/*TODO move to outAction()*/
				//myHandler.decodeParameters((BlueThreadBinaryBroadcastMessage)msg);

				

				return new WF_Second(myHandler);
			}
			/*Message out of sequence*/
			else {
				
				if (logger.isLoggable(Level.FINE)) {
					logger.logp(Level.FINE, "WF_First",
							"nextState(AISMessage)", "BinaryBroadcastMessage received from the right source MMSI but out of sequence\n");
				}
				//myHandler.emptyCache();
				return this;
			}
		case StandardClassBCSPositionReport: 
			if (!myHandler.isMMSIValid(msg)) return this;

			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WF_First",
						"nextState(AISMessage)", "StandardClassBCSPositionReport received from the right source MMSI\n");
			}
			
			myHandler.stopPositionValidationTimer();
			myHandler.storeTimeAndPosition((StandardClassBCSPositionReport)msg);
			myHandler.startPositionValidationTimer();
			return this;	
		default: 
			return this;
		}
	}

	@Override
	public void outAction(Event evt, State <Event> next) {
		// TODO Auto-generated method stub
		if(evt.getType() == EventType.AisMessage)
		{
			AISMessage msg = ((Message)evt).getMsg();
			
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WF_First", "outAction(AISMessage)", " Quitting WF_First state to state: "+next+
						" on message "+ msg+"\n");
			}
			
			if(next.ordinal()==2) 
				myHandler.cacheParameters((BlueThreadBinaryBroadcastMessage)msg);
			
		}
		
						
		
	}

	@Override
	public void inAction(Event evt, State <Event> prev) {
		// TODO Auto-generated method stub
		
		if(evt.getType() == EventType.AisMessage)
		{
			AISMessage msg = ((Message)evt).getMsg();
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WF_First", "inAction(AISMessage)", " Entering WF_First state from "+prev+" on message "
						+ msg+"\n");
				}
			
			myHandler.emptyCache();
		}
		
	}

	@Override
	public void action(Event evt) {
		// TODO Auto-generated method stub
		if(evt.getType() == EventType.AisMessage)
		{
			AISMessage msg = ((Message)evt).getMsg();
			
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "WF_First", "action(AISMessage)", " Action in WF_First state - msg="
					+ msg+"\n");
			}
		}
	}

}
