package com.bluethread.mediation.ais.fsm;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.bluethread.mediation.types.Event;
import com.bluethread.mediation.types.EventType;
import com.bluethread.mediation.types.Message;
import com.bluethread.mediation.types.ifc.AisHandler;

import com.bluethread.aismessages.ais.messages.AISMessage;
import com.bluethread.aismessages.ais.messages.StandardClassBCSPositionReport;



public class WF_ValidPosition extends State <Event>{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(WF_ValidPosition.class.getName());

	
	private AisHandler myHandler;
	
	public WF_ValidPosition(AisHandler hndlr) {
		super("WF_ValidPosition", 0);
		// TODO Auto-generated constructor stub
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "WF_ValidPosition", "WF_ValidPosition(AisHandler)",
					"Wait for first AIS binary broadcast message\n");
		}
		myHandler = hndlr;
		
	}

	@Override
	public State<Event> nextState(Event evt) {
		// TODO Auto-generated method stub

		if(evt.getType() == EventType.Timeout){
			myHandler.startPositionValidationTimer();

			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WF_ValidPosition", "nextState(Event)",
						"Position timeout received, starting a new timer");
			}

			return this;
		}

		//else: it's not a timeout

		AISMessage msg = ((Message)evt).getMsg();


		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "WF_ValidPosition", "nextState(AISMessage)",
					"Message received - msg=" + msg+"\n");
		}

		switch(msg.getMessageType()){

		case PositionReportClassAScheduled:
			if(myHandler.isMMSIValid(msg)){

				if (logger.isLoggable(Level.INFO)) {
					logger.logp(Level.INFO, "WF_ValidPosition", "nextState(AISMessage)",
							"*************************************************************************************************************\n");
					logger.logp(Level.INFO, "WF_ValidPosition", "nextState(AISMessage)",
							"A Class Assigned Position Report Message received while waiting for position fix - msg=" + msg+"\n");
					logger.logp(Level.INFO, "WF_ValidPosition", "nextState(AISMessage)",
							"*************************************************************************************************************\n");
				}

			}
			return this;
		case PositionReportClassAAssignedSchedule:
			if(myHandler.isMMSIValid(msg)){

				if (logger.isLoggable(Level.INFO)) {
					logger.logp(Level.INFO, "WF_ValidPosition", "nextState(AISMessage)",
							"*************************************************************************************************************\n");
					logger.logp(Level.INFO, "WF_ValidPosition", "nextState(AISMessage)",
							"A Class Position Report Message received while waiting for position fix - msg=" + msg+"\n");
					logger.logp(Level.INFO, "WF_ValidPosition", "nextState(AISMessage)",
							"*************************************************************************************************************\n");
				}

			}
			return this;

		case BinaryBroadcastMessage: 
			if(myHandler.isMMSIValid(msg)){

				if (logger.isLoggable(Level.INFO)) {
					logger.logp(Level.INFO, "WF_ValidPosition", "nextState(AISMessage)",
							"*************************************************************************************************************\n");
					logger.logp(Level.INFO, "WF_ValidPosition", "nextState(AISMessage)",
							"Binary Broadcast Message received while waiting for position fix - msg=" + msg+"\n");
					logger.logp(Level.INFO, "WF_ValidPosition", "nextState(AISMessage)",
							"*************************************************************************************************************\n");
				}

			}
			return this;

		case StandardClassBCSPositionReport: 
			if (!myHandler.isMMSIValid(msg)) return this;

			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WF_ValidPosition",
						"nextState(AISMessage)", "StandardClassBCSPositionReport received from the right source MMSI\n");
			}
			myHandler.stopPositionValidationTimer();
			myHandler.storeTimeAndPosition((StandardClassBCSPositionReport)msg);
			return new WF_First(myHandler);	
		default: 
			return this;
		}

	}

	@Override
	public void outAction(Event evt, State <Event> next) {
		// TODO Auto-generated method stub
		if(evt.getType() == EventType.AisMessage){
			AISMessage msg = ((Message)evt).getMsg();
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WF_ValidPosition", "outAction(AISMessage)", " Quitting WF_ValidPosition state to state: "+next+
						" on message "+ msg+"\n");
			}	
		}
					
		
	}

	@Override
	public void inAction(Event evt, State <Event> prev) {
		// TODO Auto-generated method stub
		if(evt.getType() == EventType.AisMessage){
			AISMessage msg = ((Message)evt).getMsg();
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WF_ValidPosition", "inAction(AISMessage)", " Entering WF_ValidPosition state from state: "+prev+
						" on message "+ msg+"\n");
			}	
		}
		
		//myHandler.emptyCache();
		
		
	}

	@Override
	public void action(Event evt) {
		
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "WF_ValidPosition", "action(AISMessage)", " Action in WF_ValidPosition on message\n");
		}	
		// TODO Auto-generated method stub
	}

}
