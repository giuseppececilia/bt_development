
package com.bluethread.mediation.ais.fsm;

import java.util.logging.Level;
import java.util.logging.Logger;









import com.bluethread.mediation.ais.fsm.State;
import com.bluethread.mediation.types.Event;
import com.bluethread.mediation.types.EventType;
import com.bluethread.mediation.types.Message;
import com.bluethread.mediation.types.ifc.AisHandler;

import com.bluethread.aismessages.ais.messages.AISMessage;
import com.bluethread.aismessages.ais.messages.BlueThreadBinaryBroadcastMessage;
import com.bluethread.aismessages.ais.messages.StandardClassBCSPositionReport;



public class WF_Second extends State<Event>{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(WF_Second.class
			.getName());

	private AisHandler myHandler;

	public WF_Second(AisHandler hdlr){
		super ("WF_Second",2);
		myHandler = hdlr;

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "WF_Second", "WF_Second(AisHandler)",
					"Wait for Second AIS binary broadcast message\n");
		}
		
	}
	
	@Override
	public State<Event> nextState(Event evt) {
		// TODO Auto-generated method stub

		if(evt.getType() == EventType.Timeout){
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WF_Second", "nextState(Event)",
						"Timeout received in WF_Second, going to WF_ValidPosition");
			}
			myHandler.startPositionValidationTimer();
			return new WF_ValidPosition(myHandler);
		}
			
		
		AISMessage msg = ((Message)evt).getMsg();
		
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "WF_Second", "nextState(AISMessage)",
					"Message received - msg=" + msg+"\n");
		}

		switch(msg.getMessageType()){
		case BinaryBroadcastMessage: // BinaryBroadcastMessage arrived!
			if (!myHandler.isMMSIValid(msg)) return this;
			
			if (myHandler.isSecond((BlueThreadBinaryBroadcastMessage)msg)){
				

				if (logger.isLoggable(Level.FINE)) {
					logger.logp(Level.FINE, "WF_Second",
							"nextState(AISMessage)", "BinaryBroadcastMessage received from the right source MMSI\n");
				}
				/*change state*/
				return new WF_Third(myHandler);
			}
			/*Message out of sequence*/
			else {
				if (logger.isLoggable(Level.FINE)) {
					logger.logp(Level.FINE, "WF_Second",
							"nextState(AISMessage)", "BinaryBroadcastMessage received from the right source MMSI but out of sequence\n");
				}

				//come back to initial state
				return new WF_First(myHandler);
			}
		case StandardClassBCSPositionReport: 
			if (!myHandler.isMMSIValid(msg)) return this;
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WF_Second",
						"nextState(AISMessage)", "StandardClassBCSPositionReport received from the right source MMSI\n");
			}
			myHandler.stopPositionValidationTimer();
			myHandler.storeTimeAndPosition((StandardClassBCSPositionReport)msg);
			myHandler.startPositionValidationTimer();
			
			return this;	
		default: 
			return this;
		}
	}

	@Override
	public void outAction(Event evt, State <Event> next) {
		// TODO Auto-generated method stub
		
		if(evt.getType() == EventType.AisMessage)
		{
			AISMessage msg = ((Message)evt).getMsg();

			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WF_Second", "outAction(AISMessage)", "to "+next+"\n");
			}

			if(next.ordinal()==3)
				myHandler.cacheParameters((BlueThreadBinaryBroadcastMessage)msg);
		}

	}

	@Override
	public void inAction(Event evt, State <Event> prev) {
		// TODO Auto-generated method stub
		
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "WF_Second", "inAction(AISMessage)", "from "+prev+"\n");
		}

		
	}

	@Override
	public void action(Event evt) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "WF_Second", "action(AISMessage)", "\n");
		}
	}

}
