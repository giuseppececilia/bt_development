
package com.bluethread.mediation.ais.fsm;

import java.util.logging.Logger;


import java.util.logging.Level;

import com.bluethread.mediation.ais.fsm.State;
import com.bluethread.mediation.types.Event;
import com.bluethread.mediation.types.EventType;
import com.bluethread.mediation.types.Message;
import com.bluethread.mediation.types.ifc.AisHandler;

import com.bluethread.aismessages.ais.messages.AISMessage;
import com.bluethread.aismessages.ais.messages.BlueThreadBinaryBroadcastMessage;
import com.bluethread.aismessages.ais.messages.StandardClassBCSPositionReport;

public class WF_Third extends State<Event>{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(WF_Third.class
			.getName());

	private AisHandler myHandler;

	public WF_Third(AisHandler hdlr){
		super ("WF_Third",3);
		myHandler = hdlr;

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "WF_Third", "WF_Third(AisHandler)", 
					"Wait for Third AIS binary broadcast message\n");
		}
		
	
		
	}
	
	@Override
	public State<Event> nextState(Event evt) {
		// TODO Auto-generated method stub

		if(evt.getType() == EventType.Timeout){
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WF_Third", "nextState(Event)",
						"Timeout received in WF_Third, going to WF_ValidPosition");
			}
			myHandler.startPositionValidationTimer();
			return new WF_ValidPosition(myHandler);
		}
		
		AISMessage msg = ((Message)evt).getMsg();
		
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "WF_Third", "nextState(AISMessage)",
					"Message recived - msg=" + msg+"\n");
		}

		switch(msg.getMessageType()){
		case BinaryBroadcastMessage: // BinaryBroadcastMessage arrived!
			if (!myHandler.isMMSIValid(msg)) return this;
			
			if (myHandler.isThird((BlueThreadBinaryBroadcastMessage)msg)){
				

				if (logger.isLoggable(Level.FINE)) {
					logger.logp(Level.FINE, "WF_Third",
							"nextState(AISMessage)",
							"Received AIS binary broadcast message from the right MMSI\n");
				}
				return new WF_First(myHandler);
			}
			/*Message out of sequence*/
			else {
				if (logger.isLoggable(Level.FINE)) {
					logger.logp(
							Level.FINE,
							"WF_Third",
							"nextState(AISMessage)",
							"BinaryBroadcastMessage received from the right source MMSI but out of sequence\n");
				}
				return new WF_First(myHandler);
			}
		case StandardClassBCSPositionReport: 
			if (!myHandler.isMMSIValid(msg)) return this;
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(
						Level.FINE,
						"WF_Third",
						"nextState(AISMessage)",
						"StandardClassBCSPositionReport received from the right source MMSI\n");
			}
			myHandler.stopPositionValidationTimer();
			myHandler.storeTimeAndPosition((StandardClassBCSPositionReport)msg);
			myHandler.startPositionValidationTimer();
			return this;	
		default: 
			return this;
		}
	}

	@Override
	public void outAction(Event evt, State <Event> next) {
		// TODO Auto-generated method stub

		if(evt.getType() == EventType.AisMessage)
		{
			AISMessage msg = ((Message)evt).getMsg();

			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "WF_Third", "outAction(AISMessage)", "\n");
			}

			/*data collection completed must be invoked only if we exit state for the third message in a row
			 * not for messages out of sequence*/
			if(myHandler.isThird((BlueThreadBinaryBroadcastMessage)msg)){
				myHandler.cacheParameters((BlueThreadBinaryBroadcastMessage)msg);
				myHandler.dataCollectionCompleted();
			}
		}
			
		
	}

	@Override
	public void inAction(Event evt, State <Event> prev) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "WF_Third", "inAction(AISMessage)", "\n");
		}

		
	}

	@Override
	public void action(Event evt) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "WF_Third", "action(AISMessage)", "\n");
		}
	}

}
