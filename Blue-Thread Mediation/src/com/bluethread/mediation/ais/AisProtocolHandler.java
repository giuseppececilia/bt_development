package com.bluethread.mediation.ais;

import java.util.logging.Logger;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;

import com.bluethread.mediation.ais.fsm.FSM;
import com.bluethread.mediation.ais.fsm.WF_ValidPosition;
import com.bluethread.mediation.types.Event;
import com.bluethread.mediation.types.EventType;
import com.bluethread.mediation.types.IspraData;
import com.bluethread.mediation.types.Message;
import com.bluethread.mediation.types.Timeout;
import com.bluethread.mediation.types.founding.Servant;
import com.bluethread.mediation.types.ifc.AisHandler;
import com.bluethread.mediation.types.ifc.JsonDataProducer;
import com.bluethread.mediation.types.ifc.RequestHandler;
import com.bluethread.mediation.types.ifc.TimerHandler;

import com.bluethread.aismessages.ais.messages.AISMessage;
import com.bluethread.aismessages.ais.messages.BlueThreadBinaryBroadcastMessage;
import com.bluethread.aismessages.ais.messages.StandardClassBCSPositionReport;


public class AisProtocolHandler extends Servant <Event> implements AisHandler, TimerHandler{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(AisProtocolHandler.class.getName());

	private static final int POSITION_REPORT_TIMEOUT_DELAY = 180000;  //msec 
	
	private FSM myFSM;
	private long transmitting_MMSI;
	private  IspraData sampleData;
	private Integer currentTransactionID;
	private Timer positionReportTimer;
	private RequestHandler <JsonDataProducer> backEnd;
	private long timeout_in_msecs;

	public AisProtocolHandler(RequestHandler<JsonDataProducer> nextInTheChain,Properties props){
		String sourceMMSI = props.getProperty("sourceMMSI");
		if(sourceMMSI==null){
			logger.logp(Level.SEVERE, "AisProtocolHandler",
					"AisProtocolHandler(Properties)",
					"sourceMMSI not defined - sourceMMSI=" + sourceMMSI);

			
			System.exit(-1);
		}
		
		
		transmitting_MMSI = Long.valueOf(sourceMMSI);
		
		if(props.getProperty("positionReportTimeout") == null)
			timeout_in_msecs = POSITION_REPORT_TIMEOUT_DELAY;
		else timeout_in_msecs= Long.valueOf(props.getProperty("positionReportTimeout"));
		
		sampleData = new IspraData(sourceMMSI,null,null);
		positionReportTimer = new Timer();
		myFSM = new FSM(new WF_ValidPosition(this));
		backEnd = nextInTheChain;
		
		//just the first time
		startPositionValidationTimer();
		
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "AisProtocolHandler",
					"AisProtocolHandler(Properties)",
					"AIS protocol handler started waiting messages from MMSI  - sourceMMSI="
							+ sourceMMSI+"\n");
		}

	}
	
	
	@Override
	public void handleRequest(Event t) {
		// TODO Auto-generated method stub

		if(t.getType() == EventType.AisMessage){
			
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "AisProtocolHandler",
						"handleRequest(Event)", "Received event="
								+((Message) t).getMsg().toString()+"\n");
			}
		}
			
		

		myFSM.handleEvent(t);
	}
	
	
	@Override
	public void storeTimeAndPosition(StandardClassBCSPositionReport msg) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "AisProtocolHandler",
					"storeTimeAndPosition(StandardClassBCSPositionReport)",
					"Message PositionReport received, storing position\n");
		}		
		sampleData.setLatitude(msg.getLatitude());
		sampleData.setLongitude(msg.getLongitude());
		sampleData.setSamplingDate(new Date(System.currentTimeMillis()));
		
	}
	@Override
	public boolean isMMSIValid(AISMessage msg) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "AisProtocolHandler",
					"isMMSIValid(AISMessage)", "Cheching MMSI - msg=" + msg.getSourceMmsi().toString()+"\n");
		}

		if(msg.getSourceMmsi().getMMSI().longValue() == transmitting_MMSI){
			
			if (logger.isLoggable(Level.INFO)) {
				logger.logp(Level.INFO, "AisProtocolHandler",
						"isMMSIValid(BinaryBroadcastMessage)",
						"Received message "+msg+" from valid MMSI\n");
			}
			
			return true;
		}
		else return false;
	}
	@Override
	public synchronized boolean isFirst(BlueThreadBinaryBroadcastMessage msg) {
		// TODO Auto-generated method stub


		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "AisProtocolHandler",
					"isSecond(BinaryBroadcastMessage)",
					"Checking if it's the first AIS Binary broadcast out of 3 \n[seqId=" + msg.getSequenceId().intValue()+", trID="+msg.getTransactionId()+"] currentTransactionID="+currentTransactionID);
		}
		

		// it's our message
		if (msg.getSequenceId().intValue() == 0) {
			currentTransactionID = msg.getTransactionId();

			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "AisProtocolHandler",
						"isFirst(BlueThreadBinaryBroadcastMessage)",
						"current transactionID="+currentTransactionID);
			}

			return true;
		}
		else return false;	
	}
	
	@Override
	public void cacheParameters(BlueThreadBinaryBroadcastMessage msg) {
		// TODO Auto-generated method stub
		
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "AisProtocolHandler",
					"decodeParameters(BinaryBroadcastMessage)",
					"Decoding parameters from received message =" + msg+"\n");
		}
		
		//first 2 AIS messages of the sequence contain 2 params, third only one
		if(msg.getSequenceId()<=1){
			sampleData.addParameter(msg.getP1Code(), new Float(msg.getP1Value()));
			sampleData.addParameter(msg.getP2Code(), new Float(msg.getP2Value()));	

			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "AisProtocolHandler",
						"decodeParameters(BlueThreadBinaryBroadcastMessage)",
						"Cached the following parameters P1 [" + msg.getP1Code()+
						                                    ","+ msg.getP1Value()+"]" 
								                               + "; P2 ["+ msg.getP2Code()+
							                                    ","+ msg.getP2Value()+"]\n");
			} 
		}
		else
		{
			if (logger.isLoggable(Level.FINE)) {
				logger.logp(Level.FINE, "AisProtocolHandler",
						"decodeParameters(BlueThreadBinaryBroadcastMessage)",
						"Cached the following parameter P1 [" + msg.getP1Code()+
						                                    ","+ msg.getP1Value()+"]\n");
			} 
			
			sampleData.addParameter(msg.getP1Code(), new Float(msg.getP1Value()));
		}
				
	}
	@Override
	public void emptyCache() {
		// methods used to clean accumulated data in case of FSM reset

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "AisProtocolHandler", "emptyCache()",
					"FSM set in initial state, clean the cache of parameters\n");
		}
		sampleData.cleanData();;
		
	}
	@Override
	public synchronized boolean isSecond(BlueThreadBinaryBroadcastMessage msg) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "AisProtocolHandler",
					"isSecond(BinaryBroadcastMessage)",
					"Checking if it's the second AIS Binary broadcast out of 3 \n[seqId=" + msg.getSequenceId().intValue()+", trID="+msg.getTransactionId()+"] currentTransactionID="+currentTransactionID);
		}
		if ((msg.getTransactionId().intValue() == currentTransactionID.intValue())&&(msg.getSequenceId().intValue() == 1)) 
			return true;
			else return false;
		
	}
	@Override
	public synchronized boolean isThird(BlueThreadBinaryBroadcastMessage msg) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "AisProtocolHandler",
					"isThird(BinaryBroadcastMessage)",
					"Checking if it's the third AIS Binary broadcast out of 3 =" + msg+"\n");
		}
		if ((msg.getSequenceId().intValue() == 2) && (msg.getTransactionId().intValue() == currentTransactionID.intValue())) {
			//good point to create the SondaData to fill in afterwards
			return true;
		}
		else return false;
	}


	@Override
	public void handleAISMessage(AISMessage msg) {
		// TODO Auto-generated method stub
		Message msgToFsm = new Message(msg);
		feedRequest(msgToFsm);
	}


	@Override
	public synchronized void dataCollectionCompleted() {
		// TODO Auto-generated method stub
		
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "AisProtocolHandler",
					"dataCollectionCompleted()",
					"Data collection completed, sending record to back end "+ sampleData.toString()+"\n");
		}
		
		//update to a "fresh" sampling time
		sampleData.setSamplingDate(new Date(System.currentTimeMillis()));
		// clone sampleData otherwise internal data could be affected while reading in next steps 
		backEnd.feedRequest(sampleData.clone());
		
		
	}
	


	@Override
	public void startPositionValidationTimer() {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "AisProtocolHandler",
					"startPositionValidationTimer()",
					"Starting timer waiting for an updated position AIS message\n");
		}

		positionReportTimer.schedule(new TimeoutTask(), timeout_in_msecs);
	}


	@Override
	public void stopPositionValidationTimer() {
		// TODO Auto-generated method stub
		
		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "AisProtocolHandler",
					"stopPositionValidationTimer()",
					"Stopping waiting timer for an updated position AIS message\n");
		}
		
		positionReportTimer.cancel();
		positionReportTimer = new Timer();
	}
	
	
	private class TimeoutTask extends TimerTask{
		/**
		 * Logger for this class
		 */
		private final Logger logger = Logger.getLogger(TimeoutTask.class
				.getName());
		

		@Override
		public void run() {
			// TODO Auto-generated method stub
			AisProtocolHandler.this.timeout();
		}
		
	}


	@Override
	public void timeout() {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "AisProtocolHandler", "timeout()",
					"Timeout received: no position update received from remote terminal with MMSI:"+transmitting_MMSI+"\n");
		}
		
		this.feedRequest(new Timeout());
	}


	@Override
	public void cleanUpBeforeClosing() {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "AisProtocolHandler",
					"cleanUpBeforeClosing()",
					"Clean up local data before closing\n");
		}

		positionReportTimer.cancel();
	
	}
	
}
