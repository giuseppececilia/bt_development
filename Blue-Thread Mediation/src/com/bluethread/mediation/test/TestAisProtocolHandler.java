package com.bluethread.mediation.test;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.bluethread.mediation.types.ifc.AisHandler;

import com.bluethread.aismessages.ais.messages.AISMessage;
import com.bluethread.aismessages.ais.messages.BlueThreadBinaryBroadcastMessage;
import com.bluethread.aismessages.ais.messages.StandardClassBCSPositionReport;

public class TestAisProtocolHandler implements AisHandler{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(TestAisProtocolHandler.class.getName());


	@Override
	public void storeTimeAndPosition(StandardClassBCSPositionReport msg) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "TestAisProtocolHandler",
					"storeTimeAndPosition(StandardClassBCSPositionReport)",
					"msg.toString() - msg=" + msg);
		}

		
	}

	@Override
	public boolean isMMSIValid(AISMessage msg) {
		

		// TODO Auto-generated method stub
		

		if (logger.isLoggable(Level.FINE)) {
			logger.logp(Level.FINE, "TestAisProtocolHandler",
					"isMMSIValid(AISMessage)", "msg=" + msg);
		}

		return false;
	}

	@Override
	public boolean isFirst(BlueThreadBinaryBroadcastMessage msg) {
		// TODO Auto-generated method stub
		
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "TestAisProtocolHandler",
					"isFirst(BinaryBroadcastMessage)", "msg=" + msg);
		}

		
		return false;
	}

	@Override
	public void cacheParameters(BlueThreadBinaryBroadcastMessage msg) {
		// TODO Auto-generated method stub
		
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "TestAisProtocolHandler",
					"decodeParameters(BinaryBroadcastMessage)", "msg=" + msg);
		}
		
	}

	@Override
	public void emptyCache() {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "TestAisProtocolHandler", "emptyCache()",
					"");
		}
	}

	@Override
	public boolean isSecond(BlueThreadBinaryBroadcastMessage msg) {
		// TODO Auto-generated method stub
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "TestAisProtocolHandler",
					"isSecond(BinaryBroadcastMessage)", "msg=" + msg);
		}

		return false;
	}

	@Override
	public boolean isThird(BlueThreadBinaryBroadcastMessage msg) {
		// TODO Auto-generated method stub
		
		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "TestAisProtocolHandler",
					"isThird(BinaryBroadcastMessage)", "msg=" + msg);
		}

		return false;
	}

	@Override
	public void handleAISMessage(AISMessage msg) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.INFO)) {
			logger.logp(Level.INFO, "TestAisProtocolHandler",
					"handleAISMessage(AISMessage)", "msg=" + msg);
		}
	}

	@Override
	public void dataCollectionCompleted() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startPositionValidationTimer() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stopPositionValidationTimer() {
		// TODO Auto-generated method stub
		
	}

}
