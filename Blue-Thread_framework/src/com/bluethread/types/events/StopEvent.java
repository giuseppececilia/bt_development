/**
 * 
 */
package com.bluethread.types.events;

/**
 * @author Giuseppe Cecilia
 *
 */
public class StopEvent extends Event {

	public enum Reason {
		NORMAL(0, "NORMAL"), FATAL_ERROR(10, "FATAL ERROR"), GARCEFUL_SHUTDOWN(20, "GRACEFUL SHUTDOWN");

		private int eoscode;
		private String eosreason;

		private Reason(int code, String reason) {
			eoscode = code;
			eosreason = reason;
		}

		public String toString() {

			return "Reason:" + eosreason + " ,Code:" + eoscode;

		}

	};

	private Reason myReason;

	/**
	 * @param et
	 */
	public StopEvent(Reason reason, String srcId) {
		super(EventType.STOP, srcId);
		// TODO Auto-generated constructor stub

		myReason = reason;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StopEvent (" + myReason + ")";
	}

}
