/**
 * 
 */
package com.bluethread.types.events;

/**
 * @author Giuseppe Cecilia
 *
 */
public class CloseCompletedEvent extends Event {

	/**
	 * @param et
	 */
	public CloseCompletedEvent(String srcId) {
		super(EventType.CLOSE_COMPLETED, srcId);
		// TODO Auto-generated constructor stub

	}

}
