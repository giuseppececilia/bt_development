/**
 * 
 */
package com.bluethread.types.events;

/**
 * @author Giuseppe Cecilia
 *
 */
public class TimeoutEvent extends Event {

	public enum Reason {
		START_TIMEOUT(0, "START"), SHUTDOWN_TIMEOUT(10, "SHUTDOWN"), OPEN_TIMEOUT(20, "OPEN"), CLOSE_TIMEOUT(30, "CLOSE");

		private int timeoutcode;
		private String timeoutreason;

		private Reason(int code, String reason) {
			timeoutcode = code;
			timeoutreason = reason;
		}

		public String toString() {

			return "Reason:" + timeoutreason + ", code:" + timeoutcode;

		}

	};

	private Reason myReason;

	/**
	 * @param et
	 */
	public TimeoutEvent(Reason reason, String srcId) {
		super(EventType.TIMEOUT, srcId);
		// TODO Auto-generated constructor stub

		myReason = reason;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TimeoutEvent (" + myReason + ")";
	}

}
