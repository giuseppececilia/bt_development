/**
 * 
 */
package com.bluethread.types.events;

/**
 * @author Giuseppe Cecilia
 *
 */
public class StopCompletedEvent extends Event {

	/**
	 * @param et
	 */
	public StopCompletedEvent(String srcId) {
		super(EventType.STOP_COMPLETED, srcId);
		// TODO Auto-generated constructor stub

	}

}
