/**
 * 
 */
package com.bluethread.types.events;

/**
 * @author Giuseppe Cecilia
 *
 */
public class OpenCompletedEvent extends Event {

	/**
	 * @param et
	 */
	public OpenCompletedEvent(String srcId) {
		super(EventType.OPEN_COMPLETED, srcId);
		// TODO Auto-generated constructor stub

	}

}
