/**
 * 
 */
package com.bluethread.types.events;

import dk.dma.ais.message.AisMessage;

/**
 * @author Giuseppe Cecilia
 *
 */
public class AisMessageEvent extends Event {

	private AisMessage theMessage;

	public AisMessageEvent(AisMessage msg, String srcId) {
		super(EventType.AIS_MESSAGE, srcId);
		// TODO Auto-generated constructor stub
		theMessage = msg;
	}

	public AisMessage getTheMessage() {
		return theMessage;
	}

	public String toString() {
		return "AisMessageEvent";
	}

}
