/**
 * 
 */
package com.bluethread.types.events;

/**
 * @author Giuseppe Cecilia
 *
 */
public class StartCompletedEvent extends Event {

	/**
	 * @param et
	 */
	public StartCompletedEvent(String srcId) {
		super(EventType.START_COMPLETED, srcId);
		// TODO Auto-generated constructor stub

	}

}
