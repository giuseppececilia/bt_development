/**
 * 
 */
package com.bluethread.types.events;

/**
 * @author Giuseppe Cecilia
 *
 */
public class StartEvent extends Event {

	public enum Reason {

		NORMAL(0, "NORMAL"), RESTART(10, "RESTART");

		private int startcode;
		private String startreason;

		private Reason(int code, String reason) {
			startcode = code;
			startreason = reason;
		}

		public String toString() {

			return "Reason:" + startreason + " ,Code:" + startcode;

		}

	};

	private Reason startRsn;

	/**
	 * @param et
	 */
	public StartEvent(StartEvent.Reason rsn, String srcId) {
		super(EventType.START, srcId);
		startRsn = rsn;
		// TODO Auto-generated constructor stub
	}

	public Reason getReason() {
		return startRsn;
	}

}
