package com.bluethread.types.events;

public class Event implements Comparable<Event> {

	private EventType myType;
	private String srcId;

	public Event(EventType et, String sourceId) {
		myType = et;
		srcId = sourceId;

	}

	public EventType getType() {
		return myType;
	}

	public String getSource() {
		return srcId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Event o) {
		// TODO Auto-generated method stub
		return myType.getPrio() - o.getType().getPrio();
	}

	public String toString() {

		return "<event=" + myType.toString() + ",source=" + srcId + ">";
	}

}
