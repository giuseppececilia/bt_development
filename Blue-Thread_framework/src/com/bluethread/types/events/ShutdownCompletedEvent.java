/**
 * 
 */
package com.bluethread.types.events;

/**
 * @author Giuseppe Cecilia
 *
 */
public class ShutdownCompletedEvent extends Event {

	/**
	 * @param et
	 */
	public ShutdownCompletedEvent(String srcId) {
		super(EventType.SHUTDOWN_COMPLETED, srcId);
		// TODO Auto-generated constructor stub

	}

}
