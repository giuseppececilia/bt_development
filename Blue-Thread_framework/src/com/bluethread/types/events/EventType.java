package com.bluethread.types.events;

/*
 * EventType with priority to be used when events are queued in priority queues
 */

public enum EventType {
//	AIS_MESSAGE(30), 
//	COMMAND(20), 
//	TIMEOUT(20), 
//	STOP(10), 
//	START(10), 
//	START_COMPLETED(10), 
//	STOP_COMPLETED(10), 
//	OPEN(10), 
//	OPEN_COMPLETED(10), 
//	CLOSE(10),
//	CLOSE_COMPLETED(10),
//	SHUTDOWN(10);
	
	
	AIS_MESSAGE(10), 
	COMMAND(10), 
	TIMEOUT(10), 
	STOP(10), 
	START(10), 
	START_COMPLETED(10), 
	STOP_COMPLETED(10), 
	OPEN(10), 
	OPEN_COMPLETED(10), 
	CLOSE(10),
	CLOSE_COMPLETED(10),
	SHUTDOWN(10),
	SHUTDOWN_COMPLETED(10);

	private int assignedPriority;

	EventType(int priority) {
		assignedPriority = priority;

	}

	public int getPrio() {
		return assignedPriority;
	}
}
