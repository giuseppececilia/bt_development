/**
 * 
 */
package com.bluethread.types.constants;

/**
 * @author Giuseppe Cecilia
 *
 */
public class Constants {

	public static final int DEFAULT_START_TIMEOUT = 10000;
	public static final int DEFAULT_RESTART_TIMEOUT = 15000;
	public static final int DEFAULT_SHUTDOWN_TIMEOUT = 10000;
	public static final int DEFAULT_OPEN_TIMEOUT = 10000;
	public static final int DEFAULT_CLOSE_TIMEOUT = 10000;
	public static final int DEFAULT_EVENT_QUEUE_SIZE = 10;
	public static final String QUEUE_KEY = "system.events.queuesize";
	public static final String START_TIMER_KEY = "system.timers.startTimeout";
	public static final String RESTART_TIMER_KEY = "system.timers.restartTimeout";
	public static final String SHUTDOWN_TIMER_KEY = "system.timers.shutdownTimeout";
	public static final String OPEN_TIMER_KEY = "system.timers.openTimeout";
	public static final String CLOSE_TIMER_KEY = "system.timers.closeTimeout";
	public static final String SYS_ID = "MainSystem";

}
