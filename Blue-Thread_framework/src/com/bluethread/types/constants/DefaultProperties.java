package com.bluethread.types.constants;

import java.util.Properties;

public class DefaultProperties extends Properties{

	
	private DefaultProperties(){
		setProperty(Constants.QUEUE_KEY, Integer.toString(Constants.DEFAULT_EVENT_QUEUE_SIZE));
		setProperty(Constants.START_TIMER_KEY, Integer.toString(Constants.DEFAULT_START_TIMEOUT));
		setProperty(Constants.RESTART_TIMER_KEY, Integer.toString(Constants.DEFAULT_RESTART_TIMEOUT));
		setProperty(Constants.SHUTDOWN_TIMER_KEY, Integer.toString(Constants.DEFAULT_SHUTDOWN_TIMEOUT));
		setProperty(Constants.OPEN_TIMER_KEY, Integer.toString(Constants.DEFAULT_OPEN_TIMEOUT));
		setProperty(Constants.CLOSE_TIMER_KEY, Integer.toString(Constants.DEFAULT_CLOSE_TIMEOUT));
	}
	
	private static DefaultProperties theInstance = new DefaultProperties();
	
	public static Properties getDefaultProperties(){
	
		return (Properties)theInstance;
	}
	
}
