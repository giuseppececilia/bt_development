package com.bluethread.types.fsm;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.bluethread.types.events.Event;

public class FSM {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(FSM.class.getName());

	private State<Event> currentState;

	public FSM(State<Event> initial) {
		currentState = initial;

		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "FSM", "FSM(State<Event>)", "FSM initialized in state =" + initial + "\n");
		}
	}

	public synchronized void handleEvent(Event evt) {
		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "FSM", "handleEvent(event)",
					"FSM - Handling event " + evt + " in state " + currentState + "\n");
		}

		currentState = currentState.handleEvent(evt);

		if (logger.isLoggable(Level.FINER)) {
			logger.logp(Level.FINER, "FSM", "handleEvent(event)", "FSM new state " + currentState + "\n");
		}

	}

	public State<Event> getCurrentState() {
		return currentState;
	}

	public void setCurrentState(State<Event> newState) {
		currentState = newState;
	}

}
