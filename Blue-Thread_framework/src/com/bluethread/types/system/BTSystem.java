/**
 * 
 */
package com.bluethread.types.system;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;




import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Level;
//import java.util.logging.Logger;

import com.bluethread.types.constants.Constants;
import com.bluethread.types.constants.DefaultProperties;
import com.bluethread.types.events.CloseCompletedEvent;
import com.bluethread.types.events.CloseEvent;
import com.bluethread.types.events.CommandEvent;
import com.bluethread.types.events.Event;
import com.bluethread.types.events.OpenCompletedEvent;
import com.bluethread.types.events.OpenEvent;
import com.bluethread.types.events.ShutdownCompletedEvent;
import com.bluethread.types.events.ShutdownEvent;
import com.bluethread.types.events.StartCompletedEvent;
import com.bluethread.types.events.StartEvent;
import com.bluethread.types.events.TimeoutEvent;
import com.bluethread.types.fsm.State;
import com.bluethread.types.system.interfaces.ObserverCallbackIfc;
import com.bluethread.types.system.interfaces.SubsystemIfc;
import com.bluethread.types.system.interfaces.SystemContextIfc;
import com.bluethread.types.system.states.SystemFSM;

/**
 * @author Giuseppe Cecilia
 *
 */

/**
 * 
 * @author Peppe TODO Implement complete shutdown flow
 *
 */
public abstract class BTSystem {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(BTSystem.class.getName());

	/**
	 * Logger for this class
	 */
	//private static final Logger logger = Logger.getLogger(BTSystem.class.getName());
	
	

	// private RingBuffer<Event> queueOfEvents;
	protected BlockingQueue<Event> queueOfEvents;
	protected BlockingQueue<Event> historyQueue;

	protected Properties myProps;
	private HashMap<String, SubsystemIfc> mySubsystems;
	private SystemEngine myEngine;

	private SystemContext myContext;

	private SystemFSM myFSM;

	private int startTimeout;
	private int restartTimeout;
	private int shutdownTimeout;
	private int openTimeout;
	private int closeTimeout;
	private static Properties defaultConfigProps;

	static {
		defaultConfigProps = new Properties();
		InputStream inputProperties = Thread.currentThread().getContextClassLoader().getResourceAsStream("com/bluethread/types/constants/config.properties");
		try {
			defaultConfigProps.load(inputProperties);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public BTSystem() {
		this(defaultConfigProps);
		//this(DefaultProperties.getDefaultProperties());

	}

	public BTSystem(Properties props) {

		int queueSize;
//		if (props == null) {
//			myProps = DefaultProperties.getDefaultProperties();
//
//		} else {
//			myProps = props;
//		}
		
		myProps = props;

		/*
		 * Read configuration and set default in case parameters are missing in
		 * the configuration received
		 */
		try {
			queueSize = Integer.parseInt(myProps.getProperty(Constants.QUEUE_KEY));
			/*
			 * Queue of events can be a priority queue where events are
			 * prioritized according to their assigned priority This is to be
			 * fine adjusted in order to find the right priorities otherwise FSM
			 * can go in never ending loop due to events postponed
			 */
			// queueOfEvents = new PriorityBlockingQueue<Event>(queueSize);
			queueOfEvents = new LinkedBlockingDeque<Event>(queueSize);
			historyQueue = new LinkedBlockingDeque<Event>(queueSize);

			if (logger.isInfoEnabled()) {
				logger.info("BTSystem(Properties) - test"); //$NON-NLS-1$
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Properties - System event queue size = " + queueSize + "\n"); //$NON-NLS-1$
			}
		} catch (NumberFormatException ex) {
			if (logger.isDebugEnabled()) {
				logger.debug("Properties - System event queue size not defined, setting default value:"+Constants.DEFAULT_EVENT_QUEUE_SIZE+"\n"); //$NON-NLS-1$
			}
			myProps.setProperty(Constants.QUEUE_KEY, Integer.toString(Constants.DEFAULT_EVENT_QUEUE_SIZE));
			queueOfEvents = new LinkedBlockingDeque<Event>(Constants.DEFAULT_EVENT_QUEUE_SIZE);
			historyQueue = new LinkedBlockingDeque<Event>(Constants.DEFAULT_EVENT_QUEUE_SIZE);
		}

		try {
			startTimeout = Integer.parseInt(myProps.getProperty(Constants.START_TIMER_KEY));

			if (logger.isDebugEnabled()) {
				logger.debug("Properties - start timeout = " + startTimeout + "\n"); //$NON-NLS-1$
			}
		} catch (NumberFormatException ex) {
			if (logger.isDebugEnabled()) {
				logger.debug("Properties - start timeout not defined, setting default value \n"); //$NON-NLS-1$
			}
			myProps.setProperty(Constants.START_TIMER_KEY, Integer.toString(Constants.DEFAULT_START_TIMEOUT));
			startTimeout = Constants.DEFAULT_START_TIMEOUT;
		}
		try {
			restartTimeout = Integer.parseInt(myProps.getProperty(Constants.RESTART_TIMER_KEY));

			if (logger.isDebugEnabled()) {
				logger.debug("Properties - start timeout = " + restartTimeout + "\n"); //$NON-NLS-1$
			}
		} catch (NumberFormatException ex) {
			if (logger.isDebugEnabled()) {
				logger.debug("Properties - start timeout not defined, setting default value \n"); //$NON-NLS-1$
			}
			myProps.setProperty(Constants.RESTART_TIMER_KEY, Integer.toString(Constants.DEFAULT_RESTART_TIMEOUT));
			startTimeout = Constants.DEFAULT_RESTART_TIMEOUT;
		}

		try {
			shutdownTimeout = Integer.parseInt(props.getProperty(Constants.SHUTDOWN_TIMER_KEY));

			if (logger.isDebugEnabled()) {
				logger.debug("Properties - stop timeout = " + shutdownTimeout + "\n"); //$NON-NLS-1$
			}
		} catch (NumberFormatException ex) {
			if (logger.isDebugEnabled()) {
				logger.debug("Properties - stop timeout not defined, setting default value \n"); //$NON-NLS-1$
			}
			myProps.setProperty(Constants.SHUTDOWN_TIMER_KEY, Integer.toString(Constants.DEFAULT_SHUTDOWN_TIMEOUT));
			shutdownTimeout = Constants.DEFAULT_SHUTDOWN_TIMEOUT;
		}

		try {
			openTimeout = Integer.parseInt(props.getProperty(Constants.OPEN_TIMER_KEY));

			if (logger.isDebugEnabled()) {
				logger.debug("Properties - open timeout = " + openTimeout + "\n"); //$NON-NLS-1$
			}
		} catch (NumberFormatException ex) {
			if (logger.isDebugEnabled()) {
				logger.debug("Properties - open timeout not defined, setting default value \n"); //$NON-NLS-1$
			}
			myProps.setProperty(Constants.OPEN_TIMER_KEY, Integer.toString(Constants.DEFAULT_OPEN_TIMEOUT));
			openTimeout = Constants.DEFAULT_OPEN_TIMEOUT;
		}

		try {
			closeTimeout = Integer.parseInt(props.getProperty(Constants.CLOSE_TIMER_KEY));

			if (logger.isDebugEnabled()) {
				logger.debug("Properties - close timeout = " + closeTimeout + "\n"); //$NON-NLS-1$
			}
		} catch (NumberFormatException ex) {
			if (logger.isDebugEnabled()) {
				logger.debug("Properties - close timeout not defined, setting default value \n"); //$NON-NLS-1$
			}
			myProps.setProperty(Constants.CLOSE_TIMER_KEY, Integer.toString(Constants.DEFAULT_CLOSE_TIMEOUT));
			closeTimeout = Constants.DEFAULT_CLOSE_TIMEOUT;
		}

		mySubsystems = new HashMap<String, SubsystemIfc>();
		myContext = new SystemContext();

		myEngine = new SystemEngine();
		myEngine.start();

		myFSM = new SystemFSM(myContext);
		myFSM.setCurrentState(SystemFSM.INITIAL_STATE);

		loadSubsystems(mySubsystems);
		for (Entry<String, SubsystemIfc> pair : mySubsystems.entrySet()) {

			pair.getValue().setObserverCallback(myContext);

		}
		Runtime.getRuntime().addShutdownHook(new ShutdownInitiator());

		if (logger.isInfoEnabled()) {
			logger.info("***************************************************************************************************************************\n");
			logger.info("Blue Thread System - loaded configuration " + myProps.toString() + "\n");
			logger.info("Blue Thread System - loaded subsystems " + mySubsystems.toString() + "\n");
			logger.info("***************************************************************************************************************************\n");
		}

	}

	public Properties getConfiguration() {
		return myProps;
	}

	public State<Event> getCurrentState() {

		return myFSM.getCurrentState();

	}

	public void shutdown() {

		queueOfEvents.add(new ShutdownEvent(ShutdownEvent.Reason.NORMAL, Constants.SYS_ID));

	}

	public void start() {
		queueOfEvents.add(new StartEvent(StartEvent.Reason.NORMAL, Constants.SYS_ID));
	}

	public void open() {
		queueOfEvents.add(new OpenEvent(Constants.SYS_ID));
	}

	public void close() {
		queueOfEvents.add(new CloseEvent(Constants.SYS_ID));
	}

	protected abstract void loadSubsystems(HashMap<String, SubsystemIfc> subsystems);

	private void handleEvent(Event evt) {

		if (logger.isDebugEnabled()) {
			logger.debug("System - Handling event " + evt + "\n"); //$NON-NLS-1$
		}
		myFSM.handleEvent(evt);
	}

	private class ShutdownInitiator extends Thread {

		public void run() {

			if (logger.isInfoEnabled()) {
				logger.info("Started shutdown procedure"); //$NON-NLS-1$
			}
			BTSystem.this.myContext.gracefulShutdownSubsystems();
		}

	}

	private class Executor extends Thread {

		private Event evtToHandle;

		public Executor(Event evt) {
			evtToHandle = evt;
		}

		public void run() {

			if (logger.isDebugEnabled()) {
				logger.debug("Started executor " + getName() + "\n"); //$NON-NLS-1$
			}
			handleEvent(evtToHandle);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#finalize()
		 */
		@Override
		protected void finalize() throws Throwable {
			// TODO Auto-generated method stub
			super.finalize();

			if (logger.isDebugEnabled()) {
				logger.debug("Released executor " + getName() + "\n"); //$NON-NLS-1$
			}
		}
	}

	private class SystemEngine extends Thread {

		private boolean stop = false;

		public SystemEngine() {
			super("SystemEngine Thread");
		}

		public void stopEngine() {
			stop = true;
			// just deblock thread which is waiting
			queueOfEvents.add(new CommandEvent(Constants.SYS_ID));
		}

		public void run() {
			if (logger.isDebugEnabled()) {
				logger.debug("Starting SystemEngine thread \n"); //$NON-NLS-1$
			}

			while (!stop) {
				Event evt;
				try {
					evt = queueOfEvents.take();
					if (logger.isDebugEnabled()) {
						logger.debug("SystemEngine - Event received " + evt + "\n"); //$NON-NLS-1$
					}

					(new Executor(evt)).start();

					// BTSystem.this.handleEvent(evt);

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			if (logger.isDebugEnabled()) {
				logger.debug("STOP received - quitting SystemEngine Thread\n"); //$NON-NLS-1$
			}

		}

	}

	private class SystemContext implements SystemContextIfc, ObserverCallbackIfc {

		/*
		 * 
		 * This is the interface exposed to the FSM for the actions in each
		 * state
		 */

		
		/*
		 * These hashmaps contain the requests sent to the subsystems to start/open/close/shutdown waiting to receive action completed 
		 */
		private HashMap<String, SubsystemIfc> pendingStartRequests = new HashMap<String, SubsystemIfc>();
		private HashMap<String, SubsystemIfc> pendingShutdownRequests = new HashMap<String, SubsystemIfc>();
		private HashMap<String, SubsystemIfc> pendingOpenRequests = new HashMap<String, SubsystemIfc>();
		private HashMap<String, SubsystemIfc> pendingCloseRequests = new HashMap<String, SubsystemIfc>();

		private Timer startTimer;
		private Timer shutdownTimer;
		private Timer openTimer;
		private Timer closeTimer;

		@Override
		public synchronized void startSubsystems(StartEvent.Reason rsn) {
			// TODO Auto-generated method stub

			startTimer = new Timer();
			startTimer.schedule(new TimerTask() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (logger.isInfoEnabled()) {
						logger.info("Start timeout expired\n"); //$NON-NLS-1$
					}

					queueOfEvents.add(new TimeoutEvent(TimeoutEvent.Reason.START_TIMEOUT, Constants.SYS_ID));

				}

			}, startTimeout);

			if (logger.isDebugEnabled()) {
				logger.debug("Subsystems start timer ON\n"); //$NON-NLS-1$
			}

			for (Entry<String, SubsystemIfc> pair : mySubsystems.entrySet()) {
				pendingStartRequests.put(pair.getKey(), pair.getValue());
			}
			// split in 2 for cycle to avoid receiving start completed when
			// still sending starts
			for (Entry<String, SubsystemIfc> pair : mySubsystems.entrySet()) {
				switch (rsn) {

				case RESTART:
					pair.getValue().restart();
					break;
				default:
					pair.getValue().start();

				}

			}

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SystemContextIfc#
		 * stopSubsystems()
		 */
		@Override
		public synchronized void shutdownSubsystems() {

			if (logger.isDebugEnabled()) {
				logger.debug("Shutdown subsystems\n"); //$NON-NLS-1$
			}

			// TODO Auto-generated method stub

			shutdownTimer = new Timer();
			shutdownTimer.schedule(new TimerTask() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (logger.isInfoEnabled()) {
						logger.info("Stop timeout expired\n"); //$NON-NLS-1$
					}

					queueOfEvents.add(new TimeoutEvent(TimeoutEvent.Reason.SHUTDOWN_TIMEOUT, Constants.SYS_ID));

				}

			}, shutdownTimeout);

			for (Entry<String, SubsystemIfc> pair : mySubsystems.entrySet()) {

				pair.getValue().setObserverCallback(this);
				pair.getValue().shutdown();
				pendingShutdownRequests.put(pair.getKey(), pair.getValue());
			}

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.ObserverCallbackIfc#
		 * startCompeted()
		 */
		@Override
		public synchronized void startCompleted(String id) {
			// TODO Auto-generated method stub

			if (logger.isDebugEnabled()) {
				logger.debug("Subsystem completed start - id=" + id + "\n"); //$NON-NLS-1$
			}

			if (!pendingStartRequests.containsKey(id)) {

				if (logger.isInfoEnabled()) {
					logger.info("Start completed received from subsystem not found " + id + "\n"); //$NON-NLS-1$
				}
				return;
			}
			pendingStartRequests.remove(id);
			if (pendingStartRequests.size() == 0) {
				startTimer.cancel();

				if (logger.isInfoEnabled()) {
					logger.info("All subsystems completed start procedure \n"); //$NON-NLS-1$
				}

				queueOfEvents.add(new StartCompletedEvent(Constants.SYS_ID));
			}

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.ObserverCallbackIfc#
		 * portsOpened()
		 */
		@Override
		public synchronized void portsOpened(String id) {
			// TODO Auto-generated method stub

			if (logger.isDebugEnabled()) {
				logger.debug("Subsystem completed open - id=" + id + "\n"); //$NON-NLS-1$
			}

			if (!pendingOpenRequests.containsKey(id)) {

				if (logger.isInfoEnabled()) {
					logger.info("Open completed received from subsystem not found " + id + "\n"); //$NON-NLS-1$
				}
				return;
			}

			pendingOpenRequests.remove(id);
			if (pendingOpenRequests.size() == 0) {
				openTimer.cancel();

				if (logger.isInfoEnabled()) {
					logger.info("All subsystems completed open procedure \n"); //$NON-NLS-1$
				}

				queueOfEvents.add(new OpenCompletedEvent(Constants.SYS_ID));
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.ObserverCallbackIfc#
		 * portsClosed()
		 */
		@Override
		public synchronized void portsClosed(String id) {

			// TODO Auto-generated method stub

			if (logger.isDebugEnabled()) {
				logger.debug("Subsystem completed close - id=" + id + "\n"); //$NON-NLS-1$
			}

			if (!pendingCloseRequests.containsKey(id)) {

				if (logger.isInfoEnabled()) {
					logger.info("Close completed received from subsystem not found " + id + "\n"); //$NON-NLS-1$
				}
				return;
			}

			pendingCloseRequests.remove(id);
			if (pendingCloseRequests.size() == 0) {
				closeTimer.cancel();

				if (logger.isInfoEnabled()) {
					logger.info("All subsystems completed close procedure \n"); //$NON-NLS-1$
				}

				queueOfEvents.add(new CloseCompletedEvent(Constants.SYS_ID));
			}

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.ObserverCallbackIfc#
		 * shutdownCompleted()
		 */
		@Override
		public synchronized void shutdownCompleted(String id) {
			// TODO Auto-generated method stub

			if (logger.isDebugEnabled()) {
				logger.debug("Subsystem completed stop - id=" + id + "\n"); //$NON-NLS-1$
			}

			pendingShutdownRequests.remove(id);
			if (pendingShutdownRequests.size() == 0) {
				shutdownTimer.cancel();

				if (logger.isInfoEnabled()) {
					logger.info("All subsystems completed shutdown procedure \n"); //$NON-NLS-1$
				}

				queueOfEvents.add(new ShutdownCompletedEvent(Constants.SYS_ID));
			}

		}

		public void defer(Event evt) {

			if (logger.isDebugEnabled()) {
				logger.debug("SystemContext - Event postponed " + evt + "\n"); //$NON-NLS-1$
			}
			queueOfEvents.add(evt);
		}

		public void remember(Event evt) {
			if (logger.isDebugEnabled()) {
				logger.debug("SystemContext - Event stored in the history " + evt + "\n"); //$NON-NLS-1$
			}
			historyQueue.add(evt);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.bluethread.types.system.interfaces.SystemContextIfc#openDoors()
		 */
		@Override
		public void openSubsystemsDoors() {
			// TODO Auto-generated method stub

			openTimer = new Timer();
			openTimer.schedule(new TimerTask() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (logger.isInfoEnabled()) {
						logger.info("Open timeout expired \n"); //$NON-NLS-1$
					}

					queueOfEvents.add(new TimeoutEvent(TimeoutEvent.Reason.OPEN_TIMEOUT, Constants.SYS_ID));

				}

			}, openTimeout);

			for (Entry<String, SubsystemIfc> pair : mySubsystems.entrySet()) {
				pendingOpenRequests.put(pair.getKey(), pair.getValue());
			}
			for (Entry<String, SubsystemIfc> pair : mySubsystems.entrySet()) {
				pair.getValue().portsOpen();
				;
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SystemContextIfc#
		 * gracefulShutdown()
		 */
		@Override
		public void gracefulShutdownSubsystems() {
			// TODO Auto-generated method stub

			if (logger.isInfoEnabled()) {
				logger.info("Graceful shutdown invoked \n"); //$NON-NLS-1$
			}
			queueOfEvents.add(new CloseEvent(Constants.SYS_ID));
			queueOfEvents.add(new ShutdownEvent(ShutdownEvent.Reason.GARCEFUL_SHUTDOWN, Constants.SYS_ID));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.bluethread.types.system.interfaces.SystemContextIfc#shutdown()
		 */
		// @Override
		public void shutdown() {
			// TODO Auto-generated method stub

			myEngine.stopEngine();

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.ObserverCallbackIfc#
		 * stopCompeted(java.lang.String)
		 */
		// public void stopCompleted(String id) {
		// // TODO Auto-generated method stub
		//
		// // TODO Auto-generated method stub
		//
		// if (logger.isDebugEnabled()) {
		// logger.debug("Subsystem completed stop - id=" + id + "\n");
		// //$NON-NLS-1$
		// }
		//
		// pendingShutdownRequests.remove(id);
		// if (pendingStartRequests.size() == 0) {
		// shutdownTimer.cancel();
		//
		// if (logger.isInfoEnabled()) {
		// logger.info("All subsystems completed stop procedure \n");
		// //$NON-NLS-1$
		// }
		//
		// queueOfEvents.add(new StopCompletedEvent(Constants.SYS_ID));
		// }
		//
		// }

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SystemContextIfc#
		 * closeSubsystemsDoors()
		 */
		@Override
		public void closeSubsystemsDoors() {
			// TODO Auto-generated method stub

			// TODO Auto-generated method stub

			closeTimer = new Timer();
			closeTimer.schedule(new TimerTask() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (logger.isInfoEnabled()) {
						logger.info("Close timeout expired \n"); //$NON-NLS-1$
					}

					queueOfEvents.add(new TimeoutEvent(TimeoutEvent.Reason.CLOSE_TIMEOUT, Constants.SYS_ID));

				}

			}, closeTimeout);

			for (Entry<String, SubsystemIfc> pair : mySubsystems.entrySet()) {
				pendingCloseRequests.put(pair.getKey(), pair.getValue());
			}
			for (Entry<String, SubsystemIfc> pair : mySubsystems.entrySet()) {
				pair.getValue().portsClose();
			}

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.bluethread.types.system.interfaces.SystemContextIfc#flushHistory(
		 * )
		 */
		@Override
		public void recallHistory() {
			// TODO Auto-generated method stub
			while (!historyQueue.isEmpty()) {
				Event evt;
				evt = historyQueue.remove();
				if (logger.isDebugEnabled()) {
					logger.debug("Restoring from history event " + evt + "\n"); //$NON-NLS-1$
				}
				queueOfEvents.add(evt);
			}

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SystemContextIfc#
		 * restartSubsystems()
		 */
		@Override
		public void resetSubsystems() {
			// TODO Auto-generated method stub
			if (logger.isInfoEnabled()) {
				logger.info(
						"***********************************************************************************************\n"); //$NON-NLS-1$
				logger.info("RESTARTING SUBSYSTEMS AFTER FAILURE IN " + restartTimeout / 1000 + " seconds\n"); //$NON-NLS-1$
				logger.info(
						"***********************************************************************************************\n"); //$NON-NLS-1$
			}

			// clear all inputs
			historyQueue.clear();
			queueOfEvents.clear();
			// wait a timeout before
			for (Entry<String, SubsystemIfc> pair : mySubsystems.entrySet()) {
				pair.getValue().reset();
			}

			Timer restartTimer = new Timer();
			restartTimer.schedule(new TimerTask() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (logger.isInfoEnabled()) {
						logger.info("Restart timeout expired\n"); //$NON-NLS-1$
					}

					queueOfEvents.add(new TimeoutEvent(TimeoutEvent.Reason.START_TIMEOUT, Constants.SYS_ID));
					queueOfEvents.add(new StartEvent(StartEvent.Reason.RESTART, Constants.SYS_ID));

				}

			}, restartTimeout);

		}

	}

}
