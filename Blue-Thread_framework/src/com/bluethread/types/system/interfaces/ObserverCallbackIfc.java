/**
 * 
 */
package com.bluethread.types.system.interfaces;

/**
 * @author Giuseppe Cecilia
 *
 */
public interface ObserverCallbackIfc {

	public void startCompleted(String id);

	public void portsOpened(String id);

	public void portsClosed(String id);

	public void shutdownCompleted(String id);

	// public void notifyEvent(String SubsystemId, Event evt);

}
