/**
 * 
 */
package com.bluethread.types.system.interfaces;

import com.bluethread.types.events.Event;
import com.bluethread.types.events.StartEvent;

/**
 * @author Giuseppe Cecilia
 *
 */
public interface SystemContextIfc {

	public void startSubsystems(StartEvent.Reason rsn);
	
	public void resetSubsystems();

	public void shutdownSubsystems();

	public void defer(Event evt);
	
	public void remember(Event evt);
	
	public void recallHistory();

	public void openSubsystemsDoors();
	
	public void closeSubsystemsDoors();

	public void gracefulShutdownSubsystems();

}


