/**
 * 
 */
package com.bluethread.types.system.interfaces;

/**
 * @author Giuseppe Cecilia
 *
 */
public interface SubsystemIfc {

	public void portsOpen();

	public void portsClose();

	public void start();

	public void shutdown();

	public void restart();

	public void reset();

	public String getName();

	public void setObserverCallback(ObserverCallbackIfc obs);

}
