/**
 * 
 */
package com.bluethread.types.system.test;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * @author Giuseppe Cecilia
 *
 */
public class TestPriority {

	/**
	 * @author Giuseppe Cecilia
	 *
	 */

	private BlockingQueue theQueue;
	private Producer producer;
	private Consumer consumer;

	private String[] strings = { "pippo", "peppe", "pappo", "puppo" };

	public static void main(String[] args) {
		TestPriority test = new TestPriority();
	}

	public TestPriority() {
		theQueue = new PriorityBlockingQueue<String>();

		// theQueue = new LinkedBlockingDeque<String>();

		producer = new Producer();
		producer.start();
		consumer = new Consumer();
		consumer.start();

	}

	private class Consumer extends Thread {

		public void run() {

			while (true) {

				try {
					System.out.println(theQueue.take());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}

	}

	private class Producer extends Thread {

		public void run() {

			while (true) {

				for (int i = 0; i < 4; i++) {
					System.out.println("Adding " + strings[i]);
					theQueue.add(strings[i]);
				}

				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}

	}

}
