/**
 * 
 */
package com.bluethread.types.system.test.system;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.HashMap;


import org.junit.Assert;
import org.junit.Test;

import com.bluethread.types.system.interfaces.SubsystemIfc;
import com.bluethread.types.system.states.SystemFSM;
import com.bluethread.types.system.test.system.SystemUnderTest.SubsystemOK;

/**
 * @author Giuseppe Cecilia
 *
 */
public class SystemOpenPortsFailTC {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(SystemOpenPortsFailTC.class.getName());

	/**
	 * Logger for this class
	 */
	

	@Test
	public void test() {

		SystemUnderTest sut = new SystemUnderTestPortsOK();
		sut.start();
		sut.open();
		try {
			Thread.sleep(60000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertSame(SystemFSM.ERROR_STATE, sut.getCurrentState());
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Assert.assertSame(SystemFSM.STARTED_STATE, sut.getCurrentState());
	}

	private class SystemUnderTestPortsOK extends SystemUnderTest {
		@Override
		protected void loadSubsystems(HashMap<String, SubsystemIfc> subsystems) {
			// TODO Auto-generated method stub
			subsystems.put("SubsystemFail", new SubsystemPortsFail(this, "SubsystemFail"));
			subsystems.put("SubsystemOK", new SubsystemPortsOK(this, "SubsystemOK"));

		}
	}

	public static void main(String[] args) {

		SystemOpenPortsFailTC tc = new SystemOpenPortsFailTC();
		tc.test();

	}

	class SubsystemPortsOK extends SubsystemOK {

		/**
		 * @param systemUnderTest
		 * @param id
		 */
		public SubsystemPortsOK(SystemUnderTest systemUnderTest, String id) {
			systemUnderTest.super(id);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void portsOpen() {

			if (logger.isInfoEnabled()) {
				logger.info(getName() + ": OPEN RECEIVED \n"); //$NON-NLS-1$
			}

			(new Thread() {
				public void run() {
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					myObs.portsOpened(SubsystemPortsOK.this.getName());
				}
			}).start();

		}

	}

	class SubsystemPortsFail extends SubsystemOK {

		/**
		 * @param systemUnderTest
		 * @param id
		 */
		public SubsystemPortsFail(SystemUnderTest systemUnderTest, String id) {
			systemUnderTest.super(id);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void portsOpen() {

			if (logger.isInfoEnabled()) {
				logger.info(getName() + ": OPEN RECEIVED \n"); //$NON-NLS-1$
			}

			(new Thread() {
				public void run() {
					try {
						Thread.sleep(40000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					myObs.portsOpened(SubsystemPortsFail.this.getName());
				}
			}).start();

		}
	}

}
