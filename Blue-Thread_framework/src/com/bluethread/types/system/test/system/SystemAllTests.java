/**
 * 
 */
package com.bluethread.types.system.test.system;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Giuseppe Cecilia
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ SystemDefaultConfigurationTC.class,  
	SystemConfigurationTC.class, 
	SystemFSMInitialSetupTC.class, 
	SystemStartFailTC.class,
	SystemStartOKTC.class,
	SystemOpenPortsOKTC.class,
	SystemOpenPortsFailTC.class,
	SystemClosePortsOKTC.class,
	SystemClosePortsOK2TC.class,
	SystemClosePortsFailTC.class})
public class SystemAllTests {

}
