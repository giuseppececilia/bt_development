/**
 * 
 */
package com.bluethread.types.system.test.system;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Assert;
import org.junit.Test;

import com.bluethread.types.system.interfaces.SubsystemIfc;
import com.bluethread.types.system.states.SystemFSM;
import com.bluethread.types.system.test.system.SystemUnderTest.SubsystemOK;

/**
 * @author Giuseppe Cecilia
 *
 */
public class SystemClosePortsFailTC {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SystemClosePortsFailTC.class.getName());

	@Test
	public void test() {

		SystemUnderTest sut = new SystemUnderTestPortsOK();
		sut.start();
		sut.open();
		sut.close();

		try {
			Thread.sleep(60000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertSame(SystemFSM.ERROR_STATE, sut.getCurrentState());

		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Assert.assertSame(SystemFSM.STARTED_STATE, sut.getCurrentState());
	}

	private class SystemUnderTestPortsOK extends SystemUnderTest {
		@Override
		protected void loadSubsystems(HashMap<String, SubsystemIfc> subsystems) {
			// TODO Auto-generated method stub
			subsystems.put("SubsystemOK", new SubsystemPortsOK(this, "SubsystemOK"));
			subsystems.put("SubsystemFail", new SubsystemPortsCloseFail(this, "SubsystemFail"));

		}
	}

	public static void main(String[] args) {

		SystemClosePortsFailTC tc = new SystemClosePortsFailTC();
		tc.test();

	}

	class SubsystemPortsCloseFail extends SubsystemOK {

		/**
		 * @param systemUnderTest
		 * @param id
		 */
		public SubsystemPortsCloseFail(SystemUnderTest systemUnderTest, String id) {
			systemUnderTest.super(id);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void portsOpen() {

			if (logger.isLoggable(Level.INFO)) {
				logger.info(getName() + ": OPEN RECEIVED \n"); //$NON-NLS-1$
			}

			(new Thread() {
				public void run() {
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					myObs.portsOpened(SubsystemPortsCloseFail.this.getName());
				}
			}).start();

		}

		@Override
		public void portsClose() {

			if (logger.isLoggable(Level.INFO)) {
				logger.info(getName() + ": CLOSE RECEIVED \n"); //$NON-NLS-1$
			}

			(new Thread() {
				public void run() {
					try {
						Thread.sleep(31000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					myObs.portsClosed(SubsystemPortsCloseFail.this.getName());
				}
			}).start();

		}

	}

	class SubsystemPortsOK extends SubsystemOK {

		/**
		 * @param systemUnderTest
		 * @param id
		 */
		public SubsystemPortsOK(SystemUnderTest systemUnderTest, String id) {
			systemUnderTest.super(id);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void portsOpen() {

			if (logger.isLoggable(Level.INFO)) {
				logger.info(getName() + ": OPEN RECEIVED \n"); //$NON-NLS-1$
			}

			(new Thread() {
				public void run() {
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					myObs.portsOpened(SubsystemPortsOK.this.getName());
				}
			}).start();

		}

		@Override
		public void portsClose() {

			if (logger.isLoggable(Level.INFO)) {
				logger.info(getName() + ": CLOSE RECEIVED \n"); //$NON-NLS-1$
			}

			(new Thread() {
				public void run() {
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					myObs.portsClosed(SubsystemPortsOK.this.getName());
				}
			}).start();

		}
	}

}
