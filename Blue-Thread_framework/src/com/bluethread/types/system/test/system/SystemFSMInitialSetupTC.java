/**
 * 
 */
package com.bluethread.types.system.test.system;

import org.junit.Assert;
import org.junit.Test;

import com.bluethread.types.system.states.SystemFSM;

/**
 * @author Giuseppe Cecilia
 *
 */
public class SystemFSMInitialSetupTC {

	@Test
	public void testInitialState() {
		SystemUnderTest sut = new SystemUnderTest();

		Assert.assertSame(SystemFSM.INITIAL_STATE, sut.getCurrentState());

	}

}
