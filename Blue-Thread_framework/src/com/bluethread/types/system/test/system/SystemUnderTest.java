/**
 * 
 */
package com.bluethread.types.system.test.system;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.HashMap;
import java.util.Properties;

import com.bluethread.types.system.BTSystem;
import com.bluethread.types.system.interfaces.ObserverCallbackIfc;
import com.bluethread.types.system.interfaces.SubsystemIfc;

/**
 * @author Giuseppe Cecilia
 *
 */
public class SystemUnderTest extends BTSystem {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(SystemUnderTest.class.getName());

	/**
	 * Logger for this class
	 */
	

	public SystemUnderTest(Properties props) {
		super(props);
	}

	public SystemUnderTest() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.System#loadSubsystems(java.util.HashMap)
	 */
	@Override
	protected void loadSubsystems(HashMap<String, SubsystemIfc> subsystems) {
		// TODO Auto-generated method stub
		subsystems.put("SubsystemFail", new SubsystemFail("SubsystemFail"));
		subsystems.put("SubsystemOK", new SubsystemOK("SubsystemOK"));

	}

	class SubsystemFail implements SubsystemIfc {

		protected ObserverCallbackIfc myObs;

		private String myId;

		public SubsystemFail(String id) {
			myId = id;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#portsOpen()
		 */
		@Override
		public void portsOpen() {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#portsClose()
		 */
		@Override
		public void portsClose() {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#start()
		 */
		@Override
		public void start() {
			// TODO Auto-generated method stub

			if (logger.isInfoEnabled()) {
				logger.info(myId + ": START RECEIVED \n"); //$NON-NLS-1$
			}

			(new Thread() {
				public void run() {
					try {
						Thread.sleep(15000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					myObs.startCompleted(myId);
				}
			}).start();

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.bluethread.types.system.interfaces.SubsystemIfc#stopService()
		 */
		@Override
		public void shutdown() {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#restart()
		 */
		@Override
		public void restart() {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#getName()
		 */
		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#
		 * setObserverCallback(com.bluethread.types.system.interfaces.
		 * ObserverCallbackIfc)
		 */
		@Override
		public void setObserverCallback(ObserverCallbackIfc obs) {
			// TODO Auto-generated method stub

			myObs = obs;

			if (logger.isInfoEnabled()) {
				logger.info("ObserverCallbackIfc - " + myId + ": CALLBACK SET\n"); //$NON-NLS-1$
			}

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#reset()
		 */
		@Override
		public void reset() {
			// TODO Auto-generated method stub
			if (logger.isInfoEnabled()) {
				logger.info(myId + ": RESET RECEIVED \n"); //$NON-NLS-1$
			}

		}

	}

	class SubsystemOK implements SubsystemIfc {

		protected ObserverCallbackIfc myObs;

		private String myId;

		public SubsystemOK(String id) {
			myId = id;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#portsOpen()
		 */
		@Override
		public void portsOpen() {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#portsClose()
		 */
		@Override
		public void portsClose() {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#start()
		 */
		@Override
		public void start() {
			// TODO Auto-generated method stub

			if (logger.isInfoEnabled()) {
				logger.info(myId + ": START RECEIVED, COMPLETED SENT BACK \n"); //$NON-NLS-1$
			}

			(new Thread() {
				public void run() {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					myObs.startCompleted(myId);
				}
			}).start();

			// myObs.startCompleted(myId);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.bluethread.types.system.interfaces.SubsystemIfc#stopService()
		 */
		@Override
		public void shutdown() {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#restart()
		 */
		@Override
		public void restart() {
			// TODO Auto-generated method stub

			if (logger.isInfoEnabled()) {
				logger.info(myId + ": RESTART RECEIVED, COMPLETED SENT BACK \n"); //$NON-NLS-1$
			}

			myObs.startCompleted(getName());

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#getName()
		 */
		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return myId;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#
		 * setObserverCallback(com.bluethread.types.system.interfaces.
		 * ObserverCallbackIfc)
		 */
		@Override
		public void setObserverCallback(ObserverCallbackIfc obs) {
			// TODO Auto-generated method stub
			myObs = obs;

			if (logger.isInfoEnabled()) {
				logger.info("ObserverCallbackIfc - " + myId + ": CALLBACK SET\n"); //$NON-NLS-1$
			}

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#reset()
		 */
		@Override
		public void reset() {
			// TODO Auto-generated method stub
			if (logger.isInfoEnabled()) {
				logger.info(myId + ": RESET RECEIVED \n"); //$NON-NLS-1$
			}

		}

	}

}