/**
 * 
 */
package com.bluethread.types.system.test.system;

import java.util.Properties;

import org.junit.Assert;
import org.junit.Test;

import com.bluethread.types.constants.Constants;

/**
 * @author Giuseppe Cecilia
 *
 */
public class SystemDefaultConfigurationTC {

	@Test
	public void testConfiguration() {
		SystemUnderTest sut = new SystemUnderTest();
		Properties config = sut.getConfiguration();
	
		Assert.assertTrue(Constants.DEFAULT_EVENT_QUEUE_SIZE == Integer.parseInt((String)config.get(Constants.QUEUE_KEY)));		
		Assert.assertTrue(Constants.DEFAULT_START_TIMEOUT == Integer.parseInt((String)config.get(Constants.START_TIMER_KEY)));
		Assert.assertTrue(Constants.DEFAULT_SHUTDOWN_TIMEOUT == Integer.parseInt((String)config.get(Constants.SHUTDOWN_TIMER_KEY)));
		Assert.assertTrue(Constants.DEFAULT_OPEN_TIMEOUT == Integer.parseInt((String)config.get(Constants.OPEN_TIMER_KEY)));
		Assert.assertTrue(Constants.DEFAULT_CLOSE_TIMEOUT == Integer.parseInt((String)config.get(Constants.CLOSE_TIMER_KEY)));
	}

}
