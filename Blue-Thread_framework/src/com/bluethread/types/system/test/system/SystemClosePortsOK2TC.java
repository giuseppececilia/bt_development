/**
 * 
 */
package com.bluethread.types.system.test.system;

import java.util.logging.Logger;

import java.util.HashMap;
import java.util.logging.Level;

import org.junit.Assert;
import org.junit.Test;

import com.bluethread.types.system.interfaces.SubsystemIfc;
import com.bluethread.types.system.states.SystemFSM;
import com.bluethread.types.system.test.system.SystemUnderTest.SubsystemOK;

/**
 * @author Giuseppe Cecilia
 *
 */
public class SystemClosePortsOK2TC {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SystemClosePortsOK2TC.class.getName());

	@Test
	public void test() {

		SystemUnderTest sut = new SystemUnderTestPortsOK();
		sut.start();
		/*
		 * due to event history, ports are closed at the end
		 */
		sut.close();
		sut.open();
		
		try {
			Thread.sleep(60000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		Assert.assertSame(SystemFSM.STARTED_STATE, sut.getCurrentState());
	}

	private class SystemUnderTestPortsOK extends SystemUnderTest {
		@Override
		protected void loadSubsystems(HashMap<String, SubsystemIfc> subsystems) {
			// TODO Auto-generated method stub
			subsystems.put("SubsystemOK2", new SubsystemPortsOK(this,"SubsystemOK2"));
			subsystems.put("SubsystemOK1", new SubsystemPortsOK(this,"SubsystemOK1"));

		}
	}

	public static void main(String[] args) {

		SystemClosePortsOK2TC tc = new SystemClosePortsOK2TC();
		tc.test();

	}

	class SubsystemPortsOK extends SubsystemOK {

		/**
		 * @param systemUnderTest
		 * @param id
		 */
		public SubsystemPortsOK(SystemUnderTest systemUnderTest, String id) {
			systemUnderTest.super(id);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void portsOpen() {
			
			if (logger.isLoggable(Level.INFO)) {
				logger.info(getName() + ": OPEN RECEIVED \n"); //$NON-NLS-1$
			}

			(new Thread() {
				public void run() {
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					myObs.portsOpened(SubsystemPortsOK.this.getName());
				}
			}).start();

		}
		
		
		@Override
		public void portsClose() {
			
			if (logger.isLoggable(Level.INFO)) {
				logger.info(getName() + ": CLOSE RECEIVED \n"); //$NON-NLS-1$
			}

			(new Thread() {
				public void run() {
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					myObs.portsClosed(SubsystemPortsOK.this.getName());
				}
			}).start();

		}
	}

}
