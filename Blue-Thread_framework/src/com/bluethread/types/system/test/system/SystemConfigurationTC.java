/**
 * 
 */
package com.bluethread.types.system.test.system;

import java.util.Properties;

import org.junit.Assert;
import org.junit.Test;

import com.bluethread.types.constants.Constants;

/**
 * @author Giuseppe Cecilia
 *
 */
public class SystemConfigurationTC {

	@Test
	public void testConfiguration() {

		Properties config = new Properties();

		config.put(Constants.QUEUE_KEY, "50");
		config.put(Constants.START_TIMER_KEY, "5000");
		config.put(Constants.SHUTDOWN_TIMER_KEY, "6000");
		config.put(Constants.OPEN_TIMER_KEY, "7000");
		config.put(Constants.CLOSE_TIMER_KEY, "8000");

		SystemUnderTest sut = new SystemUnderTest(config);
		Properties sysConfig = sut.getConfiguration();

		Assert.assertTrue(50 == Integer.parseInt((String) config.get(Constants.QUEUE_KEY)));
		Assert.assertTrue(5000 == Integer.parseInt(config.getProperty(Constants.START_TIMER_KEY)));
		Assert.assertTrue(6000 == Integer.parseInt(config.getProperty(Constants.SHUTDOWN_TIMER_KEY)));
		Assert.assertTrue(7000 == Integer.parseInt(config.getProperty(Constants.OPEN_TIMER_KEY)));
		Assert.assertTrue(8000 == Integer.parseInt(config.getProperty(Constants.CLOSE_TIMER_KEY)));
	}

}
