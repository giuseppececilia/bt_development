/**
 * 
 */
package com.bluethread.types.system.test.system;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;

import com.bluethread.types.system.interfaces.SubsystemIfc;
import com.bluethread.types.system.states.SystemFSM;

/**
 * @author Giuseppe Cecilia
 *
 */
public class SystemStartOKTC {

	@Test
	public void test() {

		SystemUnderTest sut = new SystemUnderTestStartOK();
		sut.start();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertSame(SystemFSM.STARTED_STATE, sut.getCurrentState());
	}

	private class SystemUnderTestStartOK extends SystemUnderTest {
		@Override
		protected void loadSubsystems(HashMap<String, SubsystemIfc> subsystems) {
			// TODO Auto-generated method stub
			subsystems.put("SubsystemOK2", new SubsystemOK("SubsystemOK2"));
			subsystems.put("SubsystemOK1", new SubsystemOK("SubsystemOK1"));

		}
	}

	public static void main(String[] args) {

		SystemStartOKTC tc = new SystemStartOKTC();
		tc.test();

	}

}
