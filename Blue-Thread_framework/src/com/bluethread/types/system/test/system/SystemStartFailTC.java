/**
 * 
 */
package com.bluethread.types.system.test.system;

import org.junit.Assert;
import org.junit.Test;

import com.bluethread.types.system.states.SystemFSM;

/**
 * @author Giuseppe Cecilia
 *
 */
public class SystemStartFailTC {

	@Test
	public void test() {

		SystemUnderTest sut = new SystemUnderTest();
		sut.start();
		try {
			Thread.sleep(180000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertSame(SystemFSM.ERROR_STATE, sut.getCurrentState());
	}

	public static void main(String[] args) {

		SystemStartFailTC tc = new SystemStartFailTC();
		tc.test();

	}

}
