/**
 * 
 */
package com.bluethread.types.system.test;

import java.util.HashMap;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bluethread.types.events.CommandEvent;
import com.bluethread.types.events.Event;
import com.bluethread.types.events.StopEvent;
import com.bluethread.types.system.BTSystem;
import com.bluethread.types.system.interfaces.ObserverCallbackIfc;
import com.bluethread.types.system.interfaces.SubsystemIfc;

/**
 * @author Giuseppe Cecilia
 *
 */
public class SystemTest extends BTSystem implements SubsystemIfc {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SystemTest.class.getName());

	private EventsProducer evtProd;

	private ObserverCallbackIfc myObserver;

	private class EventsProducer extends Thread {

		private Event[] eventsArray = { new CommandEvent("SystemTest"),
				new StopEvent(StopEvent.Reason.NORMAL, "SystemTest") };

		public void run() {

			Random rndGen = new Random();

			for (int k = 0; k < 1000000; k++) {

				int index = rndGen.nextInt(2);
				queueOfEvents.add(eventsArray[index]);

			}

		}
	}

	/**
	 * @param props
	 */
	public SystemTest(Properties props) {
		super(props);
		// TODO Auto-generated constructor stub

		evtProd = new EventsProducer();
		evtProd.start();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.SubsystemIfc#portsOpen()
	 */
	@Override
	public void portsOpen() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.SubsystemIfc#portsClose()
	 */
	@Override
	public void portsClose() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.SubsystemIfc#start()
	 */
	@Override
	public void start() {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.INFO)) {
			logger.info("Subsystem start received"); //$NON-NLS-1$
		}

		myObserver.startCompleted("SystemTest");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.SubsystemIfc#restart()
	 */
	@Override
	public void restart() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.SubsystemIfc#shutdown()
	 */
	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.System#loadSubsystem(java.util.HashMap)
	 */
	@Override
	protected void loadSubsystems(HashMap<String, SubsystemIfc> subsystems) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINER)) {
			logger.finer("HashMap<String,SubsystemIfc> - Loading subsystems"); //$NON-NLS-1$
		}

		subsystems.put("TestSubsystem", this);
	}

	public static void main(String[] args) {

		if (logger.isLoggable(Level.INFO)) {
			logger.info("String[] - Starting test"); //$NON-NLS-1$
		}

		Properties props = new Properties();
		props.put("system.events.queuesize", "50");
		SystemTest test = new SystemTest(props);
		test.start();
		// try {
		// Thread.sleep(10000);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// test.shutdown();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.SubsystemIfc#setObserverCallback(com.
	 * bluethread.types.system.ObserverCallbackIfc)
	 */
	@Override
	public void setObserverCallback(ObserverCallbackIfc obs) {
		// TODO Auto-generated method stub

		myObserver = obs;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#getName()
	 */
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "SystemTest";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#reset()
	 */
	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}
}
