/**
 * 
 */
package com.bluethread.types.system.test;

import com.bluethread.types.events.Event;
import com.bluethread.types.events.StartEvent;
import com.bluethread.types.events.StartEvent.Reason;
import com.bluethread.types.system.interfaces.SystemContextIfc;
import com.bluethread.types.system.states.SystemFSM;

/**
 * @author Giuseppe Cecilia
 *
 */
public class SystemFSMtest implements SystemContextIfc {

	public static void main(String[] args) {

		SystemFSM fsm = new SystemFSM(new SystemFSMtest());

		fsm.handleEvent(new StartEvent(StartEvent.Reason.NORMAL, "TEST"));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bluethread.types.system.interfaces.SystemContextIfc#stopSubsystems()
	 */
	@Override
	public void shutdownSubsystems() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bluethread.types.system.interfaces.SystemContextIfc#postpone(com.
	 * bluethread.types.events.Event)
	 */
	@Override
	public void defer(Event evt) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SystemContextIfc#
	 * openSubsystemsDoors()
	 */
	@Override
	public void openSubsystemsDoors() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bluethread.types.system.interfaces.SystemContextIfc#gracefulShutdown(
	 * )
	 */
	@Override
	public void gracefulShutdownSubsystems() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SystemContextIfc#
	 * closeSubsystemsDoors()
	 */
	@Override
	public void closeSubsystemsDoors() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bluethread.types.system.interfaces.SystemContextIfc#remember(com.
	 * bluethread.types.events.Event)
	 */
	@Override
	public void remember(Event evt) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bluethread.types.system.interfaces.SystemContextIfc#flushHistory()
	 */
	@Override
	public void recallHistory() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bluethread.types.system.interfaces.SystemContextIfc#restartSubsystems
	 * ()
	 */
	@Override
	public void resetSubsystems() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bluethread.types.system.interfaces.SystemContextIfc#startSubsystems(
	 * com.bluethread.types.events.StartEvent.Reason)
	 */
	@Override
	public void startSubsystems(Reason rsn) {
		// TODO Auto-generated method stub

	}

}
