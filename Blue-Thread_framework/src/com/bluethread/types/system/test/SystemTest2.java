/**
 * 
 */
package com.bluethread.types.system.test;

import java.util.HashMap;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bluethread.types.events.CommandEvent;
import com.bluethread.types.events.Event;
import com.bluethread.types.events.StopEvent;
import com.bluethread.types.system.BTSystem;
import com.bluethread.types.system.interfaces.ObserverCallbackIfc;
import com.bluethread.types.system.interfaces.SubsystemIfc;

/**
 * @author Giuseppe Cecilia
 *
 */
public class SystemTest2 extends BTSystem {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SystemTest2.class.getName());

	private EventsProducer evtProd;

	private SubsystemIfc testSubsystem = new TestSubsystem();

	private class EventsProducer extends Thread {

		private Event[] eventsArray = { new CommandEvent("SystemTest"),
				new StopEvent(StopEvent.Reason.NORMAL, "SystemTest") };

		public void run() {

			Random rndGen = new Random();

			for (int k = 0; k < 1000000; k++) {

				int sleep = rndGen.nextInt(10000);
				queueOfEvents.add(new CommandEvent("SystemTest"));

				try {
					Thread.sleep(sleep);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}
	}

	/**
	 * @param props
	 */
	public SystemTest2(Properties props) {
		super(props);
		// TODO Auto-generated constructor stub

		evtProd = new EventsProducer();
		evtProd.start();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.System#loadSubsystem(java.util.HashMap)
	 */
	@Override
	protected void loadSubsystems(HashMap<String, SubsystemIfc> subsystems) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINER)) {
			logger.finer("SystemTest2 - Loading subsystems\n"); //$NON-NLS-1$
		}

		subsystems.put("TestSubsystem", new TestSubsystem());
	}

	private class TestSubsystem implements SubsystemIfc {

		private ObserverCallbackIfc myObserver;

		public TestSubsystem() {
			// TODO Auto-generated constructor stub

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#portsOpen()
		 */
		@Override
		public void portsOpen() {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#portsClose()
		 */
		@Override
		public void portsClose() {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#start()
		 */
		@Override
		public void start() {
			// TODO Auto-generated method stub
			if (logger.isLoggable(Level.FINER)) {
				logger.finer("Subsystem start received\n"); //$NON-NLS-1$
			}

			// myObserver.startCompeted("TestSubsystem");
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.bluethread.types.system.interfaces.SubsystemIfc#stopService()
		 */
		@Override
		public void shutdown() {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#restart()
		 */
		@Override
		public void restart() {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#
		 * setObserverCallback(com.bluethread.types.system.interfaces.
		 * ObserverCallbackIfc)
		 */
		@Override
		public void setObserverCallback(ObserverCallbackIfc obs) {
			// TODO Auto-generated method stub

			if (logger.isLoggable(Level.FINER)) {
				logger.finer("TestSubsystem - setting the ObserverCallbackIfc\n"); //$NON-NLS-1$
			}
			myObserver = obs;

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#getName()
		 */
		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return "SystemTest2";
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.bluethread.types.system.interfaces.SubsystemIfc#reset()
		 */
		@Override
		public void reset() {
			// TODO Auto-generated method stub

		}

	}

	public static void main(String[] args) {

		if (logger.isLoggable(Level.INFO)) {
			logger.info("String[] - Starting test\n"); //$NON-NLS-1$
		}

		Properties props = new Properties();
		props.put("system.events.queuesize", "50");
		SystemTest2 test = new SystemTest2(props);
		test.start();
		// try {
		// Thread.sleep(10000);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// test.shutdown();

	}

}
