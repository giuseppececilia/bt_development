/**
 * 
 */
package com.bluethread.types.system.states;

import com.bluethread.types.events.Event;
import com.bluethread.types.fsm.FSM;
import com.bluethread.types.fsm.State;
import com.bluethread.types.system.interfaces.SystemContextIfc;

/**
 * @author Giuseppe Cecilia
 *
 */
public class SystemFSM extends FSM {

	/**
	 * @param initial
	 */

	public static State<Event> OPENING_STATE;
	public static State<Event> CLOSING_STATE;
	public static State<Event> STARTING_STATE;
	public static State<Event> STOPPING_STATE;
	public static State<Event> IN_SERVICE_STATE;
	public static State<Event> INITIAL_STATE;
	public static State<Event> STARTED_STATE;
	public static State<Event> ERROR_STATE;


	public SystemFSM(SystemContextIfc context) {
		super(INITIAL_STATE = new InitialState(context));
		CLOSING_STATE = new ClosingState(context);
		OPENING_STATE = new OpeningState(context);
		STOPPING_STATE = new StoppingState(context);
		IN_SERVICE_STATE = new InServiceState(context);
		STARTING_STATE = new StartingState(context);
		STARTED_STATE = new StartedState(context);
		ERROR_STATE = new ErrorState(context);

		// TODO Auto-generated constructor stub
	}

}
