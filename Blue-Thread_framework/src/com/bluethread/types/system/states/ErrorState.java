/**
 * 
 */
package com.bluethread.types.system.states;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;



import com.bluethread.types.events.Event;
import com.bluethread.types.fsm.State;
import com.bluethread.types.system.interfaces.SystemContextIfc;

/**
 * @author Giuseppe Cecilia
 * TODO: handling of RESTART 
 *
 */
public class ErrorState extends State<Event> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(ErrorState.class.getName());

	/**
	 * Logger for this class
	 */
	

	private SystemContextIfc myContext;

	/**
	 * @param name
	 * @param ordinal
	 */
	public ErrorState(SystemContextIfc context) {
		super("ERROR", 6);
		myContext = context;
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#nextState(java.lang.Object)
	 */
	@Override
	public State<Event> nextState(Event evt) {
		// TODO Auto-generated method stub

		if (logger.isDebugEnabled()) {
			logger.debug("Evaluating next state on " + evt + "\n"); //$NON-NLS-1$
		}
		switch (evt.getType()) {

		case TIMEOUT:
			return SystemFSM.INITIAL_STATE;
		default:
			if (logger.isDebugEnabled()) {
				logger.debug("ERROR state - ignoring event " + evt + "\n"); //$NON-NLS-1$
			}
			return this;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#outAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void outAction(Event evt, State<Event> next) {
		// TODO Auto-generated method stub

		// myContext.startSubsystems(StartEvent.Reason.RESTART);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#inAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void inAction(Event evt, State<Event> prev) {
		// TODO Auto-generated method stub

		if (logger.isDebugEnabled()) {
			logger.debug("Requesting SHUTDOWN in ERROR state \n"); //$NON-NLS-1$
		}
		myContext.gracefulShutdownSubsystems();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#action(java.lang.Object)
	 */
	@Override
	public void action(Event evt) {
		// TODO Auto-generated method stub

	}

}
