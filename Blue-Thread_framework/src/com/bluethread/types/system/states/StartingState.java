/**
 * 
 */
package com.bluethread.types.system.states;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import com.bluethread.types.events.Event;
import com.bluethread.types.fsm.State;
import com.bluethread.types.system.interfaces.SystemContextIfc;;

/**
 * @author Giuseppe Cecilia
 *
 */
public class StartingState extends State<Event> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(StartingState.class.getName());

	/**
	 * Logger for this class
	 */
	//private static final Logger logger = Logger.getLogger(StartingState.class.getName());

	private SystemContextIfc myContext;

	/**
	 * @param name
	 * @param ordinal
	 */
	public StartingState(SystemContextIfc context) {
		super("STARTING", 1);
		// TODO Auto-generated constructor stub

		myContext = context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#nextState(java.lang.Object)
	 */
	@Override
	public State<Event> nextState(Event evt) {
		// TODO Auto-generated method stub

		switch (evt.getType()) {

		case TIMEOUT:
			return SystemFSM.ERROR_STATE;
		case START_COMPLETED:
			return SystemFSM.STARTED_STATE;
		default:
			if (logger.isDebugEnabled()) {
				logger.debug("STARTING state - postponing event " + evt + "\n"); //$NON-NLS-1$
			}
			/*
			 * "ING" states are transitions states, events not handled in these states, are postponed for next stable state
			 */
			myContext.defer(evt);
			return this;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#outAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void outAction(Event evt, State<Event> next) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#inAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void inAction(Event evt, State<Event> prev) {
		// TODO Auto-generated method stub

		if (logger.isDebugEnabled()) {
			logger.debug("STARTING state: inAction on event " + evt + " from previous state:" + prev+"\n"); //$NON-NLS-1$ //$NON-NLS-2$
		}

		myContext.recallHistory();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#action(java.lang.Object)
	 */
	@Override
	public void action(Event evt) {
		// TODO Auto-generated method stub

	}

}
