/**
 * 
 */
package com.bluethread.types.system.states;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import com.bluethread.types.events.Event;
import com.bluethread.types.fsm.State;
import com.bluethread.types.system.interfaces.SystemContextIfc;;

/**
 * @author Giuseppe Cecilia
 *
 */
public class ClosingState extends State<Event> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(ClosingState.class.getName());

	/**
	 * Logger for this class
	 */
	//private static final Logger logger = Logger.getLogger(ClosingState.class.getName());

	private SystemContextIfc myContext;

	/**
	 * @param name
	 * @param ordinal
	 */
	public ClosingState(SystemContextIfc context) {
		super("CLOSING", 5);
		// TODO Auto-generated constructor stub

		myContext = context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#nextState(java.lang.Object)
	 */
	@Override
	public State<Event> nextState(Event evt) {
		// TODO Auto-generated method stub
		if (logger.isDebugEnabled()) {
			logger.debug("Evaluating next state on " + evt + "\n"); //$NON-NLS-1$
		}
		switch (evt.getType()) {

		case TIMEOUT:
			return SystemFSM.ERROR_STATE;
		case CLOSE_COMPLETED:
			return SystemFSM.STARTED_STATE;
		case STOP:
			return SystemFSM.INITIAL_STATE;
		default:
			if (logger.isDebugEnabled()) {
				logger.debug("CLOSING state - postponing event " + evt + "\n"); //$NON-NLS-1$
			}
			myContext.defer(evt);
			return this;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#outAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void outAction(Event evt, State<Event> next) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#inAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void inAction(Event evt, State<Event> prev) {
		// TODO Auto-generated method stub
		
		myContext.recallHistory();
		if (logger.isDebugEnabled()) {
			logger.debug("CLOSING state: inAction on event " + evt + " from previous state:" + prev+"\n"); //$NON-NLS-1$ //$NON-NLS-2$
		}

		myContext.closeSubsystemsDoors();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#action(java.lang.Object)
	 */
	@Override
	public void action(Event evt) {
		// TODO Auto-generated method stub

	}

}
