/**
 * 
 */
package com.bluethread.types.system.states;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;



import com.bluethread.types.events.Event;
import com.bluethread.types.events.EventType;
import com.bluethread.types.fsm.State;
import com.bluethread.types.system.interfaces.SystemContextIfc;;

/**
 * @author Giuseppe Cecilia
 *
 */
public class InServiceState extends State<Event> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(InServiceState.class.getName());

	/**
	 * Logger for this class
	 */


	private SystemContextIfc myContext;

	/**
	 * @param name
	 * @param ordinal
	 */
	public InServiceState(SystemContextIfc context) {
		super("INSERVICE", 4);
		myContext = context;
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#nextState(java.lang.Object)
	 */
	@Override
	public State<Event> nextState(Event evt) {
		// TODO Auto-generated method stub

		switch (evt.getType()) {
		case SHUTDOWN:
			return SystemFSM.INITIAL_STATE;

		case CLOSE:
			return SystemFSM.CLOSING_STATE;
		default:
			if (logger.isDebugEnabled()) {
				logger.debug("INSERVICE  state - event stored in history " + evt + "\n"); //$NON-NLS-1$
			}
			myContext.remember(evt);
			// myContext.postpone(evt);
			return this;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#outAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void outAction(Event evt, State<Event> next) {
		// TODO Auto-generated method stub

		if (evt.getType() == EventType.STOP) {

			myContext.gracefulShutdownSubsystems();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#inAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void inAction(Event evt, State<Event> prev) {
		// TODO Auto-generated method stub

		myContext.recallHistory();
		if (logger.isInfoEnabled()) {
			logger.info("SYSTEM IN SERVICE \n"); //$NON-NLS-1$
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#action(java.lang.Object)
	 */
	@Override
	public void action(Event evt) {
		// TODO Auto-generated method stub

	}

}
