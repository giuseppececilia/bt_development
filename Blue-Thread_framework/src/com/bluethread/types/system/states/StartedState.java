/**
 * 
 */
package com.bluethread.types.system.states;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;



import com.bluethread.types.events.Event;
import com.bluethread.types.events.EventType;
import com.bluethread.types.fsm.State;
import com.bluethread.types.system.interfaces.SystemContextIfc;;

/**
 * @author Giuseppe Cecilia
 *
 */
public class StartedState extends State<Event> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(StartedState.class.getName());

	/**
	 * Logger for this class
	 */
	

	private SystemContextIfc myContext;

	/**
	 * @param name
	 * @param ordinal
	 */
	public StartedState(SystemContextIfc context) {
		super("STARTED", 2);
		myContext = context;
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#nextState(java.lang.Object)
	 */
	@Override
	public State<Event> nextState(Event evt) {
		// TODO Auto-generated method stub

		if (logger.isDebugEnabled()) {
			logger.debug("Evaluating next state on " + evt + "\n"); //$NON-NLS-1$
		}

		switch (evt.getType()) {
		case OPEN:

			return SystemFSM.OPENING_STATE;

		case SHUTDOWN:
			return SystemFSM.INITIAL_STATE;

		default:
			if (logger.isDebugEnabled()) {
				logger.debug("STARTED  state - event stored in history " + evt + "\n"); //$NON-NLS-1$
			}
			
			myContext.remember(evt);
			return this;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#outAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void outAction(Event evt, State<Event> next) {
		// TODO Auto-generated method stub

		if(evt.getType() == EventType.SHUTDOWN){
			myContext.shutdownSubsystems();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#inAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void inAction(Event evt, State<Event> prev) {
		// TODO Auto-generated method stub

		if (logger.isDebugEnabled()) {
			logger.debug("STARTED state: inAction on event " + evt + " from previous state:" + prev+"\n"); //$NON-NLS-1$ //$NON-NLS-2$
		}

		myContext.recallHistory();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#action(java.lang.Object)
	 */
	@Override
	public void action(Event evt) {
		// TODO Auto-generated method stub

	}

}
