/**
 * 
 */
package com.bluethread.types.system.states;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.bluethread.types.events.Event;
import com.bluethread.types.fsm.State;
import com.bluethread.types.system.interfaces.SystemContextIfc;;

/**
 * @author Giuseppe Cecilia
 *
 */
public class StoppingState extends State<Event> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StoppingState.class.getName());

	private SystemContextIfc myContext;

	/**
	 * @param name
	 * @param ordinal
	 */
	public StoppingState(SystemContextIfc context) {
		super("STOPPING", 6);
		// TODO Auto-generated constructor stub

		myContext = context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#nextState(java.lang.Object)
	 */
	@Override
	public State<Event> nextState(Event evt) {
		// TODO Auto-generated method stub

		switch (evt.getType()) {

		case TIMEOUT:
		case STOP_COMPLETED:
			return SystemFSM.INITIAL_STATE;
		default:
			if (logger.isLoggable(Level.FINER)) {
				logger.finer("STOPPING state - postponing event " + evt + "\n"); //$NON-NLS-1$
			}
			myContext.defer(evt);
			return this;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#outAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void outAction(Event evt, State<Event> next) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#inAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void inAction(Event evt, State<Event> prev) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINER)) {
			logger.finer("STOPPING state: inAction on event " + evt + " from previous state:" + prev); //$NON-NLS-1$ //$NON-NLS-2$
		}

		myContext.shutdownSubsystems();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#action(java.lang.Object)
	 */
	@Override
	public void action(Event evt) {
		// TODO Auto-generated method stub

	}

}
