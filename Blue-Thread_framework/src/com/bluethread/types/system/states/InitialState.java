/**
 * 
 */
package com.bluethread.types.system.states;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.bluethread.types.events.Event;
import com.bluethread.types.events.StartEvent;
import com.bluethread.types.fsm.State;
import com.bluethread.types.system.interfaces.SystemContextIfc;

/**
 * @author Giuseppe Cecilia
 *
 */
public class InitialState extends State<Event> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(InitialState.class.getName());

	private SystemContextIfc myContext;

	/**
	 * @param name
	 * @param ordinal
	 */
	public InitialState(SystemContextIfc context) {
		super("INITIAL", 0);
		myContext = context;
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#nextState(java.lang.Object)
	 */
	@Override
	public State<Event> nextState(Event evt) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINER)) {
			logger.finer("Evaluating next state on " + evt + "\n"); //$NON-NLS-1$
		}
		switch (evt.getType()) {
		case START:
			return SystemFSM.STARTING_STATE;
		default:
			if (logger.isLoggable(Level.FINER)) {
				// logger.finer("INITIAL state - postponing event " + evt +
				// "\n"); //$NON-NLS-1$
				// logger.finer("INITIAL state - ignoring event " + evt + "\n");
				// //$NON-NLS-1$
				logger.finer("INITIAL state - event stored in history " + evt + "\n"); //$NON-NLS-1$
			}

			/*
			 * tried to postpone events not handled in this state but this can
			 * cause event looping in the same state if FSM do not move the
			 * right state for handling the postponed event
			 */
			myContext.remember(evt);
			// myContext.postpone(evt);
			return this;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#outAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void outAction(Event evt, State<Event> next) {
		// TODO Auto-generated method stub

		if (logger.isLoggable(Level.FINER)) {
			logger.finer("INITIAL state: outAction on event " + evt + " to next state:" + next); //$NON-NLS-1$ //$NON-NLS-2$
		}

		switch (evt.getType()) {
		case START:
			myContext.startSubsystems(((StartEvent) evt).getReason());
			break;
		default:
			break;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#inAction(java.lang.Object,
	 * com.bluethread.types.fsm.State)
	 */
	@Override
	public void inAction(Event evt, State<Event> prev) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.fsm.State#action(java.lang.Object)
	 */
	@Override
	public void action(Event evt) {
		// TODO Auto-generated method stub

	}

}
