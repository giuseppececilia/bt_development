package com.bluethread.rimorchiatori.smartbox.bus;

import com.google.common.eventbus.EventBus;

public class Bus {

	private static EventBus applicationBus = new EventBus();

	public static EventBus getMessagesBus() {
		return applicationBus;
	}

}
