package com.bluethread.rimorchiatori.smartbox.main.defaults;

public class Defaults {
	
	public static int DEFAULT_SMART_BOX_PORT = 12345;
	public static String DEFAULT_SMART_BOX_ADDRESS = "localhost";

}
