package com.bluethread.rimorchiatori.smartbox.main.types;

import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsCommand;

public interface ICommandsHandler {

	public void handleCommand(ItsCommand command);

}
