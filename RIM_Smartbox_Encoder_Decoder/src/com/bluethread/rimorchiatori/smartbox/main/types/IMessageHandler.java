package com.bluethread.rimorchiatori.smartbox.main.types;

public interface IMessageHandler {

	public void handleMessage(String message);

}
