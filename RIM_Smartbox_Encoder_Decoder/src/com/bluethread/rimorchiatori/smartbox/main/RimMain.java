/**
 * 
 */
package com.bluethread.rimorchiatori.smartbox.main;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import com.bluethread.rimorchiatori.smartbox.ais.handling.AisModule;
import com.bluethread.rimorchiatori.smartbox.bus.Bus;
import com.bluethread.rimorchiatori.smartbox.main.types.ICommandsHandler;
import com.bluethread.rimorchiatori.smartbox.northifc.NContainer;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsCommand;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsCommands;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsMMSIList;
import com.bluethread.rimorchiatori.smartbox.southifc.SContainer;
import com.bluethread.types.system.BTSystem;
import com.bluethread.types.system.interfaces.SubsystemIfc;
import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

/**
 * @author Peppe
 *
 */
public class RimMain extends BTSystem {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(RimMain.class.getName());

	private AisModule aisM;
	private NContainer nc;
	private SContainer sc;
	private EventBus bus;

	public RimMain(Properties props) {
		super(props);
		bus = Bus.getMessagesBus();
		bus.register(this);

	}

	@Subscribe
	public void receiveMessage(ItsMMSIList message) {

		if (logger.isDebugEnabled()) {
			logger.debug("handleCommand(ItsCommand) - Handling command  - cmd={}", message.getCommandCode()); //$NON-NLS-1$
		}
		if (message.getOperation() == ItsMMSIList.Operation.ADD)
			sc.addMMSIsToFilter(message.getMMSIList());
		else
			sc.removeMMSIsToFilter((message).getMMSIList());

	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bluethread.types.system.BTSystem#loadSubsystems(java.util.HashMap)
	 */
	@Override
	protected void loadSubsystems(HashMap<String, SubsystemIfc> subsystems) {
		// TODO Auto-generated method stub
		aisM = new AisModule(getConfiguration());
		nc = new NContainer(getConfiguration());
		sc = new SContainer(getConfiguration());
		
		subsystems.put("AisModule", aisM);
		subsystems.put("SContainer", sc);
		subsystems.put("NContainer", nc);
	}
	
	@Subscribe
	public void handleDeadEvents(DeadEvent de){
		if (logger.isDebugEnabled()) {
			logger.debug("handleDeadEvents(DeadEvent) - Caught a DEAD EVENT  - de={}", de); //$NON-NLS-1$
		}
		
	}
	
	

	

	public static void main(String[] args) {

		if (logger.isInfoEnabled()) {
			logger.info("main(String[]) - Starting MAIN"); //$NON-NLS-1$
		}
		
		Properties props = new Properties();
		
		if(args.length>0){
			String configFileName = args[0];
			if(configFileName!=null){
				File configFile = new File(configFileName);
				if(configFile.exists()){
					try {
						InputStream inputProperties = new FileInputStream(configFile);
						props.load(inputProperties);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					
				}
			}
		}else{
			
			InputStream inputProperties = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("com/bluethread/rimorchiatori/smartbox/main/config.properties");
			try {
				props.load(inputProperties);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
			
		

		

		RimMain rm = new RimMain(props);
		rm.start();
		rm.open();

	}

}
