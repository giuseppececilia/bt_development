package com.bluethread.rimorchiatori.smartbox.northifc.parser.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Test;

import com.bluethread.ais.messages.BtAisAsm.CrewRole;
import com.bluethread.ais.messages.BtAisAsm.Turn;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.BailOutErrorStrategy;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.BailOutLexer;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.SmartBoxCommandsListener;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.antlrgenerated.SmartBoxCommandsGrammarLexer;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.antlrgenerated.SmartBoxCommandsGrammarParser;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsCommand;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsCommands;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsCrewUpdate;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsLoginReceive;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsMMSIList;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsMissionSend;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsMissionStateAck;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsTurnUpdate;

import org.junit.Assert;

public class JU_TestCase {

	@Test
	public void testLoginOK() {

		ANTLRInputStream input;

		input = new ANTLRInputStream("LOGIN_RECEIVE|12345|OK");
		SmartBoxCommandsGrammarLexer lexer = new BailOutLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);
		parser.setErrorHandler(new BailOutErrorStrategy());
		// parser.root();

		ParseTree tree = parser.root();
		ParseTreeWalker walker = new ParseTreeWalker();
		SmartBoxCommandsListener sbParser = new SmartBoxCommandsListener();

		walker.walk(sbParser, tree);
		ItsCommand command = sbParser.getCommand();

		Assert.assertEquals(ItsCommands.LOGIN_RECEIVE, command.getCommandCode());

		Assert.assertEquals(true, ((ItsLoginReceive) command).getAnswer());
		Assert.assertEquals(12345, command.getDestMMSI());
		System.out.println(command);

	}

	@Test
	public void testLoginNOK() {

		ANTLRInputStream input;

		input = new ANTLRInputStream("LOGIN_RECEIVE|12345|NOK");
		SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);
		// parser.root();
		ParseTree tree = parser.root();

		ParseTreeWalker walker = new ParseTreeWalker();
		SmartBoxCommandsListener sbParser = new SmartBoxCommandsListener();

		walker.walk(sbParser, tree);

		ItsCommand command = sbParser.getCommand();

		Assert.assertEquals(ItsCommands.LOGIN_RECEIVE, command.getCommandCode());

		Assert.assertEquals(false, ((ItsLoginReceive) command).getAnswer());
		Assert.assertEquals(12345, command.getDestMMSI());
		System.out.println(command);

	}

	@Test
	public void testMission() {

		ANTLRInputStream input;

		input = new ANTLRInputStream(
				"MISSION_SEND|123|2017/11/21|1:05|BELLAVITA|PEPPINO NAVIGANTE|ENTRATA|BANCHINA|1234567");
		SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);

		ParseTree tree = parser.root();

		ParseTreeWalker walker = new ParseTreeWalker();
		SmartBoxCommandsListener sbParser = new SmartBoxCommandsListener();

		walker.walk(sbParser, tree);

		// System.out.println(tree.toStringTree(parser));

		ItsMissionSend command = (ItsMissionSend) sbParser.getCommand();

		Assert.assertEquals(ItsCommands.MISSION_SEND, command.getCommandCode());
		Assert.assertEquals("PEPPINO NAVIGANTE", command.getPilotName());
		Assert.assertEquals(1234567, command.getDestMMSI());

		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/MM/dd - hh:mm");
		try {
			Assert.assertEquals(dtf.parse("2017/11/21 - 1:05"), command.getDate_and_time());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(command);

	}

	@Test
	public void testCrew() {

		ANTLRInputStream input;
		input = new ANTLRInputStream(
				"CREW_UPDATE|12345|15|pippo|PEPPE|123453|COMANDANTE|mario|esposito|12345|NOSTROMO");

		SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);
		// parser.root();
		ParseTree tree = parser.root();

		ParseTreeWalker walker = new ParseTreeWalker();
		SmartBoxCommandsListener sbParser = new SmartBoxCommandsListener();

		walker.walk(sbParser, tree);

		ItsCommand command = sbParser.getCommand();

		Assert.assertEquals(ItsCommands.CREW_UPDATE, command.getCommandCode());
		Assert.assertEquals(12345, command.getDestMMSI());

		ItsCrewUpdate.CrewMember[] members = ((ItsCrewUpdate) command).getCrewMembers();

		Assert.assertEquals(2, members.length);

		Assert.assertEquals("pippo", members[0].getName());
		Assert.assertEquals(CrewRole.COMANDANTE, members[0].getRole());
		Assert.assertEquals("mario", members[1].getName());
		Assert.assertEquals(12345, members[1].getID());
		System.out.println(command);
	}

	@Test
	public void testTurn() {

		ANTLRInputStream input;
		input = new ANTLRInputStream("TURN_UPDATE|12345|15|MATTINA|NOTTURNO|NOTTURNO E FESTIVO");

		SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);
		// parser.root();
		ParseTree tree = parser.root();

		ParseTreeWalker walker = new ParseTreeWalker();
		SmartBoxCommandsListener sbParser = new SmartBoxCommandsListener();

		walker.walk(sbParser, tree);

		ItsCommand command = sbParser.getCommand();

		Assert.assertEquals(ItsCommands.TURN_UPDATE, command.getCommandCode());

		Assert.assertEquals(12345, command.getDestMMSI());

		Turn[] turns = ((ItsTurnUpdate) command).getTurns();

		Assert.assertEquals(3, turns.length);

		Assert.assertEquals(Turn.MATTINA, turns[0]);
		Assert.assertEquals(Turn.NOTTURNO, turns[1]);
		Assert.assertEquals(Turn.NOTTURNO_FESTIVO, turns[2]);
		System.out.println(command);
	}

	@Test
	public void testMissionStateAck() {

		ANTLRInputStream input;
		input = new ANTLRInputStream("MISSION_STATE_ACK|12345|123");

		SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);
		// parser.root();
		ParseTree tree = parser.root();

		ParseTreeWalker walker = new ParseTreeWalker();
		SmartBoxCommandsListener sbParser = new SmartBoxCommandsListener();

		walker.walk(sbParser, tree);

		ItsCommand command = sbParser.getCommand();

		Assert.assertEquals(ItsCommands.MISSION_STATE_ACK, command.getCommandCode());
		Assert.assertEquals(12345, command.getDestMMSI());
		Assert.assertEquals(123, ((ItsMissionStateAck) command).getMissionId());
		System.out.println(command);
	}

	@Test
	public void testMMSIList() {

		ANTLRInputStream input;

		input = new ANTLRInputStream("MMSI_ADD|1234567|3425345|345263|54627463");
		SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);

		ParseTree tree = parser.root();

		ParseTreeWalker walker = new ParseTreeWalker();
		SmartBoxCommandsListener sbParser = new SmartBoxCommandsListener();

		walker.walk(sbParser, tree);

		// System.out.println(tree.toStringTree(parser));

		ItsMMSIList command = (ItsMMSIList) sbParser.getCommand();

		Assert.assertEquals(ItsCommands.MMSI_LIST, command.getCommandCode());

		int[] mmsilist = ((ItsMMSIList) command).getMMSIList();
		Assert.assertEquals(1234567, mmsilist[0]);
		Assert.assertEquals(3425345, mmsilist[1]);
		Assert.assertEquals(345263, mmsilist[2]);
		Assert.assertEquals(54627463, mmsilist[3]);
		System.out.println(command);
	}

}
