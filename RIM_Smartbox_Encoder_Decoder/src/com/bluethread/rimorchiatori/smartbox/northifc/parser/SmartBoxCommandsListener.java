package com.bluethread.rimorchiatori.smartbox.northifc.parser;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.antlr.v4.runtime.Token;

import org.antlr.v4.runtime.tree.TerminalNode;

import com.bluethread.ais.messages.BtAisAsm.CrewRole;
import com.bluethread.ais.messages.BtAisAsm.Turn;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.antlrgenerated.SmartBoxCommandsGrammarBaseListener;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.antlrgenerated.SmartBoxCommandsGrammarParser;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsCommand;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsCrewUpdate;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsLoginReceive;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsMMSIList;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsMissionSend;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsMissionStateAck;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsTurnUpdate;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsUndefined;

public class SmartBoxCommandsListener extends SmartBoxCommandsGrammarBaseListener {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(SmartBoxCommandsListener.class.getName());

	// private SmartBoxCommandsGrammarParser parser;

	private ItsCommand command = new ItsUndefined();

	private String pilotName;

	@Override
	public void enterMissionsentence(SmartBoxCommandsGrammarParser.MissionsentenceContext ctx) {
		command = new ItsMissionSend();

	}

	@Override
	public void exitPilotname(SmartBoxCommandsGrammarParser.PilotnameContext ctx) {

		if (ctx.getChildCount() > 1) {
			pilotName = ctx.getChild(0).getText() + " " + ctx.getChild(1).getText();
		} else
			pilotName = ctx.getChild(0).getText();
	}

	@Override
	public void exitMissionparams(SmartBoxCommandsGrammarParser.MissionparamsContext ctx) {

		int missionId = Integer.parseInt(ctx.getChild(1).getText());

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd - hh:mm");

		String date = ctx.getChild(3).getText();

		String time = ctx.getChild(5).getText();

		Date dt = new Date(System.currentTimeMillis());

		try {
			dt = sdf.parse(date + " - " + time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block

		}

		String shipname = ctx.getChild(7).getText();

		String type = ctx.getChild(11).getText();

		String destination = ctx.getChild(13).getText();

		int mmsi = Integer.parseInt(ctx.getChild(15).getText());

		((ItsMissionSend) command).setDestMMSI(mmsi);
		((ItsMissionSend) command).setDate_and_time(dt);
		((ItsMissionSend) command).setShipname(shipname);
		((ItsMissionSend) command).setPilotName(pilotName);
		((ItsMissionSend) command).setType(type);
		((ItsMissionSend) command).setDestination(destination);
		((ItsMissionSend) command).setMissionID(missionId);

	}

	@Override
	public void enterTurnsentence(SmartBoxCommandsGrammarParser.TurnsentenceContext ctx) {

		command = new ItsTurnUpdate();
		command.setDestMMSI(Integer.parseInt(ctx.getChild(2).getText()));
	}

	@Override
	public void exitTurn(SmartBoxCommandsGrammarParser.TurnContext ctx) {

		TerminalNode tn = (TerminalNode) ctx.getChild(1).getChild(0);
		Token tk = tn.getSymbol();
		switch (tk.getType()) {

		case SmartBoxCommandsGrammarParser.MATTINA:
			((ItsTurnUpdate) command).addTurn(Turn.MATTINA);
			break;
		case SmartBoxCommandsGrammarParser.POMERIGGIO:
			((ItsTurnUpdate) command).addTurn(Turn.POMERIGGIO);
			break;
		case SmartBoxCommandsGrammarParser.NOTTURNO:
			((ItsTurnUpdate) command).addTurn(Turn.NOTTURNO);
			break;
		case SmartBoxCommandsGrammarParser.NOTTURNO_E_FESTIVO:
			((ItsTurnUpdate) command).addTurn(Turn.NOTTURNO_FESTIVO);
			break;

		default:
			((ItsTurnUpdate) command).addTurn(Turn.UNDEFINED);

		}

		if (logger.isInfoEnabled()) {
			logger.info("exitTurn: added turns ", command.toString()); //$NON-NLS-1$
		}

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */

	@Override
	public void exitOk(SmartBoxCommandsGrammarParser.OkContext ctx) {

		if (logger.isDebugEnabled()) {
			logger.debug("exitOk(SmartBoxCommandsGrammarParser.OkContext): created ItsLoginReceive(OK) command"); //$NON-NLS-1$
		}

		command = new ItsLoginReceive(true);

	}

	@Override
	public void exitLoginsentence(SmartBoxCommandsGrammarParser.LoginsentenceContext ctx) {

		command.setDestMMSI(Integer.parseInt(ctx.getChild(2).getText()));

	}

	@Override
	public void exitNok(SmartBoxCommandsGrammarParser.NokContext ctx) {

		if (logger.isDebugEnabled()) {
			logger.debug("exitNok(SmartBoxCommandsGrammarParser.NokContext): created ItsLoginReceive(NOK) command"); //$NON-NLS-1$
		}

		command = new ItsLoginReceive(false);
	}

	@Override
	public void enterCrewsentence(SmartBoxCommandsGrammarParser.CrewsentenceContext ctx) {

		command = new ItsCrewUpdate();
		command.setDestMMSI(Integer.parseInt(ctx.getChild(2).getText()));

		if (logger.isInfoEnabled()) {
			logger.info(
					"enterCrewsentence(SmartBoxCommandsGrammarParser.CrewsentenceContext) - Created ItsCrewUpdate command"); //$NON-NLS-1$
		}

	}

	@Override
	public void exitMember(SmartBoxCommandsGrammarParser.MemberContext ctx) {

		String name = ctx.getChild(1).getText();
		String srName = ctx.getChild(3).getText();
		int id = Integer.parseInt(ctx.getChild(5).getText());

		TerminalNode tn = (TerminalNode) ctx.getChild(7).getChild(0);
		Token tk = tn.getSymbol();
		switch (tk.getType()) {

		case SmartBoxCommandsGrammarParser.COMANDANTE:
			((ItsCrewUpdate) command)
					.addCrewMember(new ItsCrewUpdate.CrewMember(srName, name, id, CrewRole.COMANDANTE));
			break;
		case SmartBoxCommandsGrammarParser.MARINAIO:
			((ItsCrewUpdate) command).addCrewMember(new ItsCrewUpdate.CrewMember(srName, name, id, CrewRole.MARINAIO));
			break;
		case SmartBoxCommandsGrammarParser.NOSTROMO:
			((ItsCrewUpdate) command).addCrewMember(new ItsCrewUpdate.CrewMember(srName, name, id, CrewRole.NOSTROMO));
			break;
		case SmartBoxCommandsGrammarParser.CAPO_MACCHINE:
			((ItsCrewUpdate) command)
					.addCrewMember(new ItsCrewUpdate.CrewMember(srName, name, id, CrewRole.CAPO_MACCHINE));
			break;

		default:
			((ItsCrewUpdate) command).addCrewMember(new ItsCrewUpdate.CrewMember(srName, name, id, CrewRole.UNDEFINED));

		}

		if (logger.isInfoEnabled()) {
			logger.info("exitMember: added members ", command.toString()); //$NON-NLS-1$
		}

	}

	@Override
	public void exitMissionstateacksentence(SmartBoxCommandsGrammarParser.MissionstateacksentenceContext ctx) {

		int mmsi = Integer.parseInt(ctx.getChild(2).getText());
		int missionId = Integer.parseInt(ctx.getChild(4).getText());

		command = new ItsMissionStateAck(missionId);
		command.setDestMMSI(mmsi);

	}

	@Override
	public void enterMmsiaddsentence(SmartBoxCommandsGrammarParser.MmsiaddsentenceContext ctx) {
		command = new ItsMMSIList(ItsMMSIList.Operation.ADD);
	}

	@Override
	public void enterMmsiremovesentence(SmartBoxCommandsGrammarParser.MmsiremovesentenceContext ctx) {
		command = new ItsMMSIList(ItsMMSIList.Operation.REMOVE);
	}

	@Override
	public void exitKnownmmsi(SmartBoxCommandsGrammarParser.KnownmmsiContext ctx) {

		((ItsMMSIList) command).add(Integer.parseInt(ctx.getChild(0).getText()));
	}

	public ItsCommand getCommand() {
		return command;
	}

}
