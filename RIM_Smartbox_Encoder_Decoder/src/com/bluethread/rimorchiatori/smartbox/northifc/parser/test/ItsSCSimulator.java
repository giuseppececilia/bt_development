/**
 * 
 */
package com.bluethread.rimorchiatori.smartbox.northifc.parser.test;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * @author Peppe
 *
 */



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ItsSCSimulator {
    /**
     * Logger for this class
     */
    private static final Logger logger = LogManager.getLogger(ItsSCSimulator.class.getName());
    
    public static void main(String[] args) throws Exception {
    ServerSocket m_ServerSocket = new ServerSocket(12111);
    int id = 0;

	if (logger.isInfoEnabled()) {
	    logger.info("main(String[]) - Started Server on port 12111"); //$NON-NLS-1$
	}

    while (true) {
      Socket clientSocket = m_ServerSocket.accept();

	    if (logger.isInfoEnabled()) {
		logger.info("main(String[]) - Connection received from address "+clientSocket.getRemoteSocketAddress()); //$NON-NLS-1$
	    }

      ClientServiceThread cliThread = new ClientServiceThread(clientSocket, id++);
      cliThread.start();
    }
  }
}


class KeyboardReader extends Thread{
	private PrintWriter out;
    private Scanner keyIn;
    
    private boolean running = true;
	
    
    public KeyboardReader(PrintWriter out){
		this.out = out;
		keyIn = new Scanner(System.in);
		
		
	}
	
	public void run(){
		System.out.println("KEYBOARD READER THREAD STARTED");
		while(running){
			System.out.print("Insert command or QUIT:");
			String line = keyIn.nextLine();
			System.out.println("");
			if (line.equals("QUIT!")) running = false;
			else{
				out.println(line);
				out.flush();
			}
			
		}
		
		
		
	}
}


class ClientServiceThread extends Thread {
    /**
     * Logger for this class
     */
    private static final Logger logger = LogManager.getLogger(ClientServiceThread.class.getName());

  Socket clientSocket;
  int clientID = -1;
  boolean running = true;

  ClientServiceThread(Socket s, int i) {
    clientSocket = s;
    clientID = i;
  }

  public void run() {
    System.out.println("Accepted Client : ID - " + clientID + " : Address - "
        + clientSocket.getInetAddress().getHostName());
    try {
      BufferedReader   in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
      PrintWriter   out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
      new KeyboardReader(out).start();
      while (running) {
        String clientCommand = in.readLine();
        System.out.println("Client Says :" + clientCommand);
//        if (clientCommand.equalsIgnoreCase("quit")) {
//          running = false;
//          System.out.print("Stopping client thread for client : " + clientID);
//        } else {
//          out.println(clientCommand);
//          out.flush();
//        }
      }
    } catch (Exception e) {

    	
    	System.out.println("Cannot read from socket");
    }
    finally{
    	try {
			clientSocket.close();
			System.out.println("Client scoket disconnected");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
    }
  }
}
