package com.bluethread.rimorchiatori.smartbox.northifc.parser;

import org.antlr.v4.runtime.DefaultErrorStrategy;
import org.antlr.v4.runtime.InputMismatchException;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;

public class BailOutErrorStrategy extends DefaultErrorStrategy{

	@Override
	public void reportError(Parser recognizer, RecognitionException e){
		// TODO Auto-generated method stub
		throw new RuntimeException(e);
	}

	@Override
	public void recover(Parser recognizer, RecognitionException e) {
		// TODO Auto-generated method stub
		new RuntimeException(e);
	}

	@Override
	public void sync(Parser arg0) throws RecognitionException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Token recoverInline(Parser recognizer) throws RecognitionException {
		// TODO Auto-generated method stub
		throw new RuntimeException(new InputMismatchException(recognizer));
	}
	
	
	

}
