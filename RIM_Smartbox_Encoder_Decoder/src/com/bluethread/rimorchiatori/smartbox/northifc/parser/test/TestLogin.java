package com.bluethread.rimorchiatori.smartbox.northifc.parser.test;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import com.bluethread.rimorchiatori.smartbox.northifc.parser.BailOutErrorStrategy;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.BailOutLexer;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.SmartBoxCommandsListener;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.antlrgenerated.SmartBoxCommandsGrammarLexer;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.antlrgenerated.SmartBoxCommandsGrammarParser;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsCommand;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsUndefined;

public class TestLogin {
	
	public static void main (String [] args){
		
		ANTLRInputStream input;
		ItsCommand command = new ItsUndefined();
		input = new ANTLRInputStream("LOGIN_RECEIVE|12345|OK");
		//input = new ANTLRInputStream("LOGIN_RECEIVE|OK");
		SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);
		parser.setErrorHandler(new BailOutErrorStrategy());
		// parser.root();
		
		try{
			ParseTree tree = parser.root();
			ParseTreeWalker walker = new ParseTreeWalker();
			SmartBoxCommandsListener sbParser = new SmartBoxCommandsListener();

			walker.walk(sbParser, tree);
			
			command = sbParser.getCommand();
			
		}catch(Exception ex){
			
			System.out.println("Got EXCEPTION");
			
		}
		

	System.out.println("out of try catch, command is "+command.getCommandCode());
	
		
		
		
	}

}
