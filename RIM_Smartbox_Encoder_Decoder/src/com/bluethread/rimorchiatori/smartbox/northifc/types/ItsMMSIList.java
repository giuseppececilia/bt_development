package com.bluethread.rimorchiatori.smartbox.northifc.types;

import java.util.ArrayList;

public class ItsMMSIList extends ItsCommand {

	private ArrayList<Integer> mmsiList;
	private Operation mOperation;

	public ItsMMSIList(Operation op) {
		super(ItsCommands.MMSI_LIST);
		// TODO Auto-generated constructor stub
		mOperation = op;
		mmsiList = new ArrayList<Integer>();
	}

	public void add(int mmsi) {
		mmsiList.add(new Integer(mmsi));
	}

	public int[] getMMSIList() {
		Integer[] Ia = mmsiList.toArray(new Integer[mmsiList.size()]);
		int[] ia = new int[Ia.length];

		for (int i = 0; i < ia.length; i++) {
			ia[i] = Ia[i].intValue();

		}

		return ia;
	}

	public Operation getOperation() {
		return mOperation;
	}

	public static enum Operation {
		ADD, REMOVE
	};

	public String toString() {
		switch (mOperation) {
		case ADD:
			return "ItsMMSIList ADD command to MMSI:" + getDestMMSI();

		case REMOVE:
			return "ItsMMSIList  REMOVE command to MMSI:" + getDestMMSI();
		default:
			return "";
		}

	}
}
