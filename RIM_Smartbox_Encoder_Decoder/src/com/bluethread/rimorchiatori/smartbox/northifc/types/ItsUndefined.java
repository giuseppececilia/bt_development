package com.bluethread.rimorchiatori.smartbox.northifc.types;

public class ItsUndefined extends ItsCommand {

	public ItsUndefined() {
		super(ItsCommands.UNDEFINED);

	}

	public String toString() {
		return "ItsUndefined command to MMSI:" + getDestMMSI();
	}

}
