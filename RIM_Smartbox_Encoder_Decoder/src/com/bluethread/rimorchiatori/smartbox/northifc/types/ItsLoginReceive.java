package com.bluethread.rimorchiatori.smartbox.northifc.types;

public class ItsLoginReceive extends ItsCommand {

	private boolean answer = false;

	public ItsLoginReceive(boolean val) {
		super(ItsCommands.LOGIN_RECEIVE);
		answer = val;
	}

	public boolean getAnswer() {
		return answer;
	}

	public String toString() {
		return "ItsLoginReceive command to MMSI:" + getDestMMSI();
	}
}
