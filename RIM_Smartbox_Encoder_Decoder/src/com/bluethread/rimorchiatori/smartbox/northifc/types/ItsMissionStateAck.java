package com.bluethread.rimorchiatori.smartbox.northifc.types;

public class ItsMissionStateAck extends ItsCommand {

	private int missionId;

	public ItsMissionStateAck(int mId) {
		super(ItsCommands.MISSION_STATE_ACK);
		missionId = mId;
	}

	public int getMissionId() {
		return missionId;
	}

	public String toString() {
		return "ItsMissionStateAck command to MMSI:" + getDestMMSI();
	}

}
