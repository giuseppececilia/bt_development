package com.bluethread.rimorchiatori.smartbox.northifc.types;

import java.util.ArrayList;

import com.bluethread.ais.messages.BtAisAsm.Turn;

public class ItsTurnUpdate extends ItsCommand {

	private ArrayList<Turn> turns = new ArrayList<Turn>();;

	public ItsTurnUpdate() {
		super(ItsCommands.TURN_UPDATE);
	}

	public void addTurn(Turn t) {
		turns.add(t);
	}

	public Turn[] getTurns() {

		Turn[] a = turns.toArray(new Turn[turns.size()]);
		return a;
	}

	public String toString() {
		return "ItsTurnUpdate command to MMSI:" + getDestMMSI();
	}

}
