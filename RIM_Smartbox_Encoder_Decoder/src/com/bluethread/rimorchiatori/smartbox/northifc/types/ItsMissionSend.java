package com.bluethread.rimorchiatori.smartbox.northifc.types;

import java.util.Date;

public class ItsMissionSend extends ItsCommand {

	private int missionID;
	private Date date_and_time;
	private String pilotName;
	private String type;
	String destination;
	String shipname;

	public ItsMissionSend() {
		super(ItsCommands.MISSION_SEND);
	}

	public int getMissionID() {
		return missionID;
	}

	public void setMissionID(int missionID) {
		this.missionID = missionID;
	}

	public Date getDate_and_time() {
		return date_and_time;
	}

	public void setDate_and_time(Date date_and_time) {
		this.date_and_time = date_and_time;
	}

	public String getPilotName() {
		return pilotName;
	}

	public void setPilotName(String pilotName) {
		this.pilotName = pilotName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getShipname() {
		return shipname;
	}

	public void setShipname(String shipname) {
		this.shipname = shipname;
	}

	public String toString() {
		return "ItsMissionSend command to MMSI:" + getDestMMSI();
	}

}
