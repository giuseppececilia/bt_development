package com.bluethread.rimorchiatori.smartbox.northifc.types;

public class ItsCommand {
	
	private int command;
	
	private int destMMSI;
	
	public ItsCommand (int commandCode){
		
		command = commandCode;
		
	}
	
	public int getCommandCode(){
		return command;
	}
	
	public void setDestMMSI(int mmsi){
		destMMSI = mmsi;
	}
	
	public int getDestMMSI(){
		return destMMSI;
	}
	
	

}
