package com.bluethread.rimorchiatori.smartbox.northifc.types;

public class ItsCommands {

	public static final int MISSION_SEND = 0;
	public static final int LOGIN_RECEIVE = 1;
	public static final int CREW_UPDATE = 2;
	public static final int TURN_UPDATE = 3;
	public static final int MISSION_STATE_ACK = 4;
	public static final int MMSI_LIST = 5;
	public static final int UNDEFINED = -1;

}
