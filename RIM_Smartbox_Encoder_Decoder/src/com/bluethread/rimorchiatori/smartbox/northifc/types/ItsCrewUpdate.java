package com.bluethread.rimorchiatori.smartbox.northifc.types;

import java.util.ArrayList;
import java.util.List;

import com.bluethread.ais.messages.BtAisAsm.CrewRole;

public class ItsCrewUpdate extends ItsCommand {

	public static class CrewMember {

		private String name;
		private String surname;
		private int ID;
		private CrewRole role;

		public CrewMember(String surname, String name, int ID, CrewRole role) {
			this.name = name;
			this.surname = surname;
			this.ID = ID;
			this.role = role;
		}

		public String getName() {
			return name;
		}

		public String getSurname() {
			return surname;
		}

		public int getID() {
			return ID;
		}

		public CrewRole getRole() {
			return role;
		}

	}

	private ArrayList<CrewMember> crewMembers;

	public ItsCrewUpdate() {
		super(ItsCommands.CREW_UPDATE);

		crewMembers = new ArrayList<CrewMember>();
	}

	public void addCrewMember(CrewMember anotherMember) {
		crewMembers.add(anotherMember);

	}

	public CrewMember[] getCrewMembers() {

		CrewMember[] a = crewMembers.toArray(new CrewMember[crewMembers.size()]);

		return a;
	}

	public String toString() {
		return "ItsCrewUpdate command to MMSI:" + getDestMMSI();
	}

}
