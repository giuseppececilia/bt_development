/**
 * 
 */
package com.bluethread.rimorchiatori.smartbox.northifc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingDeque;
import static java.util.Objects.requireNonNull;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bluethread.rimorchiatori.smartbox.bus.Bus;
import com.bluethread.rimorchiatori.smartbox.main.RimMain;
import com.bluethread.rimorchiatori.smartbox.main.defaults.Defaults;
import com.bluethread.rimorchiatori.smartbox.main.types.ICommandsHandler;
import com.bluethread.rimorchiatori.smartbox.main.types.IMessageHandler;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.BailOutErrorStrategy;

import com.bluethread.rimorchiatori.smartbox.northifc.parser.SmartBoxCommandsListener;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.antlrgenerated.SmartBoxCommandsGrammarLexer;
import com.bluethread.rimorchiatori.smartbox.northifc.parser.antlrgenerated.SmartBoxCommandsGrammarParser;
import com.bluethread.rimorchiatori.smartbox.northifc.types.CommandFilter;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsCommand;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsUndefined;
import com.bluethread.types.constants.Constants;
import com.bluethread.types.system.BTSystem;
import com.bluethread.types.system.interfaces.ObserverCallbackIfc;
import com.bluethread.types.system.interfaces.SubsystemIfc;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

/**
 * @author Peppe
 *
 */
public class NContainer implements SubsystemIfc{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(NContainer.class.getName());

	private static String myId = "NContainer";
	private ObserverCallbackIfc myObserver;
	private Properties myProps;
	private Socket northPort;

	private PrintWriter toSmartBoxStream;
	private BufferedReader fromSmartBoxStream;
	private CommandsParser socketReader;
	/**
	 * filter used to route MMSIList commands to RimMain for configuring the
	 * System
	 */
	private CommandFilter filter;

	private LinkedBlockingDeque<String> toSmartBoxQueue;

	private ToSmartBoxThread tsb;

	private EventBus bus;

	public NContainer(Properties props) {
		myProps = props;
		toSmartBoxQueue = new LinkedBlockingDeque<String>();
		filter = new CommandFilter();

		bus = Bus.getMessagesBus();
		bus.register(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#portsOpen()
	 */
	@Override
	public void portsOpen() {
		// TODO Auto-generated method stub
		String address;
		int port;
		if (myProps.containsKey("smartbox.address")) {
			address = myProps.getProperty("smartbox.address");
		} else {
			address = Defaults.DEFAULT_SMART_BOX_ADDRESS;
		}

		if (myProps.containsKey("smartbox.port")) {
			port = Integer.parseInt(myProps.getProperty("smartbox.port"));
		} else {
			port = Defaults.DEFAULT_SMART_BOX_PORT;
		}

		try {

			if (logger.isInfoEnabled()) {
				logger.info("portsOpen() - Opening socket to enpoint  - address=" + address + ", port=" + port); //$NON-NLS-1$
			}

			northPort = new Socket(address, port);
			toSmartBoxStream = new PrintWriter(new OutputStreamWriter(northPort.getOutputStream()));
			fromSmartBoxStream = new BufferedReader(new InputStreamReader(northPort.getInputStream()));
			socketReader = new CommandsParser();
			socketReader.start();
			tsb = new ToSmartBoxThread();
			tsb.start();
			myObserver.portsOpened(getName());

			if (logger.isInfoEnabled()) {
				logger.info("Ports toward SmartBox OPENED "); //$NON-NLS-1$
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			if (logger.isFatalEnabled()) {
				logger.fatal("SmartBox connection failed - QUITTING"); //$NON-NLS-1$
				System.exit(-1);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#portsClose()
	 */
	@Override
	public void portsClose() {
		// TODO Auto-generated method stub

		socketReader.stopParser();
		tsb.stopRunning();

		try {
			fromSmartBoxStream.close();
			northPort.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// no need to log
		}

		myObserver.portsClosed(getName());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#start()
	 */
	@Override
	public void start() {

		// TODO Auto-generated method stub
		myObserver.startCompleted(getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#shutdown()
	 */
	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#restart()
	 */
	@Override
	public void restart() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#reset()
	 */
	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#getName()
	 */
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return myId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bluethread.types.system.interfaces.SubsystemIfc#setObserverCallback(
	 * com.bluethread.types.system.interfaces.ObserverCallbackIfc)
	 */
	@Override
	public void setObserverCallback(ObserverCallbackIfc obs) {
		// TODO Auto-generated method stub
		myObserver = obs;
	}

	private class ToSmartBoxThread extends Thread {
		private boolean stop = false;

		public void stopRunning() {
			if (logger.isDebugEnabled()) {
				logger.debug("run() - SOPPING forwarding engine of messages to SmartBox"); //$NON-NLS-1$
			}
			stop = true;
			toSmartBoxQueue.add("");
		}

		public void run() {
			if (logger.isDebugEnabled()) {
				logger.debug("run() - STARTED forwarding engine of messages to SmartBox"); //$NON-NLS-1$
			}
			while (!stop) {
				try {
					String message = toSmartBoxQueue.take();
					toSmartBoxStream.println(message);
					toSmartBoxStream.flush();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block

				}

			}
		}
	}

	private class CommandsParser extends Thread {

		private boolean stop = false;

		public void stopParser() {

			if (logger.isDebugEnabled()) {
				logger.debug("run() - STOPPING SmartBox commands parser"); //$NON-NLS-1$
			}
			stop = true;
		}

		public void run() {
			if (logger.isDebugEnabled()) {
				logger.debug("run() - STARTED SmartBox commands parser"); //$NON-NLS-1$
			}

			while (!stop) {
				String receivedCommandString = "";
				try {
					receivedCommandString = fromSmartBoxStream.readLine();
					if (logger.isDebugEnabled()) {
						logger.debug("run() - Received command: " + receivedCommandString + " from SmartBox"); //$NON-NLS-1$
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block

					if (logger.isFatalEnabled()) {
						logger.fatal("run() - Failed to read from commands stream - quitting"); //$NON-NLS-1$
					}
					System.exit(-1);
					// stop = true;
					// break;
				}

				ANTLRInputStream input = new ANTLRInputStream(receivedCommandString);
				ItsCommand command = new ItsUndefined();
				SmartBoxCommandsGrammarLexer lexer = new SmartBoxCommandsGrammarLexer(input);
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				SmartBoxCommandsGrammarParser parser = new SmartBoxCommandsGrammarParser(tokens);
				parser.setErrorHandler(new BailOutErrorStrategy());

				try {
					ParseTree tree = parser.root();
					ParseTreeWalker walker = new ParseTreeWalker();
					SmartBoxCommandsListener sbParser = new SmartBoxCommandsListener();

					walker.walk(sbParser, tree);

					command = sbParser.getCommand();

					/**
					 * if rejected by the filter, it's a configuration command
					 */
					if (filter.rejectedByFilter(command)) {
						bus.post(command);

					} else {
						
						bus.post(command);
						if (logger.isDebugEnabled()) {
							logger.debug("run() - Its Command passed to AisModule for processing [code,dest MMSI]: "+command.getCommandCode()+","+command.getDestMMSI()); //$NON-NLS-1$
						}
						
					}

				} catch (Exception ex) {
					if (logger.isDebugEnabled()) {
						logger.debug("run() - Error parsing input command from SmartBox:" + receivedCommandString); //$NON-NLS-1$
					}

				}

			}
		}

	}

	

	@Subscribe
	public void receiveMessage(String message) {

		if (logger.isDebugEnabled()) {
			logger.debug("receiveMessage(String) - Event received from the Bus  - message={}", message); //$NON-NLS-1$
		}

		try {
			toSmartBoxQueue.add(message);
		} catch (IllegalStateException ex) {

			logger.error("handleMessage(String) - Queue is full, discarding AIS message  - message={}", message); //$NON-NLS-1$

		}
	}

}
