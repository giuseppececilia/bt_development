package com.bluethread.rimorchiatori.smartbox.southifc.types;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface IPortProvider {

	public InputStream getIn() throws IOException;

	public OutputStream getOut() throws IOException;
}
