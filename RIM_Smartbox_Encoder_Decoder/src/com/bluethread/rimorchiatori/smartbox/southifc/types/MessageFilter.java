package com.bluethread.rimorchiatori.smartbox.southifc.types;

import java.util.ArrayList;

import dk.dma.ais.filter.MessageFilterBase;
import dk.dma.ais.message.AisMessage;
import static java.util.Objects.requireNonNull;

public class MessageFilter extends MessageFilterBase {

	protected ArrayList<Integer> mmsiList;

	public MessageFilter(int[] mmsi) {
		this();
		requireNonNull(mmsi);

		for (int i = 0; i < mmsi.length; i++) {
			mmsiList.add(new Integer(mmsi[i]));
		}
	}

	public MessageFilter() {
		mmsiList = new ArrayList<Integer>();
	}

	public synchronized void addMMSI(int[] mmsiList) {

		requireNonNull(mmsiList);
		for (int i = 0; i < mmsiList.length; i++) {

			if (!this.mmsiList.contains(new Integer(mmsiList[i])))
				this.mmsiList.add(new Integer(mmsiList[i]));
		}
	}

	public synchronized void removeMMSI(int[] mmsiList) {

		requireNonNull(mmsiList);
		for (int i = 0; i < mmsiList.length; i++) {

			if (this.mmsiList.contains(new Integer(mmsiList[i])))
				this.mmsiList.remove(new Integer(mmsiList[i]));
		}
	}

	@Override
	public boolean rejectedByFilter(AisMessage message) {
		// TODO Auto-generated method stub

		if (mmsiList.isEmpty())
			return false;

		switch (message.getMsgId()) {
		case 6:
		case 8:
			if (mmsiList.contains(new Integer(message.getUserId())))
				return true;
			break;
		default:
			return false;

		}

		return false;
	}

}
