package com.bluethread.rimorchiatori.smartbox.southifc.types.test;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.ArrayList;

import com.bluethread.rimorchiatori.smartbox.southifc.types.MessageFilter;

import dk.dma.ais.message.AisMessage;

public class TestMessageFilter extends MessageFilter {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(TestMessageFilter.class.getName());

	public TestMessageFilter() {
		super();
		if (logger.isDebugEnabled()) {
			logger.debug("TestMessageFilter() - TestMessageFilter instantiated"); //$NON-NLS-1$
		}
		//for testing with ITS
		mmsiList.add(new Integer(2470101));

	}

	@Override
	public boolean rejectedByFilter(AisMessage message) {
		// TODO Auto-generated method stub

		if (mmsiList.isEmpty())
			return false;

		if (mmsiList.contains(new Integer(message.getUserId())))
			return true;

		return false;
	}

}
