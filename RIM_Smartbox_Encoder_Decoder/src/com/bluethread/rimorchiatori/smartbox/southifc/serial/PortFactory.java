package com.bluethread.rimorchiatori.smartbox.southifc.serial;

import java.util.Properties;

import com.bluethread.rimorchiatori.smartbox.southifc.types.IPortProvider;

public class PortFactory {

	private static IPortProvider portProvider = null;

	public static IPortProvider getPortProvider(Properties props) {

		if (portProvider != null)
			return portProvider;
		else {
			boolean underTest = false;
			String portName = "COM1";
			int baudRate = 9600;
			if (props.containsKey("system.under.test")) {

				underTest = Boolean.parseBoolean(props.getProperty("system.under.test"));
			}
			if (underTest) {
				portProvider = new TestPortProvider();
			}

			else {

				if (props.containsKey("serial.port.name")) {
					portName = props.getProperty("serial.port.name");

				}
				if (props.containsKey("serial.port.baud")) {
					baudRate = Integer.parseInt(props.getProperty("serial.port.baud"));

				}
				portProvider = new SerialConcretePortProvider(portName, baudRate);
			}
			return portProvider;
		}

	}

}
