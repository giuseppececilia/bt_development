package com.bluethread.rimorchiatori.smartbox.southifc.serial;

import org.apache.logging.log4j.Logger;

import com.bluethread.rimorchiatori.smartbox.southifc.types.IPortProvider;

import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

public class SerialConcretePortProvider implements IPortProvider {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(SerialConcretePortProvider.class.getName());

	private String serialPortID;
	private int baud;
	private SerialPort serial;

	private static int TIME_OUT = 2000;

	public SerialConcretePortProvider(String portName, int baud) {

		// TODO Auto-generated constructor stub

		this.serialPortID = portName;
		this.baud = baud;

		CommPortIdentifier portId;
		try {
			portId = CommPortIdentifier.getPortIdentifier(serialPortID);
			serial = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);
		} catch (PortInUseException e1) {
			// TODO Auto-generated catch block

			if (logger.isInfoEnabled()) {
				logger.info("QUITTING - Serial port already in use - portName = " + serialPortID); //$NON-NLS-1$
			}

			System.exit(-1);

		} catch (NoSuchPortException e1) {
			// TODO Auto-generated catch block

			if (logger.isInfoEnabled()) {
				logger.info("QUITTING - Serial port not found - portName = " + serialPortID); //$NON-NLS-1$
			}

			System.exit(-1);
		}

		try {
			serial.setSerialPortParams(this.baud, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			// next 2 lines are vital for blocking IO on serial port
			// otherwise IOException is raised

			serial.disableReceiveTimeout();
			serial.enableReceiveThreshold(1);
		} catch (UnsupportedCommOperationException e) {

			if (logger.isInfoEnabled()) {
				logger.info("portsOpen() Failed to open serial port  - portName =" + serialPortID); //$NON-NLS-1$
			}

			System.exit(-1);
		}

		if (logger.isInfoEnabled()) {
			logger.info("portsOpen() Opened serial port - portName =" + serialPortID); //$NON-NLS-1$
		}
	}

	@Override
	public InputStream getIn() throws IOException {
		// TODO Auto-generated method stub
		return serial.getInputStream();
	}

	@Override
	public OutputStream getOut() throws IOException {
		// TODO Auto-generated method stub
		return serial.getOutputStream();
	}

}
