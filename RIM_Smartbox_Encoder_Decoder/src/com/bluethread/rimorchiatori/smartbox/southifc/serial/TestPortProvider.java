package com.bluethread.rimorchiatori.smartbox.southifc.serial;

import org.apache.logging.log4j.Logger;

import com.bluethread.rimorchiatori.smartbox.southifc.types.IPortProvider;

import org.apache.logging.log4j.LogManager;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class TestPortProvider implements IPortProvider {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(TestPortProvider.class.getName());

	private InputStream in;
	private OutputStream out;

	public TestPortProvider() {
		// TODO Auto-generated constructor stub
		try {
			in = new FileInputStream("sentences.txt");
			out = new FileOutputStream("output.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (logger.isInfoEnabled()) {
			logger.info("TestPortProvider() - Test Port Provider instantiated"); //$NON-NLS-1$
		}

	}

	@Override
	public InputStream getIn() throws IOException {
		// TODO Auto-generated method stub
		return in;

	}

	@Override
	public OutputStream getOut() throws IOException {
		// TODO Auto-generated method stub
		return out;
	}

}
