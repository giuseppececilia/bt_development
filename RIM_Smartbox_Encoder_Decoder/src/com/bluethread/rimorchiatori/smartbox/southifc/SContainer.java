/**
 * 
 */
package com.bluethread.rimorchiatori.smartbox.southifc;

import org.apache.logging.log4j.Logger;

import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.function.Consumer;

import com.bluethread.rimorchiatori.smartbox.bus.Bus;
import com.bluethread.rimorchiatori.smartbox.main.types.IMessageHandler;
import com.bluethread.rimorchiatori.smartbox.southifc.serial.PortFactory;
import com.bluethread.rimorchiatori.smartbox.southifc.types.MessageFilter;
import com.bluethread.rimorchiatori.smartbox.southifc.types.test.TestMessageFilter;
import com.bluethread.types.system.BTSystem;
import com.bluethread.types.system.interfaces.ObserverCallbackIfc;
import com.bluethread.types.system.interfaces.SubsystemIfc;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import dk.dma.ais.message.AisMessage;
import dk.dma.ais.packet.AisPacketReader;
import dk.dma.ais.reader.AisReader;
import dk.dma.ais.reader.AisReaders;
import dk.dma.ais.sentence.Abm;

/**
 * @author Peppe
 *
 */
public class SContainer implements SubsystemIfc {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(SContainer.class.getName());

	private static final String myId = "SContainer";
	private ObserverCallbackIfc myObserver;
	private Properties myProps;

	private InputStream in;
	private OutputStream out;

	private Engine theEngine;



	private MessageFilter filter;
	/**
	 * slowdown is a sleep to slow down reading from the serial in order not to
	 * full the queues
	 */
	private int slowdownreader = 0;

	// ONLY FOR TEST - TO BE REMOVED
	// private static final int[] array = { 205111000, 246173000, 413443970 };
	// MMSI_ADD|205111000|246173000|413443970|244660879|219015063
	// MMSI_ADD|2470100
	// LOGIN_RECEIVE|2470100|OK
	// MMSI_REMOVE|205111000|246173000|413443970|244660879

	// private BTSystem mySys;

	// private SerialPort serial;
	// private String serialPortID;
	// private int baudRate;
	// private static final int TIME_OUT = 2000;

	private EventBus bus;

	public SContainer(Properties props) {
		myProps = props;

		bus = Bus.getMessagesBus();
		bus.register(this);

		// serialPortID = myProps.getProperty("serial.port.name");
		// baudRate = Integer.parseInt(myProps.getProperty("serial.port.baud"));

		// Test filter, matches only MMSI, not AIS message code
		filter = new TestMessageFilter();

		if (myProps.containsKey("system.serial.slowdown.reader"))
			slowdownreader = Integer.parseInt(myProps.getProperty("system.serial.slowdown.reader"));
	}

	

	public void addMMSIsToFilter(int[] mmsiList) {

		filter.addMMSI(mmsiList);
		if (logger.isDebugEnabled()) {
			logger.debug("addMMSIsToFilter(int[]) - Added MMSIs to filter - mmsiList={}", mmsiList); //$NON-NLS-1$
		}

	}

	public void removeMMSIsToFilter(int[] mmsiList) {

		filter.removeMMSI(mmsiList);
		if (logger.isDebugEnabled()) {
			logger.debug("removeMMSIsToFilter(int[]) - Removed MMSIs to filter - mmsiList={}", mmsiList); //$NON-NLS-1$
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#portsOpen()
	 */
	@Override
	public void portsOpen() {

		try {
			in = PortFactory.getPortProvider(myProps).getIn();
			out = PortFactory.getPortProvider(myProps).getOut();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);

		}

		theEngine = new Engine();
		theEngine.start();

		myObserver.portsOpened(myId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#portsClose()
	 */
	@Override
	public void portsClose() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#start()
	 */
	@Override
	public void start() {
		// TODO Auto-generated method stub
		myObserver.startCompleted(myId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#shutdown()
	 */
	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#restart()
	 */
	@Override
	public void restart() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#reset()
	 */
	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#getName()
	 */
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return myId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bluethread.types.system.interfaces.SubsystemIfc#setObserverCallback(
	 * com.bluethread.types.system.interfaces.ObserverCallbackIfc)
	 */
	@Override
	public void setObserverCallback(ObserverCallbackIfc obs) {
		// TODO Auto-generated method stub
		myObserver = obs;
	}

	private class Engine extends Thread {

		private AisReader aisReader;

		public void run() {

			aisReader = AisReaders.createReaderFromInputStream(in);
			
			aisReader.registerHandler(new Consumer<AisMessage>() {

				public void accept(AisMessage aisMessage) {
					if (logger.isDebugEnabled()) {
						logger.debug("$Consumer<AisMessage>.accept(AisMessage) - message id:" + aisMessage.getMsgId() //$NON-NLS-1$
								+ " MMSI:" + aisMessage.getUserId());
						logger.debug("VDM:" + aisMessage.getVdm().getRawSentencesJoined().toString());
					}

					// Filter selects messages coming from the ships belonging
					// to the system
					if (filter.rejectedByFilter(aisMessage)) {

						if (logger.isInfoEnabled()) {
							logger.info(
									"$Consumer<AisMessage>.accept(AisMessage) - AIS message filtered out - message forwarded to AisModule for processing", //$NON-NLS-1$
									aisMessage.getUserId());

							bus.post(aisMessage);
						}
					} else {
						//VDM contains all the sentences related to AisMessage, in case there's more than one, 
						//send them on different bus calls
						int numOfSentences = aisMessage.getVdm().getRawSentences().size();
						String[] sentences = new String[numOfSentences];
						aisMessage.getVdm().getRawSentences().toArray(sentences);
						for (int i = 0; i<numOfSentences;i++){
							bus.post(sentences[i]);
						}
						
						
					}

					if (slowdownreader > 0) {
						try {
							Thread.sleep(slowdownreader);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
			});

			aisReader.start();
			try {
				aisReader.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	/**
	 * Method to receive the ABM from the BUS to be sent to the RADIO
	 * ABMs are generated by the AisModule
	 * @param abm
	 */
	@Subscribe
	public void receiveAbm(Abm abm) {
		

		try {
			
			String tbsent = abm.getEncoded()+"\r\n";
			out.write(tbsent.getBytes());
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (logger.isDebugEnabled()) {
			logger.debug("receiveAbm(Abm) - Sent out to radio ABM" + abm.getEncoded()); //$NON-NLS-1$
		}

	}

}
