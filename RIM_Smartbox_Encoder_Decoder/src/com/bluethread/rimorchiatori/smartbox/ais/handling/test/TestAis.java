/**
 * 
 */
package com.bluethread.rimorchiatori.smartbox.ais.handling.test;

import dk.dma.ais.binary.SixbitEncoder;
import dk.dma.ais.binary.SixbitException;
import dk.dma.ais.message.AisMessage6;
import dk.dma.ais.message.binary.BroadcastAreaNotice;
import dk.dma.ais.sentence.Vdm;

/**
 * @author Giuseppe Cecilia
 *
 */
public class TestAis {

	public TestAis() {

		SixbitEncoder sbe;
		BroadcastAreaNotice ban = new BroadcastAreaNotice();
		AisMessage6 msg = new AisMessage6();
		msg.setDestination(123456342);
		msg.setUserId(46274672);
		ban.setDuration(3);
		ban.setNotice(0);

		msg.setAppMessage(ban);

		sbe = msg.getEncoded();

		Vdm vdm = new Vdm();
		vdm.setTalker("PIPPO");

		try {

			vdm.setEncodedMessage(sbe);

			System.out.println(vdm.getEncoded());
		} catch (SixbitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {

		TestAis tais = new TestAis();
	}
}
