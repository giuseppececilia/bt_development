/**
 * 
 */
package com.bluethread.rimorchiatori.smartbox.ais.handling;

import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Logger;

import com.bluethread.types.queues.RingBuffer;

/**
 * @author Giuseppe Cecilia
 *
 */
public class AisProtocolHandler {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AisProtocolHandler.class.getName());

	private RingBuffer<ProtocolTask> inQueue;
	private HashMap<Integer, ProtocolJob> msgMap;
	private Engine myEngine;

	public AisProtocolHandler(Properties props) {

		int size = Integer.parseInt(props.getProperty("protocolhandler.inqueue.size"));
		inQueue = new RingBuffer<ProtocolTask>(size);
		msgMap = new HashMap<Integer, ProtocolJob>();
		myEngine = new Engine();
		myEngine.start();

	}

	public void handle(ProtocolTask task) {
		try {
			inQueue.add(task);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block

			logger.severe(
					"ProtocolTask - Error while requesting to process task  - task=" + task + " - exception: " + e); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	private class Engine extends Thread {

		public void run() {

			while (true) {
				try {
					inQueue.remove();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}

}
