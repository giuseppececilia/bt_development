/**
 * 
 */
package com.bluethread.rimorchiatori.smartbox.ais.handling;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Properties;
import java.util.concurrent.LinkedBlockingDeque;

import com.bluethread.ais.messages.BtAisAsm;
import com.bluethread.ais.messages.BtAisAsmLoginReceive;
import com.bluethread.ais.messages.BtAisAsmLoginSend;
import com.bluethread.rimorchiatori.smartbox.bus.Bus;

import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsCommand;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsCommands;
import com.bluethread.rimorchiatori.smartbox.northifc.types.ItsLoginReceive;
import com.bluethread.types.system.interfaces.ObserverCallbackIfc;
import com.bluethread.types.system.interfaces.SubsystemIfc;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import dk.dma.ais.binary.SixbitException;
import dk.dma.ais.message.AisMessage;
import dk.dma.ais.message.AisMessage6;
import dk.dma.ais.sentence.Abm;
import dk.dma.ais.message.AisBinaryMessage;


/**
 * @author Peppe
 *
 */
public class AisModule implements SubsystemIfc {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(AisModule.class.getName());

	private static final String myId = "AisModule";
	private ObserverCallbackIfc myObserver;
	private Properties myProps;
	private EventBus bus;
	private SouthToNorthEngine s2nEngine;
	private NorthToSouthEngine n2sEngine;

	private LinkedBlockingDeque<AisMessage> southToNorthQueue;
	private LinkedBlockingDeque<ItsCommand> NorthToSouthQueue;
	private int myMMSI;
	private String talker;

	public AisModule(Properties props) {
		myProps = props;
		if(myProps.containsKey("system.radio.station.mmsi")){
			myMMSI = Integer.parseInt(myProps.getProperty("system.radio.station.mmsi"));
		}else{
			logger.fatal("AisModule(Properties) - Radio Station MMSI NOT CONFIGURED - QUITTING"); //$NON-NLS-1$
			System.exit(-1);
		}
		
		if(myProps.containsKey("system.radio.station.talker")){
			talker = myProps.getProperty("system.radio.station.talker");
		}else{
			talker = "AI";
		}
		southToNorthQueue = new LinkedBlockingDeque<AisMessage>();
		NorthToSouthQueue = new LinkedBlockingDeque<ItsCommand>();
		bus = Bus.getMessagesBus();
		bus.register(this);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#portsOpen()
	 */
	@Override
	public void portsOpen() {
		// TODO Auto-generated method stub

		myObserver.portsOpened(myId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#portsClose()
	 */
	@Override
	public void portsClose() {
		// TODO Auto-generated method stub

		myObserver.portsClosed(myId);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#start()
	 */
	@Override
	public void start() {
		// TODO Auto-generated method stub
		s2nEngine = new SouthToNorthEngine();
		n2sEngine = new NorthToSouthEngine();
		s2nEngine.start();
		n2sEngine.start();
		
		if (logger.isDebugEnabled()) {
			logger.debug("start() - North2South and South2North engines started"); //$NON-NLS-1$
		}
		
		
		myObserver.startCompleted(myId);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#shutdown()
	 */
	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#restart()
	 */
	@Override
	public void restart() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#reset()
	 */
	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bluethread.types.system.interfaces.SubsystemIfc#getName()
	 */
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bluethread.types.system.interfaces.SubsystemIfc#setObserverCallback(
	 * com.bluethread.types.system.interfaces.ObserverCallbackIfc)
	 */
	@Override
	public void setObserverCallback(ObserverCallbackIfc obs) {
		// TODO Auto-generated method stub

		myObserver = obs;

	}

	@Subscribe
	public void receiveAisMessage(AisMessage message) {
		if (logger.isDebugEnabled()) {
			logger.debug("receiveAisMessage(AisMessage) - Received AIS message to handle" + message.toString()); //$NON-NLS-1$
		}
		southToNorthQueue.add(message);

	}

	@Subscribe
	public void receiveItsCommands(ItsCommand command) {
		if (logger.isDebugEnabled()) {
			logger.debug("receiveItsCommand(ItsCommand) - Received ItsCommand " + command.toString()); //$NON-NLS-1$
		}
		NorthToSouthQueue.add(command);
	}
	
	
	/**
	 * 
	 * @author Giuseppe Cecilia
	 * 
	 * class to process LOGIN RECEIVE message to be sent to the ship in response to
	 * LOGIN SEND. the AisMessage created is sent to South Container for transmitting to SHIP
	 *
	 */
	
	private class LRProcessor implements Runnable{
		private ItsLoginReceive myCmd;

		public LRProcessor(ItsLoginReceive cmd){
			myCmd = cmd;
		}
		@Override
		public void run() {
			
			if (logger.isDebugEnabled()) {
				logger.debug("run() - Login Receive Processor started for message received to MMSI - myMsg={}", //$NON-NLS-1$
						myCmd.getDestMMSI());
			}
			// TODO Auto-generated method stub		
			AisMessage6 aisMsg = getClean6message(myCmd.getDestMMSI());
			BtAisAsmLoginReceive btAsm = new BtAisAsmLoginReceive();
			btAsm.setAnswer(myCmd.getAnswer());
			aisMsg.setAppMessage(btAsm);	
			Abm abm = getCleanAbm(aisMsg);
			abm.setDestination(myCmd.getDestMMSI());
			bus.post(abm);
			if (logger.isDebugEnabled()) {
				logger.debug("run() - Created ABM for LOGIN RECEIVE message to MMSI:"+myCmd.getDestMMSI()+", ABM:"+abm.getEncoded()); //$NON-NLS-1$
			}
			
		}
		
	}

	/**
	 * 
	 * @author Giuseppe Cecilia
	 * Class to process the LOGIN SEND message received from the SHIP
	 * the output is sent to North Container for sending to Service Center 
	 *
	 */
	private class LSProcessor implements Runnable {

		private BtAisAsmLoginSend myMsg;
		private long sourceMMSI;

		public LSProcessor(long MMSI, BtAisAsmLoginSend ls) {
			myMsg = ls;
			sourceMMSI = MMSI;

		}

		@Override
		public void run() {
			if (logger.isDebugEnabled()) {
				logger.debug("run() - Login Send Processor started for message received from MMSI - myMsg={}", //$NON-NLS-1$
						sourceMMSI);
			}

			// TODO Auto-generated method stub
			StringBuilder toSC = new StringBuilder();
			toSC.append("LOGIN_SEND|");
			toSC.append(sourceMMSI);
			toSC.append("|");
			toSC.append(myMsg.getPIN());
			if (logger.isDebugEnabled()) {
				logger.debug("run() - LOGIN SEND TO SC - toSC={}", toSC); //$NON-NLS-1$
			}

			bus.post(toSC.toString());

		}
	}

	private class NorthToSouthEngine extends Thread{
		public void run(){
			if (logger.isDebugEnabled()) {
				logger.debug("run() - Started N2S Engine of AisModule"); //$NON-NLS-1$
			}

			while (true){
				
				try {
					ItsCommand commandToProcess = NorthToSouthQueue.take();
					switch(commandToProcess.getCommandCode()){
					case ItsCommands.LOGIN_RECEIVE: 
						(new Thread(new LRProcessor((ItsLoginReceive) commandToProcess))).start();
						break;
					case ItsCommands.CREW_UPDATE: break;
					case ItsCommands.MISSION_SEND: break;
					case ItsCommands.MISSION_STATE_ACK: break;
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
	}
	
	private class SouthToNorthEngine extends Thread {
		
		public void run() {
			if (logger.isDebugEnabled()) {
				logger.debug("run() - Started S2N Engine of AisModule"); //$NON-NLS-1$
			}

			while (true) {

				try {
					// take from the queue of received messages
					AisMessage msg = southToNorthQueue.take();

					int messageType = msg.getMsgId();
					switch (messageType) {
					case 6:
					case 8:
						// if we are here we're pretty sure it's our kind of
						// message
						if (logger.isDebugEnabled()) {
							logger.debug("AisModule: received BT proprietary AisMessage from MMSI" + msg.getUserId()); //$NON-NLS-1$
						}
						BtAisAsm btAsm;
						try {
							btAsm = (BtAisAsm) ((AisBinaryMessage) msg).getApplicationMessage();
							
							if (logger.isDebugEnabled()) {
								logger.debug("BT proprietary with ASM code:"+btAsm.getMessageCode()); //$NON-NLS-1$
							}

							// start processor for the kind of message

							switch (btAsm.getMessageCode()) {
							case BtAisAsm.LOGIN_SEND:
								(new Thread(new LSProcessor(msg.getUserId(), (BtAisAsmLoginSend) btAsm))).start();
								break;
							
							case BtAisAsm.SERVICE_RES:
								break;
							case BtAisAsm.CREW_UPDATE:
								break;

							default:
								break;
							}
						} catch (SixbitException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;

					case 7:
						break;
					default: // discard
						if (logger.isDebugEnabled()) {
							logger.debug("AisModule: received AisMessage to be discarded from MMSI" + msg.getUserId()); //$NON-NLS-1$
						}
						break;
					}

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	private  Abm getCleanAbm(AisBinaryMessage msg){
		Abm abm = new Abm();
		try {
			abm.setBinaryData(msg);
		} catch (SixbitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		abm.setTalker(talker);
		abm.setTotal(1);
		abm.setChannel(null);
		abm.setSequence(0);
		abm.setNum(1);
		return abm;
		
	}
	
	private AisMessage6 getClean6message(int destMMSI){
		AisMessage6 msgToReturn= new AisMessage6();
		msgToReturn.setRepeat(0);
		msgToReturn.setRetransmit(0);
		msgToReturn.setUserId((int)myMMSI);
		msgToReturn.setDestination(destMMSI);
		return msgToReturn;
	}

}
