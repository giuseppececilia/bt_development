package com.bluethread.rimorchiatori.smartbox.ais.handling.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.function.Consumer;

import dk.dma.ais.message.AisMessage;
import dk.dma.ais.reader.AisReader;
import dk.dma.ais.reader.AisReaders;

public class AisReaderTest {

	public static void main(String[] args) {
		AisReader reader;
		try {
			reader = AisReaders.createReaderFromInputStream(new FileInputStream("sentences.txt"));

			reader.registerHandler(new Consumer<AisMessage>() {

				public void accept(AisMessage aisMessage) {
					System.out.println("message id: " + aisMessage.getMsgId()+",MMSI:"+aisMessage.getUserId());
					
				}
			});

			reader.start();

			reader.join();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
